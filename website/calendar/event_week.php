<?php

/**
 * @file
 * Calendar week view.
 */

use Indybay\Page;

include_once '../../classes/config/indybay.cfg';
$page = new Page('event_week', 'calendar');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  $GLOBALS['body_class'] = 'page-event-week';
  include INCLUDE_PATH . '/common/content-header.inc';
  echo $page->getHtml();
  include INCLUDE_PATH . '/common/footer.inc';
}
