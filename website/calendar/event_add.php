<?php

/**
 * @file
 * Add and Update a Event.
 */

use Indybay\Page;
use Indybay\Translate;

include_once '../../classes/config/indybay.cfg';
$sftr = new Translate();
// Add article dictionary for some publish strings.
$sftr->createTranslateTable('article');
$page = new Page('event_add', 'calendar');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $GLOBALS['jquery'][] = 'validate';
  $GLOBALS['js'] = ['event_add', 'fileupload', 'publish'];
  $GLOBALS['ui']['datepicker'] = TRUE;
  $GLOBALS['body_class'] = 'page-publish page-event-add page-event';
  $GLOBALS['page_title'] = 'Publish to Calendar';
  include INCLUDE_PATH . '/common/content-header.inc';
  $page->buildPage();
  echo $page->getHtml();
  include INCLUDE_PATH . '/common/footer.inc';
}
