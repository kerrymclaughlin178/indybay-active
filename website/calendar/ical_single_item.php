<?php

/**
 * @file
 * Add and Update a Event.
 */

use Indybay\Page;

include_once '../../classes/config/indybay.cfg';
header('Content-Type: text/calendar');
$news_item_id = (int) $_GET['news_item_id'];
header('Content-Disposition: inline; filename="indybay_event_' . $news_item_id . '.ics"');
$page = new Page('ical_single_item', 'calendar');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}
