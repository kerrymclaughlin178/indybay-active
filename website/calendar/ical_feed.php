<?php

/**
 * @file
 * Generate an icalendar event feed.
 */

use Indybay\Page;

include_once '../../classes/config/indybay.cfg';
header('Content-Type: text/calendar');
$topic_id = isset($_GET['topic_id']) ? intval($_GET['topic_id']) : '0';
$region_id = isset($_GET['region_id']) ? intval($_GET['region_id']) : '0';
header('Content-Disposition: inline; filename="indybay_event_' . $topic_id . '_' . $region_id . '.ics"');
$page = new Page('ical_feed', 'calendar');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}
