<?php

/**
 * @file
 * This page shows the mailing list page for sending.
 */

// Out email to the indymedia weely email list.
include_once '../../classes/config/indybay.cfg';

$GLOBALS['page_title'] = 'News Services';
$GLOBALS['body_class'] = 'page-subscribe';

include INCLUDE_PATH . '/common/content-header.inc';

use Indybay\Page;

$page = new Page('mailinglist', 'email');

if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}

include INCLUDE_PATH . '/common/footer.inc';
