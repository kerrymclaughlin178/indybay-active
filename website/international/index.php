<?php

/**
 * @file
 * International page.
 */

$GLOBALS['page_title'] = 'International News';
$GLOBALS['page_ids'] = [44];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
