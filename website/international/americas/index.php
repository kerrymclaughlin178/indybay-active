<?php

/**
 * @file
 * Americas page.
 */

$GLOBALS['page_title'] = 'Americas News';
$GLOBALS['page_ids'] = [53];

include_once '../../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
