(function($) {
  $(document).ready(function() {

    // Clears file inputs on click.
    $('body').on('click', '.clearimage', function() {
      var thisInput = $(this).closest('td').children('.fileinputwrapper').children('.fileinput')[0];
      $(thisInput).replaceWith($(thisInput).val('').clone(true));
      $(this).parent('.previewimagewrapper').css('display', 'none');
      $(this).siblings('.previewimage').removeAttr('src');
    });
    // Loads preview images or custom alternate message.
    $('body').on('change', '.fileinput', function() {
      var thisEl = $(this);
      var reader = new FileReader();
      reader.onload = function(e) {

        var dataType = reader.result.split(';')[0].split(':')[1];
        var fileType = dataType.split('/')[1].toUpperCase();

        // Conditional for MIME type.
        if (dataType.includes('image/')) {
          // Get loaded data and render thumbnail.
          $(thisEl).closest('td').find('.previewimage').attr('src',e.target.result);
          $(thisEl).closest('td').find('.previewimage').css('display', 'block');
          $(thisEl).closest('td').find('.nopreview').css('display', 'none');
          $(thisEl).parent().siblings('.previewimagewrapper').css('display', 'inline-block');
        } else {
          // Load non-image message.
          $(thisEl).closest('td').find('.filetype').text(fileType);
          $(thisEl).closest('td').find('.nopreview').css('display', 'block');
          $(thisEl).closest('td').find('.previewimage').css('display', 'none');
          $(thisEl).parent().siblings('.previewimagewrapper').css('display', 'inline-block');
        }

      };
      // Read the image file as a data URL.
      reader.readAsDataURL(this.files[0]);
    });

    // Controls publish form inputs display.
    $("#files_select_template").hide();
    $("#upload_submit_button").hide();
    $("#nonjscript_file_boxes").hide();
    $("#upload_warning").hide();
    if ($('input[name=captcha_math]').attr('type') != 'hidden') {
      $('input[name=publish]').attr('disabled', 'disabled');
      $('#file_count').attr('disabled', 'disabled');
      $('#file_count').after(' <small>Select files to upload after completing the CAPTCHA question and clicking Preview below.</small>');
    }
    $("#file_count").change(
      function() { var numSelected=parseInt($("#file_count option:selected").val());
  	    $("nonjscript_file_boxes").hide();

				if (numSelected>0) {
					$("#preview_button").hide();
				} else {
					$("#preview_button").show();
				}
				if (numSelected>1) {
					var filebox=$("#files_select_template").clone();
				}
				for (var i=1; i<=20; i++){
					if (i<=numSelected){

						if ($("#files_select_"+i).length == 0) {
							var fileboxstr="";
		  					if (i==1){var k=fileboxstr.indexOf("<!--endoffirstupload-->");
								fileboxstr=$("#files_select_template_1").html();
		  					} else {
		  						fileboxstr=filebox.html().replace(/::uploadnum::/g,i);
		  					}
		  					fileboxstr="<div id='files_select_"+i+"'>"+fileboxstr+"</div>";
		  					$("#file_boxes2").append(fileboxstr);
						}
					} else {
						if ($("#files_select_"+i).length > 0) {
							$("#files_select_"+i).remove();
						}
					}
				}
  		}

  	);
    var numSelected=$("#file_count option:selected").val();
    if (numSelected>0) { $("#file_count").trigger("change"); }

  });
})(jQuery);
