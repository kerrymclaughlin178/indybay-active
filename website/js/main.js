(function($) {
  $(document).ready(function() {

    // Breaks site out of iframes of other sites.
    if (top.location != location) {
      top.location.href = document.location.href ;
    }
    // end iframe escape.

  	calcWindow = function() {
  		//windowWidth = $(window).width();
    	windowWidth = document.body.parentNode.clientWidth;
  		windowHeight =  $(window).height();
  		fullHeight =  document.body.clientHeight;
        // console.log('fullHeight: ',fullHeight);
        $('#shadow-maincontent').height(fullHeight);
  		DocPosTop = $(document).scrollTop();
  	}
  	calcWindow();

    // Pushes footer to bottom of window.
  	footerSet = function() {
  		calcWindow();
      mastHeight =  $('.mast').height();
      headernavHeight =  $('#headernav').height();
      footerHeight =  $('.footer').height();
      nonContentHeight = mastHeight + headernavHeight + footerHeight;
      remainingHeight = windowHeight - nonContentHeight - 8;
      $('#maincontent').css('min-height', remainingHeight);
  	}
    footerSet();

  	// Turns header nav items on and off depending on page width/resize.
  	navSet = function() {
  		calcWindow();
      // titleSet();
  		$('div,td').removeClass('active');
      // $('#headernav, .tabmenu, #shadow-maincontent, #shadow-mast').hide();
      $('#headernav, .tabmenu, #shadow-maincontent').hide();
      $('#headertab-feat').addClass('active');
  		if (windowWidth > 1040) {
        $('#headernav, .rightcol, .newswire, .newswirewrap').show();
        $('.cities-list').empty();
  		}
  	}

  	// Sets "back to top" box on or off according to page width/scroll.
  	backTopSet = function() {
  		calcWindow();
  		if ((windowWidth < 1040) && (DocPosTop > 1000)) {
  			$('#backtotopbox').css('opacity', '1');
  			$('#backtotopbox').fadeIn(1000);
  		} else {
  			$('#backtotopbox').fadeOut(500);
  		}
  	}

  	// Window resize behavior.
  	var saveWindowWidth = true;
    var savedWindowWidth;
    if (saveWindowWidth = true){
        savedWindowWidth = windowWidth;
        saveWindowWidth = false;
    }
  	$(window).resize(function() {
        windowWidth = window.innerWidth;
        if(savedWindowWidth != windowWidth) {
    			navSet();
    			backTopSet();
          savedWindowWidth = windowWidth;
        }
        footerSet();
  	});

  	// Copies center column calendar to cell for calendar tab menu overlay.
  	copyCenterCal = function() {
  		// $('.navbarcenter-calendar,.calendarwrapper-1,.calendarwrapper-2').clone().appendTo('#calendarwrap');
    	$('.navbarcenter-calendar,.calendarwrapper-1').clone().appendTo('#calendarwrap');
      // check whether highlighted events exist for feature page.
      $('#calendarwrap:empty')
        .html('<span class="noevents">No highlighted upcoming events for this topic or region.</span>');
  	}
    if ($('body').hasClass('page-front') || $('body').hasClass('page-feature')) {
  	 copyCenterCal();
    }

  	// Copies calendar page dates to cells in second row.
  	copyCalDates = function() {
  		$('.page-event-week .bodyClass tr:nth-child(1)').prepend( $('<td class="caldatelinks">Jump to: </td>') );
  		$('td.weekTitles').each(function(i) {
  			$('<div />', {
  				class: $(this).attr('class')
  			}).text($(this).text()).prependTo('.module-inner-horiz:eq(' + i + ')');
  			$('td.module-inner-horiz:eq(' + i + ') .weekTitles').wrapInner('<a name="caldateitem-' + i + '" class="caldateitem"></a>');
  			$('<div />', {
  				class: $(this).attr('class')
  			}).text($(this).text()).appendTo('.caldatelinks');
  			$('td.caldatelinks div.weekTitles:eq(' + i + ')').wrapInner('<a href="#caldateitem-' + i + '"></a>');
  		});
  		$('.pagecontent').prepend( $('<div class="calendar-header-mobile"></div>') );
  		$('.calendar-header > tbody > tr > td').each(function(i) {
  			$('<div />', {
  				class: 'calendar-header-element'
  			}).html($(this).html()).prependTo('.calendar-header-mobile');
  		});
  		$('.calendar-header-element:nth-child(2)').prependTo('.calendar-header-mobile');
  		$('.calendar-header-mobile .weekoftext').prependTo('.calendar-header-mobile .eventNav');
  	}
    if ($('body').hasClass('page-event-week')) {
  	   copyCalDates();
    }

  	// Mobile header button/tab behavior.
  	$('.closenav').click(function() {
      closeNav();
  	});
    closeNav = function() {
      // Only allows behavior if not actively scrolling.
      if (scrollActiveFlag == 0) {
        // $('#headernav,.tabmenu,.newswirewrap,#shadow-mast,#shadow-headertabs,#shadow-maincontent').hide();
        $('#headernav,.tabmenu,.newswirewrap,#shadow-maincontent,#shadow-headertabs').hide();
    		if (windowWidth > 1040) {
          $('#headernav, .rightcol, .newswire, .newswirewrap').show();
          $('.cities-list').empty();
    		}
        $('div,td').removeClass('active');
        $('#headertab-feat').addClass('active');
      }
    }

		// Header buttons behavior.
		$('.headerbutton').click(function() {
			if ($(this).hasClass('active')) {
        closeNav();
			} else {
        closeNav();
        $(this).addClass('active');
        thisID = $(this).attr('id');
        thisMenu = '#headermenu-' + thisID.replace('headerbutton-','');
        $(thisMenu).addClass('active');
				if (windowWidth > 1040) {
					// $('#shadow-maincontent').show();
          // $('#shadow-mast,#shadow-headertabs,#shadow-maincontent').show();
          $('#shadow-maincontent,#shadow-headertabs').show();
					// $('#headernav').hide();
				} else {
					$('#headernav,.tabmenu').hide();
					// $('#shadow-mast,#shadow-headertabs,#shadow-maincontent').show();
					$('#shadow-maincontent,#shadow-headertabs').show();
				}
			}
		});

    // Header menus population.
    $('#headernav .navbar').clone().appendTo('#headermenu-about');
    $('#headermenu-about a').removeClass('navbarlink').unwrap();
    if (($('.module-regions').length) && ($('.module-topics').length)) {
      $('.module-regions > div').clone().appendTo('.headermenu-category-regions');
      $('.module-topics > div').clone().appendTo('.headermenu-category-topics');
    } else {
      $('#headerbutton-category').css('display','none');
    }
    $('.headermenus .module-header-top, .headermenus .module-header').remove();
    $('.headermenus .thisfeaturelistitem a').each(function(i) {
       $(this).addClass('thisfeatured');
    });
    $('.headermenus .thisfeaturelistitem a, .headermenus .featurelistitem a').removeClass('bottomf').unwrap();

    // Header tabs behavior.
  	$('.headertab-bttn').on('click',function() {
  		// $('#headernav,#calendarwrap,#shadow-mast,#shadow-headertabs,#shadow-maincontent').hide();
    	$('#headernav,#calendarwrap,#shadow-maincontent,#shadow-headertabs').hide();
      $('.rightcol,.newswirewrap.newswire,.newswirewrap').hide();
      if ($(this).hasClass('active')) {
        $('#headernav,#calendarwrap').hide();
        $('.headertab-bttn').removeClass('active');
        $('#headertab-feat').addClass('active');
      } else {
        $('.headertab-bttn').removeClass('active');
        $(this).addClass('active');
        var thisTab = this.id;
        if (thisTab == 'headertab-feat') {
          // $('#shadow-maincontent').hide();
        } else if (thisTab == 'headertab-cal') {
          $('#calendarwrap,#shadow-maincontent').show();
        } else if (thisTab == 'headertab-newswire') {
          $('.rightcol,.newswire,.newswirewrap-local,.newswirewrap-global,.newswirewrap-other,#shadow-maincontent').show();
        }
      }
  	});

    // Load cities list.
    $('.cities-list').load('/cities.html', function() {
      removeSmalls();
    });

  	// Toggles cities list.
  	citiesToggle = function() {
  		$('.indy-link').click(function() {
  			//e.preventDefault();
  			$('.indy-link span').toggle();
  			$('.cities-list').slideToggle('slow');
  		});
  	}
    citiesToggle();

    closeCities = function() {
  		$('.cities-list').hide('slow');
  	}

  	// Removes small tags from #cities-list.
  	removeSmalls = function() {
  		$('.page .cities-list').find('small').contents().unwrap();
  	}
  	removeSmalls();

    // Adjusts sizing for media icons in articles/events/add comment.
  	iconFix = function() {
      $('img[src*="/im/imc_"]').each(function() {
        $(this).addClass('mediaicon');
      });
  	}
    if ($('body').is('.page-article, .page-event') || document.title.indexOf('comment')) {
  	   iconFix();
    }

    // Adjust landscape <video> styling.
    // $('.media video').each(function(i) {
    //   vidAspectRatio = $(this).attr('data-aspect-ratio');
    //   if (vidAspectRatio >= 1) {
    //     $(this).css('width', '100%');
    //   }
    // });

    // Show/hide media embed code.
    $('.embed').on('click',function() {
      $(this).parents('.media-options').children('.media-embed').toggle();
    });

    // Inserts top local newswire items into blurb/features.
    insertNewswire = function() {
      if ($('body').hasClass('page-front')) {
        // places insert after first blurb on front page.
        var afterBlurbNum = 3;
      } else {
        // places insert after first blurb on feature pages.
        var afterBlurbNum = 2;
      }
      var insertWrapper = '<div class="module-wrappers insert-wrapper"><div class="module module-unified"></div></div>';
      var titleAdd = '<div class="module-header"><a href="/search/?news_item_status_restriction=3&include_events=1&include_posts=1&page_id=12" class="bottommf">Latest from the Newswire</a></div>';
      $('.pagecontent .blurbwrapper:nth-child(' + afterBlurbNum + ')').after(insertWrapper);
      var newswireLink = '<div class="insert-newslink">More Newswire Stories</div>';
      // checks if local newswire exists, doesn't for international pages with newswires
      if ($('.newswirewrap-local').length) {
        $('.newswirewrap-local .nooz:nth-child(3), .newswirewrap-local .nooz:nth-child(4), .newswirewrap-local .nooz:nth-child(5)').clone().prependTo('.insert-wrapper .module.module-unified');
      } else {
        $('.newswire .nooz:nth-child(3), .newswire .nooz:nth-child(4), .newswire .nooz:nth-child(5)').clone().prependTo('.insert-wrapper .module.module-unified');
      }
      $(titleAdd).prependTo('.insert-wrapper .module.module-unified');
      $(newswireLink).appendTo('.insert-wrapper .module.module-unified');
    }
    if ($('.rightcol').length) {
      insertNewswire();
    }
    // Inserted newswire link to open local newswire overlay.
    $('.insert-newslink').on('click',function() {
      $('#headernav,#calendarwrap').hide();
      $('.rightcol,.newswire,.newswirewrap').hide();
      $('.headertab-bttn').removeClass('active');
      $('#headertab-newswire').addClass('active');
      $('.rightcol,.newswire,.newswirewrap-local,.newswirewrap-global,' +
        '.newswirewrap-other,#shadow-maincontent').show();
      scrollTop();
    });

    // Removes bullets from unordered list items with media icons inside blurbs.
    // $('.blurbtext li:has(img.mediaicon)').addClass('li-none');

    // Handles hard width post content (TABLEs/PREs/CODE) that might break width.
    overflowContentWrap = function() {
      $('.article > table, .article > pre, code, .article-attachment > table, .article-attachment > pre, code, .article-comment > table, .article-comment > pre, code').wrap('<div class="overflow-content"></div>');
    }
    overflowContentWidths = function() {
      $('.overflow-content').each(function(i) {
        var thisOverflowWidth = $(this).innerWidth();
        var thisOverflowChildWidth = $(this).children()[0].scrollWidth;
        if (thisOverflowChildWidth > thisOverflowWidth) {
          $(this).addClass('overflowed-content');
        }
      });
    }
    if ($('body').hasClass('page-article') || $('body').hasClass('page-event') || $('body').hasClass('page-subscribe')) {
      $.when(overflowContentWrap()).then(function(x) {
        overflowContentWidths();
      });
    }

  	// Scroll back to top.
  	$('#backtotopbox').click(function() {
      scrollTop();
  	});

    // Scrolls to top of page.
    scrollTop = function() {
  		scrollFlag = 1;
  		$('html, body').animate({'scrollTop': 0}, {
  				duration: 500,
  				complete: function() {
  					scrollFlag = 0;
  					backTopSet();
  				}
  		});
    }

  	// Behavior while scrolling.
  	scrollFlag = 0;
    scrollActiveFlag = 0;
  	$(window).scroll(function(){
  		$('#backtotopbox').css('opacity', '0.2');
  		$.doTimeout('scroll', 1000, function(){
  			if (scrollFlag == 0) {
  				backTopSet();
  			}
  		});
      scrollActiveFlag = 1;
  	});
    // Detect scroll end.
    $.fn.scrollEnd = function(callback, timeout) {
      $(this).scroll(function(){
        var $this = $(this);
        if ($this.data('scrollTimeout')) {
          clearTimeout($this.data('scrollTimeout'));
        }
        $this.data('scrollTimeout', setTimeout(callback,timeout));
      });
    };
    $(window).scrollEnd(function(){
        scrollActiveFlag = 0;
    }, 250);

  	// Runs after page load or later resize.
  	$(window).on('resize', function () {
  		backTopSet();
      closeCities();
      footerSet();
      closeNav();
  	});

    // Expands "clicktip" text such as on publish forms and Advanced Search.
    $('.clicktip .trigger').click(function() {
      $(this).hide();
      $(this).siblings('.target').show('fast');
    });

    // Advanced search page date restriction disabling/enabling.
    if ($('body').hasClass('page-search')) {
      $('.search-range .dateset select').prop('disabled', true);
    }
    $('.page-search #search_date_type').on('change', function () {
      var dateRestictVal = $(this).val();
      if (dateRestictVal != 0) {
        $('.search-range').removeClass('disabled');
        $('.search-range .dateset select').prop('disabled', false);
      } else {
        $('.search-range').addClass('disabled');
        $('.search-range .dateset select').prop('disabled', true);
      }
  	});

  });
})(jQuery);
