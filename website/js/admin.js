(function($) {
  $(document).ready(function() {

		// Article list admin functionality - used to set all selects to "hide".
		function hideall(){
			var i=0;
			for (i=0;i<200 && i<document.forms['order_form'].elements.length;i++){
				var nextelement=document.forms['order_form'].elements[i];
				if (nextelement.type=='select-one'){
					for (j=0;j<200 && j<nextelement.options.length;j++){
						if (nextelement.options[j].text=='Hidden')
							nextelement.selectedIndex = j;
					}
				}
			}
		}

		// Admin text links to set indivudual select's value.
    $('.admin-select-control').click(function() {
      targetSelect = $(this).data('select-id');
      targetValue = $(this).data('select-set');
      document.getElementById(targetSelect).value = targetValue;
  	});
		// function changeStatusSelectFeature(newsid, state){
		// 	document.getElementById('display_'+newsid).value=state;
		// }

    // Clears file input, preview, and related messages on click.
    $('body').on('click', '.clearimage', function() {
      var thisInput = $(this).closest('td').children('.fileinputwrapper').children('.fileinput')[0];
      $(thisInput).replaceWith($(thisInput).val('').clone(true));
      $(this).siblings('.previewimage').removeAttr('src');
      $(this).parent('.previewimagewrapper').css('display', 'none');
			$(this).closest('td').find('.filedimension').text('width x height');
			$(this).closest('td').find('.imagedimensions,.filedimensioncorrect,.filedimensionwarning').css('display', 'none');
    });
    // Loads preview image and related messages.
    $('body').on('change', '.fileinput', function() {
			$(this).closest('td').find('.filedimensioncorrect,.filedimensionwarning').css('display', 'none');

      var thisEl = $(this);
      var reader = new FileReader();
      reader.onload = function(e) {

				// Get image dimensions and display related messages.
				var img = new Image;
		    img.onload = function(e) {
          var imageWidth = this.width;
          var imageHeight = this.height;
					fileDimensions = imageWidth + ' x ' + imageHeight;
					$(thisEl).closest('td').find('.filedimension').text(fileDimensions);
					if ((imageWidth != 480) || (imageHeight != 320)) {
						$(thisEl).closest('td').find('.filedimensionwarning').css('display', 'block');
					} else {
						$(thisEl).closest('td').find('.filedimensioncorrect').css('display', 'block');
					}
					$(thisEl).closest('td').find('.imagedimensions').css('display', 'block');
		    };
				img.src = reader.result;

				// Get loaded data and display thumbnail.
				$(thisEl).closest('td').find('.previewimage').attr('src',e.target.result);
				$(thisEl).closest('td').find('.previewimage').css('display', 'block');
				$(thisEl).parent().siblings('.previewimagewrapper').css('display', 'inline-block');

      };
      // Read the image file as a data URL.
      reader.readAsDataURL(this.files[0]);

    });

		// Confirms before leaving blurb edit page.
		if ( ($('body').hasClass('admin') && $('.bgsearchgrey:contains("Short Version Of Blurb")').length) ) {
			var clicked = false;
			$('input[type=submit]').click(function() {
				clicked = true;
			});
			window.onbeforeunload = function(){
				if (!clicked) {
					return 'Are you sure you want to leave this page? Are updates saved?';
				}
			};
		}

		// Unknown, perhaps unused, originally in admin-header.inc.
		function forceReload() {
		    location.href = location.href + '?' + (new Date()).getTime();
		    var lastTime = location.search.substring(1) - 0;
		    if ((new Date()).getTime() - lastTime > 1000) {
		        forceReload();
		    }
		}

	});
})(jQuery);
