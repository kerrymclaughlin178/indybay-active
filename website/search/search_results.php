<?php

/**
 * @file
 * Paging newswire & search.
 */

include_once '../../classes/config/indybay.cfg';
use Indybay\Page;

$page = new Page('search_results', 'article');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  $GLOBALS['page_title'] = 'Search Results';
  $GLOBALS['body_class'] = 'page-search-results';
  include INCLUDE_PATH . '/common/content-header.inc';
  echo $page->getHtml();
  include INCLUDE_PATH . '/common/footer.inc';
}
