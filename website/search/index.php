<?php

/**
 * @file
 * Paging newswire & search.
 */

include_once '../../classes/config/indybay.cfg';
$GLOBALS['rssautodiscover'] = 'http://www.indybay.org/syn/newswire.rss';
use Indybay\Page;

$page = new Page('newswire', 'article');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  $GLOBALS['body_class'] = 'page-list';
  include INCLUDE_PATH . '/common/content-header.inc';
  echo $page->getHtml();
  include INCLUDE_PATH . '/common/footer.inc';
}
