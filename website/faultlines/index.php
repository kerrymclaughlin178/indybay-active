<?php

/**
 * @file
 * Faultlines page.
 */

$GLOBALS['page_title'] = 'Fault Lines';
$GLOBALS['page_ids'] = [54];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
