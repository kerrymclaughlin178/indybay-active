<?php

/**
 * @file
 * Health and housing page.
 */

$GLOBALS['page_title'] = 'Health, Housing, & Public Services News';
$GLOBALS['page_ids'] = [16];
include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
