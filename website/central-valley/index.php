<?php

/**
 * @file
 * Central valley page.
 */

$GLOBALS['page_title'] = 'Central Valley News';
$GLOBALS['page_ids'] = [35];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
