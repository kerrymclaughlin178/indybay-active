<?php

/**
 * @file
 * Missing page handler.
 */

include_once '../classes/config/indybay.cfg';

use Indybay\Page;

$page = new Page('missing', 'misc');

if ($page->getError()) {
  echo 'Fatal error: ', $page->getError();
}
else {
  $page->buildPage();
  // Page should have executed.
  $GLOBALS['page_display'] = 'f';
  include INCLUDE_PATH . '/common/content-header.inc';
  echo $page->getHtml();
  include INCLUDE_PATH . '/common/footer.inc';
}
