<?php

/**
 * @file
 * This is the page that handles content publishing from users.
 */

include_once '../classes/config/indybay.cfg';

$GLOBALS['body_class'] = 'page-publish page-article';
$GLOBALS['page_title'] = 'Publish to Newswire';
$GLOBALS['jquery'][] = 'validate';
$GLOBALS['js'] = ['publish', 'fileupload'];

include INCLUDE_PATH . '/common/content-header.inc';

use Indybay\Page;

$page = new Page('publish', 'article');

if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}

include INCLUDE_PATH . '/common/footer.inc';
