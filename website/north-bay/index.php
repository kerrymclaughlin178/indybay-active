<?php

/**
 * @file
 * North bay page.
 */

$GLOBALS['page_title'] = 'North Bay News';
$GLOBALS['page_ids'] = [40];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
