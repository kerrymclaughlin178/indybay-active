<?php

/**
 * @file
 * Antiwar page.
 */

$GLOBALS['page_title'] = 'Anti-War & Militarism News';
$GLOBALS['page_ids'] = [18];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
