<?php

/**
 * @file
 * Government page.
 */

$GLOBALS['page_title'] = 'Government & Elections News';
$GLOBALS['page_ids'] = [45];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
