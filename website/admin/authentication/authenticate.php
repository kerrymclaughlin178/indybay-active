<?php

/**
 * @file
 * This file is responsible for accepting a submission.
 */

// From the logon form and redirecting depending on success
// The includes for this file are not the standard includes for
// the admin pages since a user does not need to be logged on to run this code.
$GLOBALS['display'] = FALSE;
include_once '../../../classes/config/indybay.cfg';
use Indybay\Page;

$page = new Page('authenticate', 'login');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}
include INCLUDE_PATH . '/admin/admin-footer.inc';
