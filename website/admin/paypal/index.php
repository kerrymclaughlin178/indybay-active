<?php

/**
 * @file
 * This page displays the main list of admin options.
 */

use Indybay\DB\DB;

define('DB_DATABASE', 'radio');
$GLOBALS['display'] = TRUE;
include_once '../../../classes/config/indybay.cfg';
include INCLUDE_PATH . '/admin/admin-header.inc';
?>
<a href="/admin/">Admin</a> : Paypal | <small><strong><a href="labels.php">Print donor mailing labels</a></strong></small>
<br />
<table class="paypal">
<tr class="paypal">
<th class="paypal">id</th>
<th class="paypal">timestamp</th>
<th class="paypal">type</th>
<th class="paypal">currency</th>
<th class="paypal">gross</th>
<th class="paypal">fee</th>
<th class="paypal">status</th>
<th class="paypal">payment date</th>
<th class="paypal">subscr date</th>
<th class="paypal">subscr amount</th>
<th class="paypal">subscr period</th>
<th class="paypal">email</th>
<th class="paypal">first name</th>
<th class="paypal">last name</th>
<th class="paypal">payer business name</th>
<th class="paypal">item</th>
<th class="paypal">memo</th>
</tr>
<?php
$query = 'select * from chaching_paypal_ipns order by id desc';
$db_obj = new DB();
$result = $db_obj->query($query);
foreach ($result as $row) {
  echo '
<tr class="paypal">
<td class="paypal">', $row['id'], '</td>
<td class="paypal">', date(DATE_RFC2822, $row['timestamp']), '</td>
<td class="paypal" style="word-break: break-all">', htmlspecialchars($row['txn_type']), '</td>
<td class="paypal">', htmlspecialchars($row['mc_currency']), '</td>
<td class="paypal">', htmlspecialchars($row['mc_gross']), '</td>
<td class="paypal">', htmlspecialchars($row['mc_fee']), '</td>
<td class="paypal">', htmlspecialchars($row['payment_status']), '</td>
<td class="paypal">', htmlspecialchars($row['payment_date']), '</td>
<td class="paypal">', htmlspecialchars($row['subscr_date']), '</td>
<td class="paypal">', htmlspecialchars($row['amount3']), '</td>
<td class="paypal">', htmlspecialchars($row['period3']), '</td>
<td class="paypal">', htmlspecialchars($row['payer_email']), '</td>
<td class="paypal">', htmlspecialchars($row['first_name']), '</td>
<td class="paypal">', htmlspecialchars($row['last_name']), '</td>
<td class="paypal">', htmlspecialchars($row['payer_business_name']), '</td>
<td class="paypal">', htmlspecialchars($row['item_name']), '</td>
<td class="paypal">', htmlspecialchars($row['memo']), '</td>
</tr>';
}
?></table><?php
include INCLUDE_PATH . '/admin/admin-footer.inc';
