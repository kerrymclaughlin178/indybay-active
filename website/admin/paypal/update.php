<?php

/**
 * @file
 * Update donation counter.
 */

use Indybay\DB\DB;

define('DONATION_GOAL', 1800);
define('DONATION_START', '2018-10-01 00:00:00');
define('DB_DATABASE', 'radio');
include_once '../../../classes/config/indybay.cfg';
$query1 = 'SELECT SUM(mc_gross) AS amount FROM
  chaching_paypal_ipns WHERE DATE_SUB(now(), INTERVAL 30 DAY)
  <= FROM_UNIXTIME(timestamp) AND payment_status = "Completed"';
$query2 = 'SELECT SUM(mc_gross) AS amount FROM chaching_paypal_ipns WHERE payment_status = "Completed"';
$query3 = 'SELECT SUM(mc_gross) AS amount FROM chaching_paypal_ipns WHERE payment_status = "Completed" AND timestamp > UNIX_TIMESTAMP("' . DONATION_START . '")';
$db_obj = new DB();
$result = $db_obj->query($query1);
$result2 = $db_obj->query($query2);
$result3 = $db_obj->query($query3);
foreach ($result as $row) {
  $amount = sprintf('%01.2f', $row['amount']);
}
foreach ($result2 as $row) {
  $amount2 = sprintf('%01.2f', $row['amount']);
}
foreach ($result3 as $row) {
  $amount3 = round($row['amount']);
}

if ($amount != file_get_contents(CACHE_PATH . '/paypal.txt')) {
  file_put_contents(CACHE_PATH . '/paypal.txt', $amount);
}
if ($amount2 != file_get_contents(CACHE_PATH . '/paypaltotal.txt')) {
  file_put_contents(CACHE_PATH . '/paypaltotal.txt', $amount2);
}
$contents3 = '$' . number_format($amount3);
if ($contents3 != file_get_contents(CACHE_PATH . '/paypalcampaign.txt')) {
  file_put_contents(CACHE_PATH . '/paypalcampaign.txt', $contents3);
  $data['goal'] = [
    'amount' => DONATION_GOAL,
    'formatted' => '$' . number_format(DONATION_GOAL),
  ];
  $data['total'] = [
    'amount' => $amount3,
    'formatted' => '$' . number_format($amount3),
  ];
  file_put_contents(CACHE_PATH . '/paypalcampaign.json', json_encode($data));
}
