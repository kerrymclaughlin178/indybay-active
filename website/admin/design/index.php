<?php

/**
 * @file
 * Displays a list of form to edit or add a template, include or css.
 */

$GLOBALS['display'] = TRUE;
include_once '../../../classes/config/indybay.cfg';
include_once INCLUDE_PATH . '/admin/admin-header.inc';
use Indybay\Page;

$page = new Page('logedit_index', 'admin/misc');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml('logedit_index');
}
include INCLUDE_PATH . '/admin/admin-footer.inc';
