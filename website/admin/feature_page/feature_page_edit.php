<?php

/**
 * @file
 * This file displays an edit page for a category.
 */

// This only includes things like the name and description and default
// values for new stories.
$GLOBALS['display'] = TRUE;
include_once '../../../classes/config/indybay.cfg';
include_once INCLUDE_PATH . '/admin/admin-header.inc';
use Indybay\Page;

$page = new Page('feature_page_edit', 'admin/feature_page');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}
include INCLUDE_PATH . '/admin/admin-footer.inc';
