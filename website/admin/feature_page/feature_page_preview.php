<?php

/**
 * @file
 * Displays example of what the feature will look like when pushed live.
 */

// This page also includes the link to push the page live.
$GLOBALS['display'] = TRUE;
include_once '../../../classes/config/indybay.cfg';
include INCLUDE_PATH . '/admin/admin-header.inc';
use Indybay\Page;

$page = new Page('feature_page_preview', 'admin/feature_page');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
  echo $page->getHtml();
}
include INCLUDE_PATH . '/admin/admin-footer.inc';
