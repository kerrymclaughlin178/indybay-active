<?php

/**
 * @file
 * Displays a list of features for a given category.
 */

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: no-store, no-cache, must-revalidate');
header('Cache-Control: post-check=0, pre-check=0', FALSE);
header('Pragma: no-cache');

$GLOBALS['display'] = TRUE;
include_once '../../../classes/config/indybay.cfg';
include_once INCLUDE_PATH . '/admin/admin-header.inc';
use Indybay\Page;

$page = new Page('advanced_search', 'admin/advanced');
if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage();
}
include INCLUDE_PATH . '/admin/admin-footer.inc';
