<?php

/**
 * @file
 * Global justice page.
 */

$GLOBALS['page_title'] = 'Global Justice & Anti-Capitalism News';
$GLOBALS['page_ids'] = [22];

include_once '../../classes/config/indybay.cfg';

include INCLUDE_PATH . '/common/feature_page_includes.inc';
