<?php

/**
 * @file
 * This is the page that handles content publishing from users.
 */

use Indybay\Pages\Rss\RssGenerator;

header('Content-Type: application/rss+xml; charset=UTF-8');

include_once '../../classes/config/indybay.cfg';

$rss_generator = new RssGenerator();

echo $rss_generator->generate($_GET);
