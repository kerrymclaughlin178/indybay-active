<?php

/**
 * @file
 * Gives an overview of the different syndication files (per category).
 *
 * This page is served from cache to avoid database overload.
 */

$GLOBALS['display'] = TRUE;
include_once '../../classes/config/indybay.cfg';
$GLOBALS['body_class'] = 'page-subscribe';
include INCLUDE_PATH . '/common/content-header.inc';

use Indybay\Page;

$page = new Page('syndication_index', 'rss');

if ($page->getError()) {
  echo 'Fatal error: ' . $page->getError();
}
else {
  $page->buildPage('syndication_index');
  echo $page->getHtml();
}

include INCLUDE_PATH . '/common/footer.inc';
