<!-- TEMPLATE -->
<div class="webcast">
  <div class="headers">
    <div class="heading">
      <strong class="heading">{{ TPL_LOCAL_TITLE1|raw }}</strong>
    </div>
    <div class="author">
      by {{ TPL_LOCAL_DISPLAYED_AUTHOR_NAME|raw }}
      <span class="nowrap author-email">{{ TPL_LOCAL_EMAIL|raw }}</span>
      <br />
      <span class="nowrap"><em>{{ TPL_LOCAL_CREATED|raw }}</em></span>
    </div>
  </div>
  <div class="summary">
    {{ TPL_LOCAL_SUMMARY|raw }}
  </div>
  {{ TPL_LOCAL_NOMEDIA1|raw }}
  <div class="media">
    {{ TPL_LOCAL_MEDIA|raw }}
  </div>
  {{ TPL_LOCAL_NOMEDIA2|raw }}
  <div class="article">
    {{ TPL_LOCAL_TEXT|raw }}
  </div>
  <!-- END .article -->
  {% if TPL_LOCAL_RELATED_URL|length %}
  <div class="link">
    <a href="{{ TPL_LOCAL_RELATED_URL|raw }}">{{ TPL_LOCAL_SHORTENED_RELATED_LINK|raw }}</a>
  </div>
  {% endif %}
</div>
<!-- END .webcast -->
<!-- /TEMPLATE -->
