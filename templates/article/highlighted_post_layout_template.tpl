$cache_class->safeInclude("{{ TPL_LOCAL_PATH }}"."{{ TPL_LOCAL_NEWS_ITEM_ID }}_content.html");
include("{{ TPL_LOCAL_PATH }}"."{{ TPL_LOCAL_NEWS_ITEM_ID }}_attachments.inc");
?>

<?php
if (!array_key_exists("printable",$_GET) || $_GET["printable"]==""){
	?><div class="addcomment article-addcomment">
	<a href="/comment.php?top_id={{ TPL_LOCAL_NEWS_ITEM_ID }}">Add Your Comments</a>
	</div>

	<div class="comment_box_longversion">

		<?php
		if (!array_key_exists("show_comments",$_GET) || $_GET["show_comments"]==""){
				$cache_class->safeInclude("{{ TPL_LOCAL_PATH }}"."{{ TPL_LOCAL_NEWS_ITEM_ID }}_commentbox.html");
		}else{
			?>
			<hr />
			<h3><a name="comments">Comments</a>&#160;
			<small>(<a href="{{ TPL_LOCAL_NEWS_ITEM_ID }}.php">Hide Comments</a>)</small></h3>

			<?php
				include("{{ TPL_LOCAL_PATH }}"."{{ TPL_LOCAL_NEWS_ITEM_ID }}_comments.inc");
		}
		?>
	<?php
	}
	?>

	</div>
	<!-- END .comment_box_longversion -->

<?php
