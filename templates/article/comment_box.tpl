
<div class="comments_list">
<small><strong>
<a href="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}.php?show_comments=1#comments">
LATEST COMMENTS ABOUT THIS ARTICLE
</a>
</strong></small><br />
Listed below are the latest comments about this post.<br />
These comments are submitted anonymously by website visitors.<br />
            <table class="comments_list_table">
              <tr>

                <td class="comments_list_head">TITLE</td>
                <td class="comments_list_head">AUTHOR</td>
                <td class="comments_list_head">DATE</td>
              </tr>
              {{ TPL_LOCAL_LATEST_COMMENT_LINKS|raw }}
</table>
</div>
