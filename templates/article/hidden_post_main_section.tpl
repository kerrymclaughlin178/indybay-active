<!-- TEMPLATE -->
<div class="webcast">
  <div class="headers">
    <div class="heading">
      <strong class="heading"><strike>{{ TPL_LOCAL_TITLE1|raw }}</strike></strong>
    </div>
    <div class="author">
      by {{ TPL_LOCAL_DISPLAYED_AUTHOR_NAME|raw }} <em>{{ TPL_LOCAL_CREATED|raw }}</em>
    </div>
    <div class="summary">
      <strike>{{ TPL_LOCAL_SUMMARY|raw }}</strike>
    </div>
    {% if TPL_LOCAL_SHORTENED_RELATED_LINK|length %}
    <div class="link">
      <strike>{{ TPL_LOCAL_SHORTENED_RELATED_LINK|raw }}</strike>
    </div>
    {% endif %}
  </div>
  <!-- END .headers -->
</div>
<!-- END .webcast -->
<!-- /TEMPLATE -->
