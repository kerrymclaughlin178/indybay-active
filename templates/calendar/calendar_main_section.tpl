{{ TPL_LOCAL_HIDE_LINK_TO_WEEK1|raw }}
<span class="event-viewother leftrightpadding"><strong>
<a href="/calendar/?year={{ TPL_LOCAL_DISPLAYED_YEAR|raw }}&month={{ TPL_LOCAL_DISPLAYED_MONTH|raw }}&day={{ TPL_LOCAL_DISPLAYED_DAY|raw }}">View events for the week of <span class="nowrap">{{ TPL_LOCAL_DISPLAYED_MONTH|raw }}/{{ TPL_LOCAL_DISPLAYED_DAY|raw }}/{{ TPL_LOCAL_DISPLAYED_YEAR|raw }}</span></a>
</strong></span>

<br />
{{ TPL_LOCAL_HIDE_LINK_TO_WEEK2|raw }}
<table border="0" class="bgult" cellspacing="1" cellpadding="4" itemscope itemtype="http://schema.org/Event">
	<tr>
		<td class="bgsearchgrey heading" colspan="2" itemprop="name">
			{{ TPL_LOCAL_TITLE1|raw }}
		</td>
	</tr>

	<tr>
		<td class="bgsearchgrey add-link" colspan="2">
			<small><a title="You must have a program on your computer that supports iCal for this to work" href="/calendar/ical_single_item.php?news_item_id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}"><img src="/images/feed-icon-16x16.png" alt="iCal" align="top" class="ical" /> Import into your personal calendar</a></small>
		</td>
	</tr>

	<tr>
		<td class="bgaccent">
			<strong>Date</strong>
		</td>
		<td class="bgsearchgrey">
			<span>{{ TPL_LOCAL_FORMATTED_DISPLAYED_DATE|raw }}</span>
		</td>
	</tr>

	<tr>
		<td class="bgaccent">
			<strong>Time</strong>
		</td>
		<td class="bgsearchgrey">
			<span>
			<span itemprop="startDate" content="{{ TPL_LOCAL_ISO_START_TIME|raw }}">{{ TPL_LOCAL_FORMATTED_DISPLAYED_START_TIME|raw }}</span>
			-
			<span itemprop="endDate" content="{{ TPL_LOCAL_ISO_END_TIME|raw }}">{{ TPL_LOCAL_FORMATTED_DISPLAYED_END_TIME|raw }}</span>
		</span>
		</td>
	</tr>

	<tr>
		<td class="bgaccent">
			<strong>Event Type</strong>
		</td>
		<td class="bgsearchgrey">
			<span>{{ TPL_LOCAL_KEYWORD_NAME|raw }}</span>
		</td>
	</tr>

	{{ TPL_LOCAL_CONTACT_INFO_ROWS|raw }}

	<tr>
		<td class="bgaccent" colspan="2">
			<strong>Location Details</strong>
		</td>
	</tr>
	<tr itemprop="location" itemscope itemtype="http://schema.org/Place">
		<td class="event-location-summary bgsearchgrey" colspan="2" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
			{{ TPL_LOCAL_SUMMARY|raw }}
		</td>
	</tr>

	<tr>
		<td class="bgsearchgrey" colspan="2">

			<div class="article calendar-attachment">
				{{ TPL_LOCAL_TEXT|raw }}
			</div>
			<!-- END .article??? -->
			<div class="media">
				{{ TPL_LOCAL_MEDIA|raw }}
			</div>

			{% if TPL_LOCAL_RELATED_URL|length %}
			<div class="link">
				{{ TPL_LOCAL_MORE_INFO_LABEL|raw }}
				<a href="{{ TPL_LOCAL_RELATED_URL|raw }}">{{ TPL_LOCAL_SHORTENED_RELATED_LINK|raw }}</a>
			</div>
			<br/>
			{% endif %}

			<div class="addedtocalendar">
				<small>Added to the calendar on <em>{{ TPL_LOCAL_CREATED|raw }}</em>
        <meta itemprop="url" content="{{ TPL_LOCAL_ARTICLE_URL|raw }}" /></small>
			</div>

		</td>
	</tr>
</table>

{{ TPL_LOCAL_HIDE_LINK_TO_WEEK1|raw }}

{{ TPL_LOCAL_HIDE_LINK_TO_WEEK2|raw }}

<!-- /TEMPLATE -->
