<div class="storyshort blurbwrapper">
  <div class="hed">
    <div class="feature-blurb-date">
      {{ TPL_LOCAL_FORMATTED_DATE|raw }}
    </div>
    <a class="headline-text" name="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}" rel="bookmark" href="{{ TPL_LOCAL_SINGLE_ITEM_VIEW_LINK|raw }}" title="Permanent link to this story">{{ TPL_LOCAL_TITLE1|raw }}</a>
  </div>
  <div class="feature-blurb-background">
    <div class="feature-blurb-subhead">
      <!-- <div class="feature-blurb-date">
        {{ TPL_LOCAL_FORMATTED_DATE|raw }}
      </div> -->
      <a class="feature-blurb" rel="bookmark" href="{{ TPL_LOCAL_SINGLE_ITEM_VIEW_LINK|raw }}" title="Permanent link to this story">{{ TPL_LOCAL_TITLE2|raw }}</a>
    </div>
  </div>
  <div class="blurb">
    <div class="blurbtext">
      {{ TPL_LOCAL_BREAKING_NEWS|raw }}
      {{ TPL_LOCAL_SUMMARY|raw }}
      {{ TPL_LOCAL_READ_MORE_LINKS|raw }}
    </div>
  </div>
</div>
<!-- template was short_version_no_image -->
