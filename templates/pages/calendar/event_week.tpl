<table cellpadding="6" cellspacing="0" border="0" style="" class="calendar-header">
	<tr>
		<td valign="top">{{ TPL_CAL_EVENT_MONTH_VIEW_PREV|raw }}</td>
		<td align="center"><div class="eventNav">
				<span class="weeknav"><a href="event_week.php?day={{ TPL_LOCAL_LAST_WEEK_DAY|raw }}&amp;month={{ TPL_LOCAL_LAST_WEEK_MONTH|raw }}&amp;year={{ TPL_LOCAL_LAST_WEEK_YEAR|raw }}&amp;topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}&amp;news_item_status_restriction={{ TPL_LOCAL_NEWS_ITEM_STATUS_RESTRICTION|raw }}">
					{{ TPL_PREV_WEEK|raw }}
				</a></span>
				<span class="headline-text weekoftext">{{ TPL_WEEK_OF|raw }}
				{{ TPL_CAL_CUR_DATE|raw }}</span>
				<span class="weeknav"><a href="event_week.php?day={{ TPL_LOCAL_NEXT_WEEK_DAY|raw }}&amp;month={{ TPL_LOCAL_NEXT_WEEK_MONTH|raw }}&amp;year={{ TPL_LOCAL_NEXT_WEEK_YEAR|raw }}&amp;topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}&amp;news_item_status_restriction={{ TPL_LOCAL_NEWS_ITEM_STATUS_RESTRICTION|raw }}">
					{{ TPL_NEXT_WEEK|raw }}
				</a></span>
			</div>
			<div class="eventNav2">
				<span class="nowrap"><a class="calendar-addevent-link" href="/calendar/event_add.php?topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}">
				{{ TPL_CAL_ADD_AN_EVENT|raw }}</a></span>
				&#160;
				&#160;
				&#160;
				&#160;
<a href="#ical" title="Add to calendar application"><img class="ical" align="top" border="0" width="16" height="16" alt="iCal feed" src="/images/feed-icon-16x16.png" /></a>
				&#160;
				&#160;
				&#160;
				&#160;
				<span class="nowrap"><a href="/search/search_results.php?news_item_status_restriction=690690&amp;include_events=1&amp;search_date_type=displayed_date&amp;submitted_search=1&amp;topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}">
					{{ TPL_CAL_LIST_FUTURE_EVENTS|raw }}
				</a></span>
			</div>
			<form action="event_week.php" method="GET" style="">
			<div class="eventNav3">

					<strong>{{ TPL_TOPIC|raw }}:</strong>	{{ TPL_CAL_EVENT_TOPIC_DROPDOWN|raw }}

			</div>
			<div class="eventNav3">

					<strong>{{ TPL_REGION|raw }}:</strong>	{{ TPL_CAL_EVENT_LOCATION_DROPDOWN|raw }}

			</div>
			<input type="hidden" name="day" value="{{ TPL_LOCAL_DAY|raw }}"/>
			<input type="hidden" name="month" value="{{ TPL_LOCAL_MONTH|raw }}"/>
			<input type="hidden" name="year" value="{{ TPL_LOCAL_YEAR|raw }}"/>
			<input type=submit name="Filter" style="" value="{{ TPL_FILTER|raw }}" />
			</form>
</td>
		<td align="right" valign="top">{{ TPL_CAL_EVENT_MONTH_VIEW_NEXT|raw }}</td>
	</tr>
</table>

<table class="bodyClass" cellpadding="0" cellspacing="1" border="0">
	<tr>
		{{ TPL_CAL_EVENT_MONTH_DAYTITLE|raw }}
	</tr>
	{{ TPL_CAL_EVENT_MONTH_VIEW_FULL|raw }}
</table>

<a name="ical"></a>

<table class="calendar-links" cellpadding="3" cellspacing="3"><tr><th align="right"><a
href="webcal://www.indybay.org/calendar/ical_feed.php?topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}"
title="iCal feed allows you to subscribe to this calendar"><img align="top" border="0" width="16" height="16" alt="iCal feed" src="/images/feed-icon-16x16.png" class="ical" /></a> Subscribe to this calendar:</th>
<td><a href="webcal://www.indybay.org/calendar/ical_feed.php?topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}"
title="Add to iCal or compatible calendar application">All events</a></td>
<td><a
href="webcal://www.indybay.org/calendar/ical_feed.php?topic_id={{ TPL_LOCAL_TOPIC_ID|raw }}&amp;region_id={{ TPL_LOCAL_REGION_ID|raw }}&amp;news_item_status_restriction=105"
title="Add featured events to calendar application">Featured events</a></td>
</tr><tr>
<th align="right">Add this calendar to your Google Calendar:</th>
<td><a
href="http://www.google.com/calendar/render?cid=http%3A%2F%2Fwww.indybay.org%2Fcalendar%2Fical_feed.php%3Ftopic_id%3D{{ TPL_LOCAL_TOPIC_ID|raw }}%26region_id%3D{{ TPL_LOCAL_REGION_ID|raw }}"
title="Add this calendar to your Google calendar">All events</a></td>
<td><a
href="http://www.google.com/calendar/render?cid=http%3A%2F%2Fwww.indybay.org%2Fcalendar%2Fical_feed.php%3Ftopic_id%3D{{ TPL_LOCAL_TOPIC_ID|raw }}%26region_id%3D{{ TPL_LOCAL_REGION_ID|raw }}%26news_item_status_restriction%3D105"
title="Add featured events to your Google calendar">Featured events</a></td></tr></table>
