<!-- publish form part 1 -->
{{ TPL_HIDE1|raw }}
{{ TPL_LOCAL_DISPLAY_PREVIEW|raw }}
{{ TPL_HIDE2|raw }}

<a name="publishform"></a>

<form enctype="multipart/form-data" method="post" action="/calendar/event_add.php" accept-charset="UTF-8" id="publish-form">

  {{ TPL_VALIDATION_MESSAGES|raw }}
  {{ TPL_STATUS_MESSAGES|raw }}
  {{ TPL_HIDE1|raw }}

  <!-- publish form part 1 -->
  <table border="0" cellspacing="0" cellpadding="1" class="bgaccent roundtop">
    <tr>
      <td class="bgaccent">

        <span class="extraspaceleftsm">
          <strong>{{ TPL_PUB_STEPONE|raw }}</strong>
        </span>

        <table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
          <tr>
            <td class="publish-wrapper">

              <table class="publish-table" width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr class="bgpenult">
                  <td width="25%">
                    <strong>Event Title</strong>
                    <small>({{ TPL_REQUIRED|raw }})</small>
                  </td>
                  <td width="75%">
                    <input type="text" name="title1" id="title1" placeholder="Use Title Case for Titles" maxlength="90" value="{{ TPL_LOCAL_TITLE1|raw }}" />
                  </td>
                </tr>

                <tr class="bgpenult">
                  <td>
                    <strong>Organizer/Author</strong>
                    <small>({{ TPL_REQUIRED|raw }})</small>
                  </td>
                  <td>
                    <input type="text" name="displayed_author_name" id="displayed_author_name" size="25" maxlength="45" value="{{ TPL_LOCAL_DISPLAYED_AUTHOR_NAME|raw }}" />
                  </td>
                </tr>

                <tr>
                  <td>
                    <span class="subheader"><strong>Contact Info</strong></span>
                  </td>
                  <td></td>
                </tr>

                <tr>
                  <td>
                    {{ TPL_CONTACT_EMAIL|raw }}
                  </td>
                  <td><input type="text" name="email" size="25" maxlength="45" value="{{ TPL_LOCAL_EMAIL|raw }}" />
                  </td>
                </tr>

                <tr>
                  <td>
                    {{ TPL_CONTACT_PHONE|raw }}
                  </td>
                  <td>
                    <input type="text" name="phone" size="25" maxlength="45" value="{{ TPL_LOCAL_PHONE|raw }}" />
                  </td>
                </tr>

                <!-- <tr>
                  <td>
                    {{ TPL_CONTACT_ADDRESS|raw }}
                  </td>
                  <td>
                    <input type="text" name="address" size="45" maxlength="45" value="{{ TPL_LOCAL_ADDRESS|raw }}" />
                    <br />
                  </td>
                </tr> -->

                <tr>
                  <td></td>
                  <td>
                    <span class="nowrap">
                      <label for="display_contact_info">Display contact info?</label>
                      {{ TPL_LOCAL_CHECKBOX_DISPLAY_CONTACT_INFO|raw }}
                    </span>
                    <br/>
                    <small>
                      {{ TPL_DISPLAY_ADDITIONAL_ADDITIONAL_TEXT|raw }}
                    </small>
                  </td>
                </tr>

                <tr>
                  <td colspan="2">
                    <hr size="1" noshade />
                  </td>
                </tr>

                <tr>
                  <td>Event Topic</td>
                  <td>{{ TPL_CAT_TOPIC_SELECT|raw }}</td>
                </tr>

                <tr>
                  <td>Event Region</td>
                  <td>{{ TPL_CAT_REGION_SELECT|raw }}</td>
                </tr>

                <tr>
                  <td>Event Type</td>
                  <td>{{ TPL_CAT_TYPE_SELECT|raw }}</td>
                </tr>

                <tr class="bgpenult dateselects">
                  <td class="vtop">
                    <strong>Event Date</strong>
                    <small>({{ TPL_REQUIRED|raw }})</small>
                  </td>
                  <td>
                    {{ TPL_CAL_SELECT_DATE_DISPLAYED_DATE|raw }}
                  </td>
                </tr>

                <tr class="bgpenult dateselects">
                  <td>
                    <strong>Event Duration</strong> <small>({{ TPL_REQUIRED|raw }})</small>
                  </td>
                  <td>
                    {{ TPL_LOCAL_SELECT_DURATION|raw }} (hours:minutes)
                  </td>
                </tr>

                <tr>
                  <td class="vtop">
                    <strong>Location Details</strong> <small>({{ TPL_REQUIRED|raw }})</small>
                  </td>
                  <td>
                    <!-- this needs to be broken down into specific location parts,
                          as per ticket #202 -->
                    <textarea name="summary" rows="3" style="" wrap="virtual">{{ TPL_LOCAL_SUMMARY|raw }}</textarea>
                    <br />
                  </td>
                </tr>

                <tr>
                  <td>{{ TPL_EVENT_LINK_TO_MORE_DETAILS|raw }}</td>
                  <td>
                    <input type="text" name="related_url" style="" maxlength="255" value="{{ TPL_LOCAL_RELATED_URL|raw }}" />
                  </td>
                </tr>

              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <!-- publish form part 2 -->
  <table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
    <tr>
      <td class="bgaccent">

        <span class="extraspaceleftsm">
          <strong>{{ TPL_PUB_STEPTWO|raw }}</strong>
        </span>

        <table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
          <tr>
            <td class="publish-wrapper">

              <table class="publish-table" width="100%" border="0" cellspacing="2" cellpadding="4">
                <tr class="bgpenult">

                  <td width="25%" class="vtop">

                    <strong>{{ TPL_EVENT_DESCRIPTION|raw }}</strong>
                    <br />
                    <small>
                      <span class="rednote"><strong>Tip:</strong></span>
                      <a href="/notice/legibility-note.html" title="Click to see an example" target="blank">use spaces between paragraphs</a>.
                    </small>
                  </td>
                  <td width="75%">
                    <textarea name="text" rows="10" style="" wrap="virtual">{{ TPL_LOCAL_TEXT|raw }}</textarea>
                    <br />
                    <span class="nowrap"><label for="is_text_html">{{ TPL_IS_EVENT_DESCRIPTION_HTML|raw }}</label> {{ TPL_LOCAL_CHECKBOX_IS_TEXT_HTML|raw }}</span>
                    <p><small>For plain text, URLs will be converted to clickable links. You may also use &lt;a href=&quot;url&quot;&gt;visible linked text&lt;/a&gt; in plain text articles (other tags will be ignored unless you select &quot;HTML&quot;). When selecting &quot;HTML&quot;, you <strong>must</strong> provide &lt;p&gt;&lt;/p&gt; and &lt;br /&gt; tags for paragraphs and line breaks.</small></p>
                  </td>
                </tr>

              </table>

            </td>
          </tr>
        </table>

      </td>
    </tr>
  </table>

  <!-- publish form part 3 -->
  <table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
    <tr>
      <td class="bgaccent">

        <span class="extraspaceleftsm">
          <strong>{{ TPL_PUB_STEPTHREE|raw }}</strong>
        </span>

        <table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
          <tr class="bgpenult">
            <td>

              <table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
                <tr>
                  <td class="publish-wrapper">

                    <table class="publish-table" width="100%" border="0" cellspacing="2" cellpadding="4">
                      <tr class="bgpenult">

                        <td width="25%" class="vtop">
                          <strong>How many files to upload?</strong>
                        </td>
                        <td width="75%">

                          <div id="upload_warning"><small>(A Title, Author and Location Must Be Entered Before You Can Choose Uploads)</small><br /></div>

                          <span>{{ TPL_SELECT_FILECOUNT|raw }}</span>

                          <input type="submit" name="action" value="Enter" id="upload_submit_button">

                          <div class="clicktip">

                            <p class="trigger" title="click here to learn more">Please note the allowed file types and file sizes!</p>

                            <div class="target">
                              {{ TPL_MAX_UPLOAD|raw }} {{ TPL_UPLOAD_MAX_FILESIZE|raw }}; {{ TPL_TOTAL_LIMIT|raw }} {{ TPL_POST_MAX_SIZE|raw }}; {{ TPL_MAX_EXECUTION_TIME|raw }} {{ TPL_HOURS|raw }}
                              <br />
                              {{ TPL_ACCEPTED_TYPES|raw }}
                              <br />
                              <strong>For playability of media files on mobile devices</strong>, upload videos as MP4s with H.264 encoding. For audio, upload MP3s.
                            </div><!-- END .target -->

                          </div><!-- END .clicktip -->

                        </td>
                      </tr>
                    </table>

                    <div id="file_boxes2"></div>

                  </td>
                </tr>
              </table>

            </td>
          </tr>
        </table>

        <div id="nonjscript_file_boxes">
          {{ TPL_FILE_BOXES|raw }}
        </div>

      </td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent roundbot">
    <tr>
      <td class="bgaccent">

        <span class="extraspaceleftsm">
          <strong>CAPTCHA</strong>
        </span>

        <table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent roundbot">
          <tr class="bgpenult">
            <td>

              <table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
                <tr>
                  <td class="publish-wrapper">

                    <table class="publish-table" width="100%" border="0" cellspacing="2" cellpadding="4">
                      <tr class="bgpenult">
                        <td width="25%" class="vtop">
                          <strong>Anti-spam question</strong>
                          <small>({{ TPL_REQUIRED|raw }})</small>
                        </td>
                        <td width="75%">{{ TPL_CAPTCHA_FORM|raw }}</td>
                      </tr>
                    </table>

                  </td>
                </tr>
              </table>

            </td>
          </tr>
        </table>

      </td>
    </tr>
  </table>

  <div class="publish-buttons" style="">
    <span id="preview_button">{{ TPL_LOCAL_PREVIEW|raw }}</span>
    <input type="submit" name="publish" value="Publish" />
    <!-- <br /><br /> -->
  </div>

  {{ TPL_HIDE2|raw }}

</form>

<div id="files_select_template" style="visibility: hidden;">

  <hr size="1" noshade />

  <table class="publish-table" width="100%" border="0" cellspacing="2" cellpadding="4">
    <tr>
      <td width="25%"><strong>Title #::uploadnum::</strong>
      </td>
      <td width="75%">
        <input size="25" maxlength="90" type="text" name="linked_file_title_::uploadnum::" value="" />
      </td>
    </tr>
    <tr>
      <td><strong>Upload #::uploadnum::</strong></td>
      <td>
        <div class="fileinputwrapper">
          <input class="fileinput" type="file" name="linked_file_::uploadnum::" {{ TPL_FILE_ACCEPT|raw }} />
        </div>
        <div class="previewimagewrapper">
          <div class="clearimage" title="Clear this attachment">×</div>
          <div class="nopreview">Previews not available for <span class="filetype">media</span> files.</div>
          <img class="previewimage" />
        </div>
      </td>
    </tr>
    <!--endoffirstupload-->
    <tr>
      <td>Optional Text #::uploadnum::</td>
      <td><textarea name="linked_file_comment_::uploadnum::" rows="3" cols="50" wrap="virtual"></textarea></td>
    </tr>
  </table>

</div>

<div id="files_select_template_1" style="visibility: hidden;">

  <table class="publish-table" width="100%" border="0" cellspacing="2" cellpadding="4">
    <tr>
      <td width="25%"><strong>Upload #1</strong>
      </td>
      <td width="75%">
        <div class="fileinputwrapper">
          <input class="fileinput" type="file" name="linked_file_1" {{ TPL_FILE_ACCEPT|raw }} />
        </div>
        <div class="previewimagewrapper">
          <div class="clearimage" title="Clear this attachment">×</div>
          <div class="nopreview">Previews not available for <span class="filetype">media</span> files.</div>
          <img class="previewimage" />
        </div>
      </td>
    </tr>
  </table>

</div>

<!-- end publish template -->
