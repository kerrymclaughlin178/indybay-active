&copy; 2000-2003 {{ TPL_SITE_NAME|raw }}. Unless otherwise stated by
the author, all content is free for non-commercial reuse, reprint, and
rebroadcast, on the net and elsewhere. Opinions are those of the
contributors and are not necessarily endorsed by the {{ TPL_SITE_CRUMB|raw }}. 
<a href="/process/disclaimer.php">{{ TPL_LANG_DISCLAIMER|raw }}</a> | 
<a href="/process/privacy.php">{{ TPL_LANG_PRIVACY|raw }}</a>
