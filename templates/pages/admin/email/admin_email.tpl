
<h3>{{ TPL_MAILING_LIST_ADMIN|raw }}</h3>


{{ TPL_LOCAL_SUCCESS|raw }}

<form name="mailinglist" method="post" action="admin_email.php">
<strong>{{ TPL_TO|raw }}: {{ TPL_LOCAL_TO_DROPDOWN|raw }}</strong><br /><br />
<strong>{{ TPL_FROM|raw }}: {{ TPL_LOCAL_FROM_DROPDOWN|raw }}</strong><br /><br />
<strong>{{ TPL_SUBJECT|raw }}:</strong><br />
<input type="hidden" name="action" value="send" />
<input type="text" name="msubject" value="{{ TPL_LOCAL_MSUBJECT|raw }}" size="40" />

<br /><br />

<strong>{{ TPL_MESSAGE_BODY|raw }}:</strong>
<br />
<input type="submit" name="generate" value="Generate Weekly Indybay Email" />
&#160;
<input type="submit" name="generate_sc" value="Generate SC Email" />
<br />
<textarea name="mbody" rows="24" cols="100">
{{ TPL_LOCAL_TEXTAREA|raw }}
</textarea>

<br /><br />

{{ TPL_CHECK_HERE|raw }}: <input type="checkbox" name="failsafe" value="1" />

<br /><br />

<input type="submit" name="Submit" value="{{ TPL_SEND|raw }}" />
</form>
