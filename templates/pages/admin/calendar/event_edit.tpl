<strong>
{{ TPL_VALIDATION_MESSAGES|raw }}
{{ TPL_STATUS_MESSAGES|raw }}
</strong>
<!-- article_edit template -->

<form action="/admin/calendar/event_edit.php" method="post">

<input type="hidden" name="editswitch" value="1" />

<input type="hidden" name="news_item_id" value="{{ TPL_LOCAL_ID|raw }}" />

<table border="0" cellspacing="1" cellpadding="1" class="bgsearchgrey">
<tr><td colspan="2"><input type=submit name="save" value="save" />
<input type=submit name="save_and_regenerate_all" value="save & regenerate comments" />
&#160; &#160; 
<input type=submit name="blurbify" value="Create Blurb From Post" />
{{ TPL_PUBLISH_RESULT|raw }}
</td></tr>

<tr><td colspan=2>{{ TPL_ID|raw }}:&#160;
{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}&#160;
{{ TPL_VERSION_ID|raw }}:&#160;
{{ TPL_LOCAL_NEWS_ITEM_VERSION_ID|raw }}
</td></tr>

<tr><td valign="top">
<hr />
<small>{{ TPL_CREATED|raw }}:

{{ TPL_LOCAL_CREATION_DATE|raw }} 
<br />
{{ TPL_LOCAL_CREATED_BY_USER_INFO|raw }}
</small>
<hr />
</td><td valign="top">
<hr />
<small>
{{ TPL_MODIFIED|raw }}:
{{ TPL_LOCAL_VERSION_CREATION_DATE|raw }}
<br />
{{ TPL_LOCAL_LAST_UPDATED_BY_USER_INFO|raw }}
</small>
<hr />
</td></tr>
<tr><td colspan=2>

</td></tr>
<tr><td>{{ TPL_TITLE|raw }}:</td>
<td><input type="text" name="title1" size="70" value="{{ TPL_LOCAL_TITLE1|raw }}" /></td></tr>
<tr><td>{{ TPL_DISPLAYED_DATE|raw }}:</td>
<td colspan=2>{{ TPL_LOCAL_SELECT_DATE_DISPLAYED_DATE|raw }}</td></tr>

<tr><td>{{ TPL_EVENT_DURATION|raw }}:</td>
<td><input type="text" name="event_duration" size="15" value="{{ TPL_LOCAL_EVENT_DURATION|raw }}" /></td></tr>

<tr><td colspan=2>&#160;</td></tr>

<tr><td>{{ TPL_EVENT_TYPE|raw }}:</td>
<td>{{ TPL_CAT_TYPE_SELECT|raw }}</td></tr>

<tr><td colspan=2>&#160;</td></tr>

<tr><td>{{ TPL_CONTACT_NAME|raw }}:</td>
<td><input type="text" name="displayed_author_name" size="70" value="{{ TPL_LOCAL_DISPLAYED_AUTHOR_NAME|raw }}" /></td></tr>
<tr><td>{{ TPL_EMAIL|raw }}:</td>
<td><input type="text" name="email" size="70" value="{{ TPL_LOCAL_EMAIL|raw }}" /></td></tr>
<tr><td>{{ TPL_PHONE|raw }}:</td>
<td><input type="text" name="phone" size="70" value="{{ TPL_LOCAL_PHONE|raw }}" /></td></tr>
<tr><td>{{ TPL_ADDRESS|raw }}:</td>
<td><input type="text" name="address" size="70" value="{{ TPL_LOCAL_ADDRESS|raw }}" /></td></tr>

<tr><td colspan=2>{{ TPL_SHOW_CONTACT_INFO|raw }} {{ TPL_LOCAL_CHECKBOX_DISPLAY_CONTACT_INFO|raw }}</td></tr>

<tr><td colspan=2>&#160;</td></tr>


<tr><td>
{{ TPL_DISPLAYSTAT|raw }}:
</td><td>
{{ TPL_SELECT_DISPLAY|raw }}
&#160;
{{ TPL_ARTTYPE|raw }}:&#160;

{{ TPL_SELECT_NEWS_ITEM_TYPE_ID|raw }}
</td></tr>

<input type="hidden" name="old_display" value="{{ TPL_LOCAL_OLD_DISPLAY|raw }}">


<tr><td colspan="2">{{ TPL_LOCATION_INFORMATION|raw }}:<br />
<textarea name="summary" style="width: 99%" rows="5">{{ TPL_LOCAL_SUMMARY|raw }}</textarea></td></tr>
<tr><td colspan=2>{{ TPL_IS_SUMMARY_HTML|raw }} {{ TPL_LOCAL_CHECKBOX_IS_SUMMARY_HTML|raw }}
</td></tr>
<tr><td colspan="2">{{ TPL_DESCRIPTION|raw }}:<br />
<textarea name="text" style="width: 99%" rows="20">{{ TPL_LOCAL_TEXT|raw }}</textarea></td></tr>
<tr><td colspan=2>{{ TPL_IS_TEXT_HTML|raw }} {{ TPL_LOCAL_CHECKBOX_IS_TEXT_HTML|raw }}
</td></tr>
<tr><td>{{ TPL_LINK|raw }}:</td>
<td><input type="text" name="related_url" size="70" value="{{ TPL_LOCAL_RELATED_URL|raw }}" /></td></tr>

<tr><td valign="top">{{ TPL_PAGE_LIST|raw }}:</td>
<td>
{{ TPL_LOCAL_CHECKBOX_REGION|raw }}
<br />
{{ TPL_LOCAL_CHECKBOX_TOPIC|raw }}
<br />
{{ TPL_LOCAL_CHECKBOX_INT|raw }}
<br />
{{ TPL_LOCAL_CHECKBOX_OTHER|raw }}
<br />
</td></tr>
<tr><td colspan="2">&#160;</td></tr>
<tr><td colspan="2"></td></tr>
<tr><td colspan=2>
<table border=1>
<tr>
<td>{{ TPL_PARENT_ID|raw }}:</td>
<td>{{ TPL_MEDIA_ATTACHMENT_ID|raw }}:</td>
<td>{{ TPL_THUMBNAIL_MEDIA_ATTACHMENT_ID|raw }}:</td>
</tr>
<tr>
<td><input type="text" name="parent_item_id" size="10" value="{{ TPL_LOCAL_PARENT_ID|raw }}" /></td>
<td><input type="text" size="10"  name="media_attachment_id" value="{{ TPL_LOCAL_MEDIA_ATTACHMENT_ID|raw }}" /></td>
<td><input type="text" size="10"  name="thumbnail_media_attachment_id" value="{{ TPL_LOCAL_THUMBNAIL_MEDIA_ATTACHMENT_ID|raw }}" /></td>
</table>
</td>
</tr>
<tr><td colspan="2">&#160;</td></tr>
<tr><td colspan="2"></td></tr>
<tr>
<td colspan=2>

<input type=submit name="save2" value="save">
</td></tr>
<tr><td colspan=2>
{{ TPL_LOCAL_PREVIOUS_VERSION_LIST|raw }}
</td></tr>
</table>
<input type="hidden" name="timestamp" value="{{ TPL_LOCAL_MICROTIME|raw }}">
</form>
<br />
<strong>
Current Post:
<a href="{{ TPL_LOCAL_PUBLISH_LINK|raw }}">
{{ TPL_LOCAL_PUBLISH_LINK|raw }}
</a>
</strong>
<br />
{{ TPL_LOCAL_DISPLAY_PREVIEW|raw }}


<!-- end article_edit template -->

