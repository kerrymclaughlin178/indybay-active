<h3>{{ TPL_SITE_DESIGN|raw }}</h3>
<table border=3 cellspacing=5 cellpadding=5>

<tr valign="top">
<td>
Test Files
</td>
<td>
CSS Files (real not test)
</td>
<td>
Include Files (real not test)
</td>
<td>
Template Files (real not test)
</td>
</tr>
<tr valign="top">
<td width="25%">
{{ TPL_TEST_LIST|raw }}
</td>
<td width="25%">
{{ TPL_CSS_LIST|raw }}
</td>
<td width="25%">
{{ TPL_INCLUDE_LIST|raw }}
</td>
<td width="25%">
{{ TPL_TEMPLATE_LIST|raw }}
</td>
</tr>

</table>
