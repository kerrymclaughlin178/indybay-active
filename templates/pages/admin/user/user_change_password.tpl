

<Form name="user_change_password" method="post" action="user_change_password.php">
<input type="hidden" name="user_id" value="{{ TPL_LOCAL_USER_ID|raw }}">


<strong><a href="user_edit.php?user_id={{ TPL_LOCAL_USER_ID|raw }}">{{ TPL_USER_EDIT_DETAIL|raw }}</a></strong>
<TABLE>
{{ TPL_STATUS_MESSAGES|raw }}
{{ TPL_VALIDATION_MESSAGES|raw }}

<TR><TD>{{ TPL_USERNAME|raw }}</TD><TD>
{{ TPL_LOCAL_USERNAME|raw }}
</TD></TR>
<TR><TD>{{ TPL_EMAIL|raw }}</TD><TD>
{{ TPL_LOCAL_EMAIL|raw }}
</TD></TR>

<TR><TD>{{ TPL_OLD_PASSWORD|raw }}</TD><TD>
<INPUT type="password" NAME="old_password" VALUE="" />
</TD></TR>

<TR><TD>{{ TPL_NEW_PASSWORD|raw }}</TD><TD>
<INPUT type="password" NAME="new_password1" VALUE="" />
</TD></TR>

<TR><TD>{{ TPL_REPEAT_NEW_PASSWORD|raw }}</TD><TD>
<INPUT type="password" NAME="new_password2" VALUE="" />
</TD></TR>
</TABLE>

<INPUT type=submit name="change_password" value="{{ TPL_CHANGE_PASSWORD|raw }}" />


</FORM>
