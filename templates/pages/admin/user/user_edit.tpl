
<Form name="user_edit" method="post" action="user_edit.php">
<input type="hidden" name="user_id" value="{{ TPL_LOCAL_USER_ID|raw }}">

<TABLE>

<TR><TD>{{ TPL_USERNAME|raw }}</TD><TD>
{{ TPL_LOCAL_USERNAME|raw }}
</TD></TR>

<TR><TD colspan=2>{{ TPL_HAS_LOGIN_RIGHTS|raw }}
{{ TPL_LOCAL_CHECKBOX_HAS_LOGIN_RIGHTS|raw }}
</TD></TR>

<TR><TD>{{ TPL_EMAIL|raw }}</TD><TD>
<INPUT NAME="email" VALUE="{{ TPL_LOCAL_EMAIL|raw }}" />
</TD></TR>

<TR><TD>{{ TPL_PHONE|raw }}</TD><TD>
<INPUT NAME="phone" VALUE="{{ TPL_LOCAL_PHONE|raw }}" />
</TD></TR>

<TR><TD>{{ TPL_FIRST_NAME|raw }}</TD><TD>
<INPUT NAME="first_name" VALUE="{{ TPL_LOCAL_FIRST_NAME|raw }}" />
</TD></TR>

<TR><TD>{{ TPL_LAST_NAME|raw }}</TD><TD>
<INPUT NAME="last_name" VALUE="{{ TPL_LOCAL_LAST_NAME|raw }}" />
</TD></TR>
<TR><TD>{{ TPL_LAST_LOGIN|raw }}</TD><TD>
{{ TPL_LOCAL_LAST_LOGIN|raw }}
</TD></TR>
</TABLE>
<small>{{ TPL_LOCAL_CREATION_INFO|raw }}</small>
<br />

<INPUT type=submit name="save" value="{{ TPL_SAVE|raw }}" />
|
<strong><a href="user_change_password.php?user_id={{ TPL_LOCAL_USER_ID|raw }}">{{ TPL_CHANGE_PASSWORD|raw }}</a></strong>
</TD></TR></TABLE>

</FORM>
