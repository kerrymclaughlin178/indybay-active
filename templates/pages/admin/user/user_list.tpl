<strong>
<a href="user_edit.php?user_id={{ TPL_LOCAL_SESSION_USER_ID|raw }}">{{ TPL_USER_EDIT_OWN|raw }}</a>

|
<a href="user_change_password.php?user_id={{ TPL_LOCAL_SESSION_USER_ID|raw }}">{{ TPL_CHANGE_OWN_PASSWORD|raw }}</a><br /><br />

</strong>



<strong>Enabled Users:</strong><br /><br />
Users Who Have Logged In In The Past 15 Days
<TABLE class="bgsearchgrey">
<tr ><td>
<table >
<td>User ID</td><td>Username</td><td>First Name</td><td>Last Name</td>
<td>Email</td><td>Last Login</td><td>Last Admin Activity</td></tr>

{{ TPL_ENABLED_USERS_RECENT|raw }}
</td></tr>
</table>
</TABLE>
<br />
Users Who Have Not Logged In In The Past 15 Days
<TABLE class="bgsearchgrey">
<tr ><td>
<table >
<td>User ID</td><td>Username</td><td>First Name</td><td>Last Name</td>
<td>Email</td><td>Last Login</td><td>Last Admin Activity</td></tr>

{{ TPL_ENABLED_USERS_NONRECENT|raw }}
</td></tr>
</table>
</TABLE>
<br /><br />

<strong>Users Without Login Rights:</strong>
<br />
<TABLE class="bgsearchgrey">
<tr ><td>
<table >
<td>User ID</td><td>Username</td><td>First Name</td><td>Last Name</td>
<td>Email</td><td>Last Login</td></tr>

{{ TPL_DISABLED_USERS|raw }}
</td></tr>
</table>
</TABLE>
<br />
<a href="user_add.php">{{ TPL_USER_ADD|raw }}</a><br /><br />