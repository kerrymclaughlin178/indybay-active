<TABLE>

<TR><TD>{{ TPL_USERNAME|raw }}</TD><TD>
{{ TPL_LOCAL_USERNAME|raw }}
</TD></TR>

<TR><TD colspan=2>{{ TPL_HAS_LOGIN_RIGHTS|raw }}
{{ TPL_LOCAL_HAS_LOGIN_RIGHTS|raw }}
</TD></TR>

<TR><TD>{{ TPL_EMAIL|raw }}</TD><TD>
{{ TPL_LOCAL_EMAIL|raw }}
</TD></TR>

<TR><TD>{{ TPL_PHONE|raw }}</TD><TD>
{{ TPL_LOCAL_PHONE|raw }}
</TD></TR>

<TR><TD>{{ TPL_FIRST_NAME|raw }}</TD><TD>
{{ TPL_LOCAL_FIRST_NAME|raw }}
</TD></TR>

<TR><TD>{{ TPL_LAST_NAME|raw }}</TD><TD>
{{ TPL_LOCAL_LAST_NAME|raw }}
</TD></TR>
<TR><TD>{{ TPL_LAST_LOGIN|raw }}</TD><TD>
{{ TPL_LOCAL_LAST_LOGIN|raw }}
</TD></TR>
</TABLE>
<small>{{ TPL_LOCAL_CREATION_INFO|raw }}</small>
<br />

<a href="user_edit.php?user_id={{ TPL_LOCAL_USER_ID|raw }}">{{ TPL_EDIT|raw }}</a>

</TD></TR></TABLE>
