
{{ TPL_VALIDATION_MESSAGES|raw }}
{{ TPL_STATUS_MESSAGES|raw }}

<Form name="user_edit" method="post" action="user_add.php">
<input type="hidden" name="is_posted" value="1">

<TABLE>

<TR><TD>{{ TPL_USERNAME|raw }}</TD><TD>
<INPUT NAME="username" VALUE="{{ TPL_LOCAL_USERNAME|raw }}" />
</TD></TR>

<TR><TD colspan=2>{{ TPL_HAS_LOGIN_RIGHTS|raw }}
{{ TPL_LOCAL_CHECKBOX_HAS_LOGIN_RIGHTS|raw }}
</TD></TR>

<TR><TD>{{ TPL_EMAIL|raw }}</TD><TD>
<INPUT NAME="email" VALUE="{{ TPL_LOCAL_EMAIL|raw }}" />
</TD></TR>

<TR><TD>{{ TPL_PHONE|raw }}</TD><TD>
<INPUT NAME="phone" VALUE="{{ TPL_LOCAL_PHONE|raw }}" />
</TD></TR>

<TR><TD>{{ TPL_FIRST_NAME|raw }}</TD><TD>
<INPUT NAME="first_name" VALUE="{{ TPL_LOCAL_FIRST_NAME|raw }}" />
</TD></TR>

<TR><TD>{{ TPL_LAST_NAME|raw }}</TD><TD>
<INPUT NAME="last_name" VALUE="{{ TPL_LOCAL_LAST_NAME|raw }}" />
</TD></TR>
<TR><TD>&#160;</TD><TD>
&#160;
</TD></TR>

<TR><TD>{{ TPL_PASSWORD1|raw }}</TD><TD>
<INPUT type="PASSWORD" NAME="new_password1"  />
</TD></TR>

<TR><TD>{{ TPL_PASSWORD2|raw }}</TD><TD>
<INPUT type="PASSWORD" NAME="new_password2"/>
</TD></TR>


</TABLE>
<br />

<INPUT type=submit name="add" value="Add" />
</TD></TR></TABLE>

</FORM>
