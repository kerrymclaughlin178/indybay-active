<!-- publish form part 1 -->

<a name="publishform"></a>
<form enctype="multipart/form-data" method="post" action="/admin/feature_page/blurb_add.php">

{{ TPL_VALIDATION_MESSAGES|raw }}
{{ TPL_STATUS_MESSAGES|raw }}

<table width="80%" border="0" cellspacing="2" cellpadding="4" class="bgsearchgrey">
<tr  valign="top">

<td width="25%">
<strong>{{ TPL_TITLE1|raw }}</strong>
</td>

<td width="75%">
<input type="text" name="title1" style="width: 90%" maxlength="90" value="{{ TPL_LOCAL_TITLE1|raw }}" />
</td>

</tr>

<tr valign="top">

<td width="25%">
<strong>{{ TPL_TITLE2|raw }}</strong> <small>({{ TPL_REQUIRED|raw }})</small>
</td>

<td width="75%">
<input type="text" name="title2" style="width: 90%" maxlength="90" value="{{ TPL_LOCAL_TITLE2|raw }}" />
</td>

</tr>

<tr><td><strong>{{ TPL_TOPIC|raw }}:</strong></td><td>{{ TPL_CAT_TOPIC_SELECT|raw }}<td></td></tr>
<tr><td><strong>{{ TPL_REGION|raw }}:</strong></td><td>{{ TPL_CAT_REGION_SELECT|raw }}<td></td></tr>
<tr><td><strong>{{ TPL_OTHER|raw }}:</strong></td><td>{{ TPL_CAT_OTHER_SELECT|raw }}<td></td></tr>
</table>


<br /><br />
<table class="bgsearchgrey" width="75%">
<tr>
<td  colspan=2>
<strong>{{ TPL_SHORT_VERSION|raw }}</strong>
</td></tr>
<tr><td colspan=2>
<textarea name="summary" rows="6" style="width: 90%" wrap="virtual">{{ TPL_LOCAL_SUMMARY|raw }}</textarea>
<br />
</td>

</tr>

<tr><td coslpan=2><strong>{{ TPL_IS_SHORT_VERSION_HTML|raw }}</strong> {{ TPL_LOCAL_CHECKBOX_IS_SUMMARY_HTML|raw }}
</td></tr>

<tr ><td colspan=2><hr/></td></tr>
<tr ><td colspan=2>
  <strong>{{ TPL_THUMBNAIL_MEDIA_ATTACHMENT|raw }}:</strong><br />
  <small>
  {{ TPL_IMAGE_SELECT_DESC|raw }}
  </small>
  <br />
  {{ TPL_ENTER_ID|raw }}
  <input type="text" name="thumbnail_media_attachment_id" size=10 maxlength="15" value="{{ TPL_LOCAL_THUMBNAIL_MEDIA_ATTACHMENT_ID|raw }}" />
  <br />
  {{ TPL_OR|raw }}
  <br />
  {{ TPL_CHOOSE_RECENT_ADMIN_UPLOAD|raw }}
  {{ TPL_LOCAL_THUMBNAIL_RECENT_ADMIN_UPLOADS_SELECT|raw }}
  <br />
  {{ TPL_OR|raw }}
  <br />
  <!-- <input type="file" name="thumbnail_upload"> -->
  {{ TPL_UPLOAD_A_FILE|raw }}
  <div class="fileinputwrapper">
    <input class="fileinput" type="file" name="thumbnail_upload" accept=".gif,.jpeg,.jpg,.png" />
  </div>
  <div class="previewimagewrapper">
    <div class="clearimage" title="Clear this attachment">×</div>
    <img class="previewimage" />
  </div>
  <div class="imagedimensions">
    Image is <span class="filedimension">width x height</span>.
    <span class="filedimensioncorrect">Perfect!</span>
    <span class="filedimensionwarning">It should be 480 x 320.</span>
  </div>
</td>
</tr>
<tr ><td colspan=2><hr /></td></tr>
</table>


<br /><br />
<table class="bgsearchgrey" width="75%">
<tr>
<td coslpan=2 >
<strong>{{ TPL_LONG_VERSION|raw }}</strong>
</td>
</tr>
<tr>
<td colspan=2>
<textarea name="text" rows="16" style="width: 90%" wrap="virtual">{{ TPL_LOCAL_TEXT|raw }}</textarea>
</td>

</tr>

<tr><td colspan=2><strong>{{ TPL_IS_LONG_VERSION_HTML|raw }}</strong> {{ TPL_LOCAL_CHECKBOX_IS_TEXT_HTML|raw }}
</td></tr>
<tr >

<td width="25%"><strong>{{ TPL_PUB_URL|raw }}</strong></td>

<td width="75%">
<input type="text" name="related_url" style="width: 90%" maxlength="255" value="{{ TPL_LOCAL_RELATED_URL|raw }}"/>
</td>

</tr>

<tr ><td colspan=2><hr/></td></tr>
<tr ><td colspan=2>
  <strong>{{ TPL_MEDIA_ATTACHMENT|raw }}:</strong><br />
  <small>
  {{ TPL_IMAGE_SELECT_DESC|raw }}
  </small>
  <br />
  {{ TPL_ENTER_ID|raw }}
  <input type="text" name="media_attachment_id" size=10 maxlength="15" value="{{ TPL_LOCAL_MEDIA_ATTACHMENT_ID|raw }}" />
  <br />
  {{ TPL_OR|raw }}
  <br />
  {{ TPL_CHOOSE_RECENT_ADMIN_UPLOAD|raw }}
  {{ TPL_LOCAL_RECENT_ADMIN_UPLOADS_SELECT|raw }}
  <br />
  {{ TPL_OR|raw }}
  <br />
  <!-- <input type="file" name="main_upload"> -->
  {{ TPL_UPLOAD_A_FILE|raw }}
  <div class="fileinputwrapper">
    <input class="fileinput" type="file" name="main_upload" accept=".gif,.jpeg,.jpg,.png" />
  </div>
  <div class="previewimagewrapper">
    <div class="clearimage" title="Clear this attachment">×</div>
    <img class="previewimage" />
  </div>
  <div class="imagedimensions">
    Image is <span class="filedimension">width x height</span>.
    <span class="filedimensioncorrect">Perfect!</span>
    <span class="filedimensionwarning">It should be 480 x 320.</span>
  </div>
</td>
</tr>
<tr ><td colspan=2><hr/></td></tr>

</table>

<input type="submit" name="publish" value="{{ TPL_BUTTON_PUBLISH|raw }}" />
</form>
<!-- end publish template -->
