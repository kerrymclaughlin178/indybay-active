

<a href="blurb_edit.php?id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">{{ TPL_BACK_TO_BLURB_EDIT|raw }}</a>
<br /><br />
<strong>
{{ TPL_LOCAL_PAGE_ASSOCIATIONS|raw }}
</strong>

<br />
{{ TPL_RECATEGORIZE_FEATURE_PAGE_EXPLANATIONS|raw }}
<br />
<br />
<form method="GET">
{{ TPL_ADD_AS_CURRENT_TO_PAGE|raw }}: {{ TPL_LOCAL_SELECT_NONASSOCIATED_PAGES|raw }} <input name="add_current" value="{{ TPL_ADD_SUBMIT_TEXT|raw }}" type="submit">
<input type="hidden" name="id" value="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}" />
</form>
<br />
<form method="GET">
{{ TPL_ADD_AS_ARCHIVED_TO_PAGE|raw }}: {{ TPL_LOCAL_SELECT_NONASSOCIATED_PAGES|raw }} <input name="add_archived" value="{{ TPL_ADD_SUBMIT_TEXT|raw }}" type="submit">
<input type="hidden" name="id" value="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}" />
</form>
<br />
<form method="GET">
{{ TPL_REMOVE_CATEGORY_ASSOCIATION|raw }}: {{ TPL_LOCAL_SELECT_ASSOCIATED_PAGES|raw }} <input name="remove_association" value="{{ TPL_REMOVE_SUBMIT_TEXT|raw }}" type="submit">
<input type="hidden" name="id" value="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}" />
</form>

