<a href="blurb_add.php?preselect_page_id={{ TPL_LOCAL_PAGE_ID|raw }}">{{ TPL_BLURB_ADD|raw }}</a>
 | 
<a href="feature_page_preview.php?page_id={{ TPL_LOCAL_PAGE_ID|raw }}">{{ TPL_PREVIEW_FEATURE_PAGE|raw }}</a>
 | 
<a href="/?preview=1&page_id={{ TPL_LOCAL_PAGE_ID|raw }}">{{ TPL_TEST_PAGE|raw }}</a>

<h2>{{ TPL_CURRENT_BLURB_LIST_FOR|raw }} <EM>{{ TPL_LOCAL_FEATURE_PAGE_NAME|raw }}</EM></h2>

<small>
{{ TPL_LAST_PUSHED_DATE_TEXT|raw }} {{ TPL_LOCAL_LAST_PUSHED_DATE|raw }}
</small>
<FORM name="order_form" action="feature_page_blurb_list.php" method="post">
<h3>
{{ TPL_LOCAL_CURRENT_LINK|raw }}
|
{{ TPL_LOCAL_ARCHIVED_LINK|raw }}
|
{{ TPL_LOCAL_HIDDEN_LINK|raw }}
</h3>
<INPUT type=submit name="reorder" value="Reorder, Change Templates And Archive" />
<TABLE width="100%" >
<tr style="background-color: #e0e0e0;" >
<td><strong>{{ TPL_NEWS_ITEM_ID|raw }}</strong></td>
<td><strong>{{ TPL_TITLE|raw }}</strong></td>
<td><strong>{{ TPL_CREATED|raw }}</strong></td>
<td><strong>{{ TPL_MODIFIED|raw }}</strong></td>
<td><strong>{{ TPL_ORDER_NUM|raw }}</td>
<td><strong>{{ TPL_TEMPLATE|raw }}</td>
<td>&#160;</td>
<td>&#160;</td>
</tr>
{{ TPL_LOCAL_TABLE_ROWS|raw }}

</TABLE>

<br />
<INPUT type=submit name="reorder" value="Reorder, Change Templates And Archive" />
<input type="hidden" name="page_id" value="{{ TPL_LOCAL_PAGE_ID|raw }}">
</FORM>
