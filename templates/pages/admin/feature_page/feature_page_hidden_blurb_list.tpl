<h2>{{ TPL_HIDDEN_BLURB_LIST_FOR|raw }} {{ TPL_LOCAL_FEATURE_PAGE_NAME|raw }}</h2>
<h3>
{{ TPL_LOCAL_CURRENT_LINK|raw }}
|
{{ TPL_LOCAL_ARCHIVED_LINK|raw }}
|
{{ TPL_LOCAL_HIDDEN_LINK|raw }}
</h3>
{{ TPL_NAV|raw }}
<TABLE width="100%" >
<tr style="background-color: #e0e0e0;">
<td><strong>{{ TPL_NEWS_ITEM_ID|raw }}</strong></td>
<td><strong>{{ TPL_TITLE|raw }}</strong></td>
<td><strong>{{ TPL_CREATED|raw }}</strong></td>
<td><strong>{{ TPL_MODIFIED|raw }}</strong></td>
<td>&#160;</td>
<td>&#160;</td>
<td>&#160;</td>
</tr>
{{ TPL_LOCAL_TABLE_ROWS|raw }}

</TABLE>
{{ TPL_NAV|raw }}
<br />



