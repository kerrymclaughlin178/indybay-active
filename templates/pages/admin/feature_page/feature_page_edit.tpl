
<h3>{{ TPL_EDIT_PAGE_INFO|raw }}</h3>

<form name ="category_update_form" method="post" >

<TABLE>
    <TR> 
    	<TD colspan="2">
	</TD>
    </TR>

	  <INPUT TYPE="hidden" NAME="page_id" VALUE={{ TPL_LOCAL_PAGE_ID|raw }}>

    <TR> 
      	<TD>{{ TPL_PAGE_SHORT_NAME|raw }}</TD>
       	<TD> 
          <input name="short_display_name" value="{{ TPL_LOCAL_SHORT_DISPLAY_NAME|raw }}" />
       	</TD>
    </TR>
    <TR>
	<TD>{{ TPL_PAGE_LONG_NAME|raw }}</TD>
   	<TD>
   		
   	  <INPUT NAME="long_display_name" size="40" VALUE="{{ TPL_LOCAL_LONG_DISPLAY_NAME|raw }}" />
   	</TD>
    </TR>
    
    <TR>
	<TD>{{ TPL_PAGE_SUMMARY_LENGTH|raw }}</TD>
	<TD><input name="items_per_newswire_section" value="{{ TPL_LOCAL_ITEMS_PER_NEWSWIRE_SECTION|raw }}" /></TD>
    </TR>
    <TR>
	<TD>{{ TPL_PAGE_RELATIVE_PATH|raw }}</TD>
	<TD><input name="relative_path" value="{{ TPL_LOCAL_RELATIVE_PATH|raw }}" /></TD>
    </TR>
    <TR> 
    <TD valign="top">{{ TPL_INCLUDE_IN_READMORE_LINKS|raw }}</TD>
    <TD><input type="checkbox" name="include_in_readmore_links" value="1" {{ TPL_LOCAL_INCLUDE_IN_READMORE_LINKS|raw }} /></TD>
    
    </TR>
    
    <TR> 						
	<TD>{{ TPL_NEWSWIRE_TYPE|raw }}</TD>
	<TD>{{ TPL_LOCAL_SELECT_NEWSWIRE|raw }}</TD>
    </TR>
    
    <TR>
	<TD>{{ TPL_CENTERCOLUMN_TYPE|raw }}:</TD>
	<TD>{{ TPL_LOCAL_SELECT_CENTERCOLUMN|raw }}</TD>
    </TR>
    
    <TR>
	<TD colspan=1><hr /></TD><TD>&#160;</td>
    </TR>
    <TR>
	<TD colspan=2>{{ TPL_EVENT_LIST_PREFERENCES|raw }}</TD>
    </TR>

    
    <TR>
	<TD>{{ TPL_EVENTLIST_TYPE|raw }}:</TD>
	<TD>{{ TPL_LOCAL_SELECT_EVENTLIST|raw }}</TD>
    </TR>
     
    <TR>
	<TD>{{ TPL_MAX_EVENTS_IN_HIGHLIGHTED_LIST|raw }}:</TD>
	<TD><input name="max_events_in_highlighted_list" value="{{ TPL_LOCAL_MAX_EVENTS_IN_HIGHLIGHTED_LIST|raw }}"/></TD>
    </TR>
    
    <TR>
	<TD>{{ TPL_MAX_TIMESPAN_FOR_HIGHLIGHTED_EVENT_LIST|raw }}:</TD>
	<TD><input name="max_timespan_in_days_for_highlighted_event_list" value="{{ TPL_LOCAL_MAX_TIMESPAN_IN_DAYS_FOR_HIGHLIGHTED_EVENT_LIST|raw }}"/></TD>
    </TR>
    
    <TR>
	<TD>{{ TPL_MAX_EVENTS_IN_FULL_EVENT_LIST|raw }}:</TD>
	<TD><input name="max_events_in_full_event_list" value="{{ TPL_LOCAL_MAX_EVENTS_IN_FULL_EVENT_LIST|raw }}"/></TD>
    </TR>
    
    <TR>
	<TD>{{ TPL_MAX_TIMESPAN_FOR_FULL_EVENT_LIST|raw }}:</TD>
	<TD><input name="max_timespan_in_days_for_full_event_list" value="{{ TPL_LOCAL_MAX_TIMESPAN_IN_DAYS_FOR_FULL_EVENT_LIST|raw }}"/></TD>
    </TR>
    
       
        </TR>
        <TR> 
        <TD><hr/></TD>
        <TD valign="top">&#160;</TD>
    </TR>
<TR> 
        <TD colspan="2"> 
          <input type=submit name="save" value="{{ TPL_SAVE|raw }}"/>
        </TD>
    </TR>
    <TR></TR>
</TABLE>
</FORM>

