<strong>
{{ TPL_VALIDATION_MESSAGES|raw }}
{{ TPL_STATUS_MESSAGES|raw }}
</strong>

<strong>
{{ TPL_LOCAL_PAGE_ASSOCIATIONS|raw }}
</strong>
<a href="blurb_recategorize.php?id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">{{ TPL_RECATEGORIZE|raw }}</a>
 |
<a href="/admin/breaking/create.php?parent_item_id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">Add breaking news item for this blurb</a>

<form enctype="multipart/form-data" method="post" action="blurb_edit.php">
<input type="hidden" name="editswitch" value="1" />

<input type="hidden" name="news_item_id" value="{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}" />

<table border="0" cellspacing="1" cellpadding="1"  class="bgsearchgrey" width="80%">
<tr><td colspan="2"><input type=submit name="save" value="save" />
<br />
</td></tr>

<tr><td colspan=3>{{ TPL_ID|raw }}:&#160;
{{ TPL_LOCAL_NEWS_ITEM_ID|raw }}&#160;
{{ TPL_VERSION_ID|raw }}:&#160;
{{ TPL_LOCAL_NEWS_ITEM_VERSION_ID|raw }}
</td></tr>

<tr><td valign="top" width="225">
<hr />
<small>{{ TPL_CREATED|raw }}:

{{ TPL_LOCAL_CREATION_DATE|raw }}
<br />
{{ TPL_LOCAL_CREATED_BY_USER_INFO|raw }}
</small>
<hr />
</td><td valign="top" width="175">
<hr />
<small>
{{ TPL_MODIFIED|raw }}:
{{ TPL_LOCAL_VERSION_CREATION_DATE|raw }}
<br />
{{ TPL_LOCAL_LAST_UPDATED_BY_USER_INFO|raw }}
</small>
<hr />
</td>
<td>&#160;</td>
</tr>
<tr  valign="top">

<td width="25%">
<strong>{{ TPL_TITLE1|raw }}</strong>
</td>

<td width="75%">
<input type="text" name="title1" style="width: 90%" maxlength="90" value="{{ TPL_LOCAL_TITLE1|raw }}" />
</td>

</tr>

<tr  valign="top">

<td width="25%">
<strong>{{ TPL_TITLE2|raw }}</strong> <small>({{ TPL_REQUIRED|raw }})</small>
</td>

<td width="75%">
<input type="text" name="title2" style="width: 90%" maxlength="90" value="{{ TPL_LOCAL_TITLE2|raw }}" />
</td>

</tr>
</table>


<br /><br />
<table class="bgsearchgrey" width="75%">
<tr>
<td  colspan=2>
<strong>{{ TPL_SHORT_VERSION|raw }}</strong>
</td></tr>
<tr><td colspan=2>
<textarea name="summary" rows="10" style="width: 90%" wrap="virtual">{{ TPL_LOCAL_SUMMARY|raw }}</textarea>
<br />
</td>

</tr>

<tr><td coslpan=2><strong>{{ TPL_IS_SHORT_VERSION_HTML|raw }}</strong> {{ TPL_LOCAL_CHECKBOX_IS_SUMMARY_HTML|raw }}
</td></tr>

<tr ><td colspan=2><hr/></td></tr>
<tr ><td colspan=2>
  <strong>{{ TPL_THUMBNAIL_MEDIA_ATTACHMENT|raw }}:</strong><br />
  <small>
  {{ TPL_IMAGE_SELECT_DESC|raw }}
  </small>
  <br />
  {{ TPL_ENTER_ID|raw }}
  <input type="text" name="thumbnail_media_attachment_id" size=10 maxlength="15" value="{{ TPL_LOCAL_THUMBNAIL_MEDIA_ATTACHMENT_ID|raw }}" />

  {{ TPL_LOCAL_THUMBNAIL_EDIT_LINK|raw }}
  <br />
  {{ TPL_OR|raw }}
  <br />
  {{ TPL_CHOOSE_RECENT_ADMIN_UPLOAD|raw }}
  {{ TPL_LOCAL_THUMBNAIL_RECENT_ADMIN_UPLOADS_SELECT|raw }}
  <br />
  {{ TPL_OR|raw }}
  <br />
  <!-- <input type="file" name="thumbnail_upload"> -->
  {{ TPL_UPLOAD_A_FILE|raw }}
  <div class="fileinputwrapper">
    <input class="fileinput" type="file" name="thumbnail_upload" accept=".gif,.jpeg,.jpg,.png" />
  </div>
  <div class="previewimagewrapper">
    <div class="clearimage" title="Clear this attachment">×</div>
    <img class="previewimage" />
  </div>
  <div class="imagedimensions">
    Image is <span class="filedimension">width x height</span>.
    <span class="filedimensioncorrect">Perfect!</span>
    <span class="filedimensionwarning">It should be 480 x 320.</span>
  </div>
</td>
</tr>
<tr ><td colspan=2><hr /></td></tr>
</table>


<br /><br />
<table class="bgsearchgrey" width="75%">
<tr>
<td coslpan=2 >
<strong>{{ TPL_LONG_VERSION|raw }}</strong>
</td>
</tr>
<tr>
<td colspan=2>
<textarea name="text" rows="30" style="width: 90%" wrap="virtual">{{ TPL_LOCAL_TEXT|raw }}</textarea>
</td>

</tr>

<tr><td colspan=2><strong>{{ TPL_IS_LONG_VERSION_HTML|raw }}</strong> {{ TPL_LOCAL_CHECKBOX_IS_TEXT_HTML|raw }}
</td></tr>
<tr >

<td width="25%"><strong>{{ TPL_PUB_URL|raw }}</strong></td>

<td width="75%">
<input type="text" name="related_url" style="width: 90%" maxlength="255" value="{{ TPL_LOCAL_RELATED_URL|raw }}"/>
</td>

</tr>

<tr ><td colspan=2><hr/></td></tr>
<tr ><td colspan=2>
  <strong>{{ TPL_MEDIA_ATTACHMENT|raw }}:</strong><br />
  <small>
  {{ TPL_IMAGE_SELECT_DESC|raw }}
  </small>
  <br />
  {{ TPL_ENTER_ID|raw }}
  <input type="text" name="media_attachment_id" size=10 maxlength="15" value="{{ TPL_LOCAL_MEDIA_ATTACHMENT_ID|raw }}" />

  {{ TPL_LOCAL_ATTACHMENT_EDIT_LINK|raw }}
  <br />
  {{ TPL_OR|raw }}
  <br />
  {{ TPL_CHOOSE_RECENT_ADMIN_UPLOAD|raw }}
  {{ TPL_LOCAL_RECENT_ADMIN_UPLOADS_SELECT|raw }}
  <br />
  {{ TPL_OR|raw }}
  <br />
  <!-- <input type="file" name="main_upload"> -->
  {{ TPL_UPLOAD_A_FILE|raw }}
  <div class="fileinputwrapper">
    <input class="fileinput" type="file" name="main_upload" accept=".gif,.jpeg,.jpg,.png" />
  </div>
  <div class="previewimagewrapper">
    <div class="clearimage" title="Clear this attachment">×</div>
    <img class="previewimage" />
  </div>
  <div class="imagedimensions">
    Image is <span class="filedimension">width x height</span>.
    <span class="filedimensioncorrect">Perfect!</span>
    <span class="filedimensionwarning">It should be 480 x 320.</span>
  </div>
</td>
</tr>
<tr ><td colspan=2><hr/></td></tr>


</table>
<input type=submit name="save2" value="save">
</td></tr>
<tr><td colspan=3>
<br />
{{ TPL_RECENT_PREVIOUS_VERSIONS|raw }}:
{{ TPL_LOCAL_PREVIOUS_VERSION_LIST|raw }}
<small><a href="/admin/article/newsitem_history.php?id={{ TPL_LOCAL_NEWS_ITEM_ID|raw }}">{{ TPL_BLURB_HISTORY|raw }}</a></small>
</td></tr>
</table>
<input type="hidden" name="timestamp" value="{{ TPL_LOCAL_MICROTIME|raw }}">
</form>

<br />
<table width=70%>
<tr><td>


<h3><a name="preview">{{ TPL_LONG_VERSION|raw }}:</a></h3>
{{ TPL_LOCAL_DISPLAY_LONG_PREVIEW|raw }}
<br clear="all">
<h3>{{ TPL_SHORT_VERSION|raw }}:</h3>
{{ TPL_LOCAL_DISPLAY_SHORT_PREVIEW|raw }}
</td></tr>
</table>

<!-- end article_edit template -->
