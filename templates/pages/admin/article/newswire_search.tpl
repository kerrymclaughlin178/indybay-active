
<!--we cant open or close the form since its needed for paging| we could use hidden fields but its 
safer to only have thing in one place-->


{{ TPL_SEARCH|raw }}:
<input name="search" type="text" size="14"  value="{{ TPL_LOCAL_SEARCH|raw }}" />

{{ TPL_SEARCH_DISPLAY|raw }}

{{ TPL_SEARCH_MEDIUM|raw }}

{{ TPL_CAT_TOPIC_SELECT|raw }}

{{ TPL_CAT_REGION_SELECT|raw }}
<br />
<small>{{ TPL_INCLUDE_COMMENTS }}</small>
{{ TPL_LOCAL_CHECKBOX_COMMENTS|raw }}
<small>{{ TPL_INCLUDE_ATTACHMENTS }}</small>
{{ TPL_LOCAL_CHECKBOX_ATTACHMENTS|raw }}
<small>{{ TPL_INCLUDE_EVENTS }}</small>
{{ TPL_LOCAL_CHECKBOX_EVENTS|raw }}
<small>{{ TPL_INCLUDE_POSTS }}</small>
{{ TPL_LOCAL_CHECKBOX_POSTS|raw }}
<small>{{ TPL_INCLUDE_BLURBS }}</small>
{{ TPL_LOCAL_CHECKBOX_BLURBS|raw }}
<br />
<input type="hidden" name="parent_item_id" value="{{ TPL_LOCAL_PARENT_ITEM_ID }}"/>
<input type="submit" name="submitted_search" value="{{ TPL_GO }}"/>

