<strong>
{{ TPL_VALIDATION_MESSAGES|raw }}
{{ TPL_STATUS_MESSAGES|raw }}
</strong>
<h3>{{ TPL_ARTICLES_ADMIN|raw }}</h3>
<span style="float: right;"><a href="/admin/feature_page/blurb_add.php">{{ TPL_BLURB_ADD|raw }}</a></span>

(<a href="#aboutclass">Read About New Classification choices</a>)
<hr />
<table><tr>
<td>

<form action="/admin/article/article_edit.php">{{ TPL_ENTER_ARTICLE_ID|raw }}<br />
<input type="text" name="id" value="">
<input type="submit" name="{{ TPL_ARTICLE_EDIT|raw }}" value="Submit"></form>
</Form>
</td><td>
<form action="/admin/article/index.php">{{ TPL_ENTER_COMMENT_ID|raw }}<br />
<input type="text" name="parent_item_id" value="{{ TPL_LOCAL_PARENT_ITEM_ID|raw }}">
<input type="hidden" name="comments" value="yes">
<input type="submit" name="force_parent_id_search" value="Submit"></form>
</Form>
</td>
</tr></table>

<hr />

<div class="list-search">
<form method="get" action="/admin/article/index.php">
{{ TPL_TOP_SEARCH_FORM|raw }}

{{ TPL_NAV|raw }}
</form>
</div>

<FORM method="post" name="order_form">
<INPUT type="submit" name="classify" value="{{ TPL_SAVE|raw }}"/><br >
<INPUT type="hidden" name="num_rows" value="{{ TPL_NUM_ROWS|raw }}" />
<INPUT type="hidden" name="bulk_classify" value="1" />
<br >
{{ TPL_TABLE_MIDDLE|raw }}
{{ TPL_NAV|raw }}
<INPUT type=submit name="classify" value="{{ TPL_SAVE|raw }}"/>
<p/>
<a href="JavaScript:hideall();">Set All Dropdowns to Hidden</a>
|
<a href="JavaScript:document.forms['order_form'].reset()">Reset</a>
</FORM>


<br ><br /><br >
<strong><a name="aboutclass">ABOUT CLASSIFICATIONS</a></strong>
<br ><br />
The new article admin system gives you a choice of more ways to classify posts than the old system.
<br /><br />
NEW is the status when anything new comes in.
<br /><br />
The HIGHLIGHTED statuses are used to make posts what we call "local" and "global"
in the right column. If you make an event HIGHLIGHTED (local or nonlocal) it will appear in the eventlinks.
<br /><br />

OTHER acts the same as NEW but is a way to mark something as having been examined.
The corporate reports choices are similar to OTHER on most pages
but they make posts appear in the right column on international pages.
<br /><br />
HIDDEN hides a post or comment
<br /><br />
QUESTIONABLE means you are not sure if something should be hidden (QUESTIONABLE/HIDE will hide it and QUESTIONABLE/DONTHIDE will not hide it)
<br ><br /><br />
<strong>Classifying Comments</strong>
<br ><br >
To make it easier for editors to know if comments have been reviewed it is worthwhile to classify comments.
The "Needs Attention" page only shows NEW and QUESTIONABLE comments so you will want to classify any comment that shouldn't be hidden as OTHER.
Classifying a comment as hidden will hide it but the other statuses will not have an effect on how the comment is seen.
<br />
<br ><br />
<br ><br />
<br ><br />
<br ><br />
<br ><br />
<br ><br />
<br ><br />
