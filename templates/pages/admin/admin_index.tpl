<big>{{ TPL_ADMIN_WELCOME|raw }} {{ TPL_USERNAME|raw }}.</big>

<ul>
<li><h3><a href="/admin/user/user_list.php">{{ TPL_ADMIN_USER|raw }}</a></h3></li>
<li><h3><a href="/admin/feature_page/feature_page_display_list.php">{{ TPL_ADMIN_CATEGORY|raw }}</a></h3></li>
<li><h3><a href="/admin/calendar/">{{ TPL_ADMIN_CALENDAR|raw }}</a></h3></li>
<li><h3><a href="/admin/article/">{{ TPL_ADMIN_ARTICLE|raw }}</a></h3></li>
<li><h3><a href="/admin/media/upload_display_list.php">{{ TPL_ADMIN_UPLOAD|raw }}</a></h3></li>
<li><h3><a href="/admin/email/">{{ TPL_ADMIN_EMAIL|raw }}</a></h3></li>
</ul>

<h3><a href="authentication/authenticate_display_logon.php">{{ TPL_ADMIN_LOGOFF|raw }}</a></h3>
