<h2>{{ TPL_UPLOAD_LIST|raw }}</h2>
<a href="upload_add.php">{{ TPL_ADD_NEW_UPLOAD|raw }}</a>
<br /><br />
<hr>
<p/>
<h3>Search For Uploads:</h3>
<form name="upload_list" type="GET">

{{ TPL_UPLOAD_TYPE|raw }}:
{{ TPL_LOCAL_SELECT_UPLOAD_TYPES|raw }}
<br />
{{ TPL_MEDIA_TYPE_GROUPING|raw }}: 
{{ TPL_LOCAL_SELECT_GROUPING_IDS|raw }}
<br />
File Name: <input type="test" name="keyword" value="{{ TPL_LOCAL_KEYWORD|raw }}">
<br />
<input type="submit" name="filter" value="Filter">


<br />
{{ TPL_NAV|raw }}
<table >
<tr>
<td></td>
</tr>
{{ TPL_UPLOAD_ROWS|raw }}
</table>
{{ TPL_NAV|raw }}
</form>
