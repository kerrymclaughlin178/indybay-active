<strong><a href="/admin/media/upload_list.php">Return To Upload List</a> {{ TPL_DELETE_LINK|raw }}</strong>
<table >
<tr class="bgsearchgrey" ><td>{{ TPL_MEDIA_ATTACHMENT_ID|raw }}</td><td>{{ TPL_LOCAL_MEDIA_ATTACHMENT_ID|raw }}</td></tr>
<tr class="bgsearchgrey" ><td>{{ TPL_FILE_LOCATION|raw }}</td><td>{{ TPL_LOCAL_IMAGE_LOCATION|raw }}</td></tr>
<tr class="bgsearchgrey"><td>{{ TPL_UPLOAD_TYPE|raw }}</td><td>{{ TPL_LOCAL_UPLOAD_TYPE_NAME|raw }}</td></tr>
<tr class="bgsearchgrey"><td>{{ TPL_UPLOAD_SIZE|raw }}</td><td>{{ TPL_LOCAL_UPLOAD_SIZE|raw }}</td></tr>
<tr class="bgsearchgrey"><td colspan="2">{{ TPL_LOCAL_THUMBNAIL|raw }}
</td></tr>

<tr class="bgsearchgrey"><td>{{ TPL_ORIGINAL_FILE_NAME|raw }}</td><td>{{ TPL_LOCAL_ORIGINAL_FILE_NAME|raw }}</td></tr>
<tr class="bgsearchgrey"><td colspan="2" valign="top">
{{ TPL_CURRENT_RELATED_NEWS_ITEM_LINKS|raw }}
{{ TPL_PREVIOUS_RELATED_NEWS_ITEM_LINKS|raw }}
{{ TPL_PARENT_IMAGE_LINK|raw }}
{{ TPL_CHILD_IMAGE_LINKS|raw }}

</td></tr>
</table>

