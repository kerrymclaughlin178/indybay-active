
<div class="error404">

  <h1>404 Error</h1>

  <h2>Indybay was unable to find the page you requested.</h2>

  <h3>Try our <a href="/">home page</a> or <a href="/search/">search</a> for what you want.</h3>

  <img src="/im/unembedded-800.jpg" />

</div>
