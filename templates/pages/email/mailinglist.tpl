<!-- Mailing List Template -->

<h1 class="headline-text">Subscribe to Off-Site Updates</h1>

<p><strong>News & Events Emails</strong>
<br/>
Subscribe to our low-volume email list or manage your subscription at <br />
<a href="https://lists.riseup.net/www/info/indybay-news">https://lists.riseup.net/www/info/indybay-news</a></p>

<p><strong>Indybay Event Calendar</strong>
<br/>
Browse the <img src="/im/imc_event.svg" alt="event" style="width: 12px;"> <a href="/calendar/">calendar</a> for iCal feeds filtered by topic and/or region.  These iCal feeds work with a variety of calendar applications for your computer or mobile device as well as online calendar apps. Here's a <a href="https://www.google.com/calendar/render?cid=http%3A%2F%2Fwww.indybay.org%2Fcalendar%2Fical_feed.php%3Ftopic_id%3D0%26region_id%3D36" title="Add a calendar feed to your Google calendar">sample link</a> to add the City of San Francisco calendar to your Google Calendar.</p>

<p><strong>Twitter</strong>
<br/>
Follow <a href="https://twitter.com/intent/follow?screen_name=indybay" rel="noreferrer" target="_blank">@indybay</a>

<p><strong>Facebook</strong>
<br/>
Follow <a href="https://www.facebook.com/indybay.org/" rel="noreferrer" target="_blank">indybay.org</a>

<p><strong>Mastodon.social</strong>
<br/>
Follow <a href="https://mastodon.social/@indybay" rel="noreferrer" target="_blank">@indybay</a>

<p><strong>Instagram</strong>
<br/>
Follow <a href="https://www.instagram.com/indymedia/" rel="noreferrer" target="_blank">indymedia</a>

<p style="margin-bottom: 0;"><strong>Audio and Video Feeds ("Podcasts")</strong></p>
<table>
  <tr>
    <td rowspan="2">(iTunes 4.9+)</td>
    <td><a href="/syn/audio.pcast"><img src="/im/pcast.png" width="45" height="15" alt="pcast" style="border: 0; margin-top: 2px;" /></a></td>
    <td><a href="/syn/audio.pcast">Subscribe to Indybay's audio channel</a></td>
  </tr>
  <tr>
    <td><a href="/syn/video.pcast"><img src="/im/pcast.png" width="45" height="15" alt="pcast" style="border: 0; margin-top: 2px;" /></a></td>
    <td><a href="/syn/video.pcast">Subscribe to Indybay's video channel</a></td>
  </tr>
</table>
</p>

<p><strong>Javascript Newsfeed</strong>
<br/>
Indymedia is on the FBI's watch list &#8212; put it on yours too!
Simply include this HTML in your webpage to add our headlines to
your site:
<br />
<code>&lt;script type="text/javascript" src="https://www.indybay.org/syn/jscript.php"&gt;&lt;/script&gt;</code></p>

<div class="subscribe_container">
  <div class="subscribe_header">RSS/XML Newsfeeds:</div>
  <div class="subscribe_item"><a class="subscribe_link" href="/syn/generate_rss.php?include_posts=0&amp;include_blurbs=1">Feature Stories <span>(rss)</span></a></div>
  <div class="subscribe_item"><a class="subscribe_link" href="/syn/generate_rss.php?news_item_status_restriction=1155">Newswire <span>(rss)</span></a></div>
  <div class="subscribe_item"><a class="subscribe_link" href="/syn/generate_rss.php?news_item_status_restriction=1155&amp;rss_version=2">Newswire with enclosures <span>(rss)</span></a></div>
  <div class="subscribe_item"><a class="subscribe_link" href="/syn/">Complete Syndication Index <span>includes topical and regional feeds!</span></a></div>
</div>

<p>You can add the above URLs to your blog, feed reader app or browser extension. Feel free to <a href="mailto:indybay@lists.riseup.net">let us know</a> if you find it useful or if there's a problem.</p>

<div class="subscribe_container">
  <div class="subscribe_header">3rd party syndication services:</div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/frontpage">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/frontpage">Frontpage featured stories</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/media">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/media">Newswire with media enclosures</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/features">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/features">Featured stories for all sections of the site</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/newswire">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/newswire">Open-publishing newswire</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/video">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/video">Video</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/radio">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/radio">Radio</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/photos">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/photos">Photo</a>
  </div>
  <div class="subscribe_item">
    <span class="linkright"><a href="https://add.my.yahoo.com/rss?url=https://feeds.feedburner.com/indybay/torrents">add to yahoo</a></span>
    <a class="subscribe_link" href="https://feeds.feedburner.com/indybay/torrents">BitTorrent torrents</a>
  </div>
</div>
<br/>
