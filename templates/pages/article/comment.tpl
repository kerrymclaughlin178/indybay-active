<h1 class="headline-text">Add comment on:</h1>

<div class="comment_articlepreview">
  <strong>{{ TPL_LOCAL_PARENT_TITLE1|raw }}</strong>
  <br />
  <em>by {{ TPL_LOCAL_PARENT_DISPLAYED_AUTHOR_NAME|raw }}</em>
  <br />
  {{ TPL_LOCAL_PARENT_SUMMARY|raw }}

  {{ TPL_HIDE1|raw }}
  {{ TPL_LOCAL_DISPLAY_PREVIEW|raw }}
  {{ TPL_HIDE2|raw }}
</div>

<hr size="1" noshade />

<h3>Guidelines for commenting on news articles:</h3>

<p>Thanks for contributing to Indybay's open publishing newswire. You may use any format for your response article, from traditional academic discourse to
  subjective personal account. Please keep it on topic and concise. And please read our <a href="/newsitems/2002/08/04/1395001.php">editorial policy</a>, <a href="/newsitems/2003/12/15/16659061.php">privacy</a>, and <a href="/newsitems/2003/12/15/16659051.php">legal</a>
  statements before continuing. Or <a href="javascript:history.back()">go back to the article</a>.</p>

<a name="publishform"></a>
<form enctype="multipart/form-data" method="post" accept-charset="UTF-8" action="/comment.php?top_id={{ TPL_LOCAL_PARENT_ITEM_ID|raw }}" id="publish-form">
  <input type="hidden" name="parent_item_id" value="{{ TPL_LOCAL_PARENT_ITEM_ID|raw }}" />
  {{ TPL_VALIDATION_MESSAGES|raw }}
  {{ TPL_STATUS_MESSAGES|raw }}

  <table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent roundtop">
    <tr>
      <td class="bgaccent">

        <span class="extraspaceleftsm">
          <strong>{{ TPL_PUB_STEPONE|raw }}</strong>
        </span>

        <table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
          <tr>
            <td class="publish-wrapper">
              <table class="publish-table" width="100%" border="0" cellspacing="2" cellpadding="4">
                <tr class="bgpenult">

                  <td width="25%">
                    <strong>{{ TPL_TITLE3|raw }}</strong> <small>({{ TPL_REQUIRED|raw }})</small>
                  </td>

                  <td width="75%">
                    <input type="text" name="title1" maxlength="90" value="{{ TPL_LOCAL_TITLE1|raw }}" />
                  </td>

                </tr>
                <tr class="bgpenult">

                  <td>
                    <strong>{{ TPL_PUB_AUTHOR|raw }}</strong> <small>({{ TPL_REQUIRED|raw }})</small>
                  </td>

                  <td>
                    <input type="text" name="displayed_author_name" size="25" maxlength="45" value="{{ TPL_LOCAL_DISPLAYED_AUTHOR_NAME|raw }}" />
                  </td>

                </tr>

                <tr>
                  <td>
                    <span class="subheader"><strong>Contact Info</strong></span>
                  </td>
                  <td></td>
                </tr>

                <tr class="bgpenult">
                  <td>
                    {{ TPL_EMAIL|raw }}
                  </td>
                  <td>
                    <input type="text" name="email" maxlength="45" value="{{ TPL_LOCAL_EMAIL|raw }}" />
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <span class="nowrap">
                      <label for="display_contact_info">{{ TPL_DISPLAY_EMAIL|raw }}</label>
                      {{ TPL_LOCAL_CHECKBOX_DISPLAY_CONTACT_INFO|raw }}
                    </span>
                    <br/>
                    <small>
                      {{ TPL_DISPLAY_ADDITIONAL_ADDITIONAL_TEXT|raw }}
                    </small>
                  </td>
                </tr>

                <tr class="bgpenult">
                  <td>{{ TPL_PUB_URL|raw }}</td>
                  <td>
                    <input type="text" name="related_url" maxlength="255" value="{{ TPL_LOCAL_RELATED_URL|raw }}" /></td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    <!-- publish form part 2 -->

    <table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
      <tr>
        <td class="bgaccent">

          <span class="extraspaceleftsm">
            <strong>{{ TPL_PUB_STEPTWO|raw }}</strong>
          </span>

          <table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
            <tr>
              <td class="publish-wrapper">

                <table class="publish-table" width="100%" border="0" cellspacing="2" cellpadding="4">
                  <tr>
                    <td width="25%" class="vtop">

                      <strong>TEXT/HTML</strong>
                      <small>({{ TPL_REQUIRED|raw }})</small>
                      <br />
                      <small>
                        <span class="rednote"><strong>Tip:</strong></span>
                        <a href="/notice/legibility-note.html" title="Click to see an example" target="blank">use spaces between paragraphs</a>.
                      </small>

                  </td>
                  <td width="75%">

                    <textarea name="text" rows="10" cols="80">{{ TPL_LOCAL_TEXT|raw }}</textarea><br />
                    <span class="nowrap"><label for="is_text_html">{{ TPL_IS_TEXT_HTML|raw }}</label> {{ TPL_LOCAL_CHECKBOX_IS_TEXT_HTML|raw }}</span>
                    <p><small>{{ TPL_PUB_ART|raw }}</small></p>

                  </td>
                </tr>
              </table>

            </td>
          </tr>
        </table>

      </td>
    </tr>
  </table>

  <!-- publish form part 3 -->

  <table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
    <tr>
      <td class="bgaccent">

        <span class="extraspaceleftsm">
          <strong>{{ TPL_PUB_STEPTHREE|raw }}</strong>
        </span>

        <table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent">
          <tr class="bgpenult">
            <td>

              <table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
                <tr>
                  <td class="publish-wrapper">

                    <table class="publish-table" width="100%" border="0" cellspacing="2" cellpadding="4">
                      <tr class="bgpenult">

                        <td width="25%" class="vtop">
                          <strong>{{ TPL_PUB_CUANTOFILES|raw }}</strong>
                        </td>

                        <td width="75%">

                          <span>{{ TPL_SELECT_FILECOUNT|raw }}</span>

                          <input type="submit" name="action" value="Enter" id="upload_submit_button" />

                          <div class="clicktip">

                            <p class="trigger" title="click here to learn more">Please note the allowed file types and file sizes!</p>

                            <div class="target">
                              {{ TPL_MAX_UPLOAD|raw }} {{ TPL_UPLOAD_MAX_FILESIZE|raw }}; {{ TPL_TOTAL_LIMIT|raw }} {{ TPL_POST_MAX_SIZE|raw }}; {{ TPL_MAX_EXECUTION_TIME|raw }} {{ TPL_HOURS|raw }}
                              <br />
                              {{ TPL_ACCEPTED_TYPES|raw }}
                              <br />
                              <strong>For playability of media files on mobile devices</strong>, upload videos as MP4s with H.264 encoding. For audio, upload MP3s.
                            </div><!-- END .target -->

                          </div><!-- END .clicktip -->

                        </td>

                      </tr>
                    </table>
                    <div id="file_boxes2"></div>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>

        <div id="nonjscript_file_boxes">{{ TPL_FILE_BOXES|raw }}</div>

      </td>
    </tr>
  </table>

  <table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent roundbot">
    <tr>
      <td class="bgaccent"><strong>&#160;CAPTCHA</strong>
        <table width="100%" border="0" cellspacing="0" cellpadding="1" class="bgaccent roundbot">
          <tr class="bgpenult">
            <td>
              <table width="100%" border="0" cellspacing="0" cellpadding="4" class="bgpenult">
                <tr>
                  <td class="publish-wrapper">
                    <table class="publish-table" width="100%" border="0" cellspacing="2" cellpadding="4">
                      <tr class="bgpenult">
                        <td width="25%" class="vtop">
                          <strong>Anti-spam question</strong>
                          <small>({{ TPL_REQUIRED|raw }})</small>
                        </td>
                        <td width="75%">
                          {{ TPL_CAPTCHA_FORM|raw }}
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <div class="publish-buttons">
    <span id="preview_button">{{ TPL_LOCAL_PREVIEW|raw }}</span>
    <input type="submit" name="publish" value="{{ TPL_BUTTON_PUBLISH|raw }}" />
  </div>

</form>
<!-- end publish template -->


<div id="files_select_template" style="visibility: hidden;">
  <hr size="1" width="90%" />
  <table class="publish-table" width="100%" border="0" cellspacing="2" cellpadding="4">
    <tr>
      <td width="25%"><strong>Title #::uploadnum::</strong></td>
      <td width="75%"><input size="25" maxlength="90" type="text" name="linked_file_title_::uploadnum::" value="" /></td>
    </tr>
    <!-- <script>
    // $('body').on('change', '.fileinput', function() {
    //   console.log('body onchange');
    // });
    $('.fileinput').change(function() {
      console.log('.change');
    });
    </script> -->
    <tr>
      <td><strong>Upload #::uploadnum::</strong></td>
      <td>
        <div class="fileinputwrapper">
          <input class="fileinput" type="file" name="linked_file_::uploadnum::" {{ TPL_FILE_ACCEPT|raw }} />
        </div>
        <div class="previewimagewrapper">
          <div class="clearimage" title="Clear this attachment">×</div>
          <div class="nopreview">Previews not available for <span class="filetype">media</span> files.</div>
          <img class="previewimage" />
        </div>
      </td>
    </tr>
    <!-- end of first upload -->
    <tr>
      <td> Optional Text #::uploadnum::</td>
      <td><textarea name="linked_file_comment_::uploadnum::" rows="3" cols="50"></textarea></td>
    </tr>
  </table>
</div>


<div id="files_select_template_1" style="visibility: hidden;">
  <table class="publish-table" width="100%" border="0" cellspacing="2" cellpadding="4">
    <tr>
      <td width="25%"><strong> Upload #1</strong></td>
      <td width="75%">
        <div class="fileinputwrapper">
          <input class="fileinput" type="file" name="linked_file_1" {{ TPL_FILE_ACCEPT|raw }} />
        </div>
        <div class="previewimagewrapper">
          <div class="clearimage" title="Clear this attachment">×</div>
          <div class="nopreview">Previews not available for <span class="filetype">media</span> files.</div>
          <img class="previewimage" />
        </div>
      </td>
    </tr>
  </table>
</div>
