<div class="list-search">

  <form name="search" action="/search/search_results.php">

    <h1 class="headline-text">{{ TPL_SEARCH_ADVANCED|raw }}</h1>

    <div class="search-element">
      <span class="mobilebreak">
        <strong>{{ TPL_KEYWORD|raw }}:</strong>
      </span>
      <input name="search" type="text" size="20" value="{{ TPL_LOCAL_SEARCH|raw }}" placeholder="Enter keyword(s)" />
    </div>

    <div class="search-explanation">

      <div class="clicktip">

        <p class="trigger" title="click here to learn more">How keyword search logic works</p>

        <div class="target">

          Multiple search words imply "<em>or</em>" logic. A leading plus sign indicates that the word <em>must</em> be present.
          To search for an exact phrase, enclose it in double quotes.

          <dl>
            <dt><em>apple banana</em></dt>
            <dd>Find articles that contain at least one of the two words.</dd>
            <dt><em>+apple +juice</em></dt>
            <dd>Find articles that contain both words.</dd>
            <dt><em>+apple -macintosh</em></dt>
            <dd>Find articles that contain the word <q>apple</q> but not <q>macintosh</q>.</dd>
            <dt><em>apple*</em></dt>
            <dd>Find articles that contain words such as <q>apple</q>, <q>apples</q>, <q>applesauce</q>, or <q>applet</q>.</dd>
            <dt><em>"some words"</em></dt>
            <dd>Find articles that contain the exact phrase <q>some words</q> (for example, <q>some words of wisdom</q> but not <q>some noise words</q>).</dd>
          </dl>

        </div><!-- END .target -->

      </div><!-- END .clicktip -->

    </div>

    <hr size="1" noshade />

    <div class="search-element">
      <span>{{ TPL_MEDIA_TYPE|raw }}: </span>
      {{ TPL_SEARCH_MEDIUM|raw }}
    </div>

    <div class="search-element">
      <span>{{ TPL_DISPLAY_OPTIONS|raw }}: </span>
      {{ TPL_SEARCH_DISPLAY|raw }}
    </div>

    <div class="search-element">
      <span>{{ TPL_TOPIC|raw }}: </span>
      {{ TPL_CAT_TOPIC_SELECT|raw }}
    </div>

    <div class="search-element">
      <span>{{ TPL_REGION|raw }}: </span>
      {{ TPL_CAT_REGION_SELECT|raw }}
    </div>

    <br class="mobnarrowhide" />

    <div class="search-element">
      <span>{{ TPL_SEARCH_DATE_RESTRICTION|raw }}: </span>
      {{ TPL_SEARCH_DATE_TYPE_SELECT|raw }}
    </div>

    <div class="search-element">
      <span class="search-range disabled">
        <span class="datetext mobilewidebreak">{{ TPL_SEARCH_BETWEEN|raw }} </span>
        <span class="dateset">{{ TPL_DATE_BETWEEN_START|raw }} </span>
        <span class="datetext mobilebreak">{{ TPL_SEARCH_AND|raw }} </span>
        <span class="dateset">{{ TPL_DATE_BETWEEN_END|raw }}</span>
      </span>
    </div>

    <br class="mobnarrowhide" />

    <div class="search-element">
      <span>{{ TPL_INCLUDE_POSTS|raw }}
      {{ TPL_LOCAL_CHECKBOX_POSTS|raw }}</span>
    </div>

    <div class="search-element">
      <span>{{ TPL_INCLUDE_COMMENTS|raw }}
      {{ TPL_LOCAL_CHECKBOX_COMMENTS|raw }}</span>
    </div>

    <div class="search-element">
      <span>{{ TPL_INCLUDE_EVENTS|raw }}
      {{ TPL_LOCAL_CHECKBOX_EVENTS|raw }}</span>
    </div>

    <div class="search-element">
      <span>{{ TPL_INCLUDE_ATTACHMENTS|raw }}
      {{ TPL_LOCAL_CHECKBOX_ATTACHMENTS|raw }}</span>
    </div>

    <div class="search-element">
      <span>{{ TPL_INCLUDE_BLURBS|raw }}
      {{ TPL_LOCAL_CHECKBOX_BLURBS|raw }}</span>
    </div>

    <br class="mobnarrowhide" />

    <input type="submit" value="{{ TPL_GO|raw }}" name="submitted_search" />

  </form>

</div>
