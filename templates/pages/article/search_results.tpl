<h1 class="headline-text">{{ TPL_SEARCH_RESULTS|raw }}</h1>

{{ TPL_STATUS_MESSAGES|raw }}

<p>{{ TPL_LOCAL_ADVANCED_SEARCH_RESULTS_INFO|raw }}</p>

<div class="list-search list-search-top">
  <form method="get" action="search_results.php">
    {{ TPL_TOP_SEARCH_FORM|raw }}
    {{ TPL_NAV|raw }}
  </form>
</div>

{{ TPL_TABLE_MIDDLE|raw }}

<div class="list-search list-search-bottom">
  <form method="get" action="search_results.php">
    {{ TPL_NAV|raw }}
    {{ TPL_TOP_SEARCH_FORM|raw }}
  </form>
</div>
