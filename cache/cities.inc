<div class="imc-network">

<span class="label-key">key:
<span class="active">active</span>,
<span class="archive">archive</span>
</span>

<br/><br/>

<div><a class="left archive" title="archive" href="http://www.indymedia.org/">indymedia.org</a></div>

<div><span class="broken"><br/><strong>africa</strong></span></div>
<div><a class="left broken" href="http://ambazonia.indymedia.org/">ambazonia</a></div>
<div><a class="left broken" href="http://canarias.indymedia.org">canarias</a></div>
<div><a class="left broken" href="http://estrecho.indymedia.org">estrecho / madiaq</a></div>
<div><a class="left broken" href="http://kenya.indymedia.org/">kenya</a></div>
<div><a class="left broken" href="http://nigeria.indymedia.org/">nigeria</a></div>
<div><a class="left broken" href="http://southafrica.indymedia.org/">south africa</a></div>

<div><span class="broken"><br/><strong>canada</strong></span></div>
<div><a class="left broken" href="http://hamilton.indymedia.org/">hamilton</a></div>
<div><a class="left broken" href="http://londonontario.indymedia.org">london, ontario</a></div>
<div><a class="left broken" href="http://maritimes.indymedia.org/">maritimes</a></div>
<div><a class="left broken" href="http://montreal.indymedia.org/">montreal</a></div>
<div><a class="left broken" href="http://ontario.indymedia.org/">ontario</a></div>
<div><a class="left broken" href="http://ottawa.indymedia.ca">ottawa</a></div>
<div><a class="left broken" href="http://quebec.indymedia.org/">quebec</a></div>
<div><a class="left broken" href="http://thunderbay.indymedia.org/">thunder bay</a></div>
<div><a class="left broken" href="http://bc.indymedia.org/">vancouver</a></div>
<div><a class="left broken" href="http://vicindymedia.org">victoria</a></div>
<div><a class="left broken" href="http://windsor.indymedia.org/">windsor</a></div>
<div><a class="left broken" href="http://winnipeg.indymedia.org">winnipeg</a></div>

<div><span class="broken"><br/><strong>east asia</strong></span></div>
<div><a class="left broken" href="http://burma.indymedia.org/">burma</a></div>
<div><a class="left broken" href="http://jakarta.indymedia.org/">jakarta</a></div>
<div><a class="left broken" href="http://japan.indymedia.org/">japan</a></div>
<div><a class="left broken" href="http://korea.indymedia.org">korea</a></div>
<div><a class="left broken" href="http://manila.indymedia.org/">manila</a></div>
<div><a class="left broken" href="http://qc.indymedia.org">qc</a></div>

<div><span><br/><strong>europe</strong></span></div>
<div><a class="left questionable" href="http://abruzzo.indymedia.org">abruzzo</a></div>
<div><a class="left broken" href="http://alacant.indymedia.org/">alacant</a></div>
<div><a class="left broken" href="http://andorra.indymedia.org">andorra</a></div>
<div><a class="left archive" title="archive" href="http://antwerpen.indymedia.org">antwerpen</a></div>
<div><a class="left broken" href="http://armenia.indymedia.org/">armenia</a></div>
<div><a class="left active" title="active" href="http://athens.indymedia.org/">athens</a></div>
<div><a class="left broken" href="http://austria.indymedia.org/">austria</a></div>
<div><a class="left active" title="active" href="http://barcelona.indymedia.org/">barcelona</a></div>
<div><a class="left broken" href="http://belarus.indymedia.org/">belarus</a></div>
<div><a class="left archive" title="archive" href="http://belgium.indymedia.org/">belgium</a></div>
<div><a class="left broken" href="http://belgrade.indymedia.org">belgrade</a></div>
<div><a class="left archive" title="archive" href="http://bristol.indymedia.org/">bristol</a></div>
<div><a class="left broken" href="http://bulgaria.indymedia.org">bulgaria</a></div>
<div><a class="left questionable" href="http://calabria.indymedia.org">calabria</a></div>
<div><a class="left broken" href="http://croatia.indymedia.org">croatia</a></div>
<div><a class="left archive" title="archive" href="http://cyprus.indymedia.org">cyprus</a></div>
<div><a class="left questionable" href="http://emiliaromagna.indymedia.org/">emilia-romagna</a></div>
<div><a class="left broken" href="http://estrecho.indymedia.org">estrecho / madiaq</a></div>
<div><a class="left broken" href="http://euskalherria.indymedia.org/">euskal herria</a></div>
<div><a class="left broken" href="http://galiza.indymedia.org">galiza</a></div>
<div><a class="left active" title="active" href="http://de.indymedia.org/">germany</a></div>
<div><a class="left broken" href="http://grenoble.indymedia.org">grenoble</a></div>
<div><a class="left broken" href="http://hungary.indymedia.org">hungary</a></div>
<div><a class="left broken" href="http://london.indymedia.org/">imc-london</a></div>
<div><a class="left active" title="active" href="http://www.indymedia.ie/">ireland</a></div>
<div><a class="left archive" title="archive" href="http://istanbul.indymedia.org/">istanbul</a></div>
<div><a class="left archive questionable" href="http://italy.indymedia.org/">italy</a></div>
<div><a class="left broken" href="http://laplana.indymedia.org">la plana</a></div>
<div><a class="left broken" href="http://liege.indymedia.org">liege</a></div>
<div><a class="left questionable" href="http://liguria.indymedia.org">liguria</a></div>
<div><a class="left active" title="active" href="http://lille.indymedia.org">lille</a></div>
<div><a class="left active" title="active" href="http://linksunten.indymedia.org">linksunten</a></div>
<div><a class="left broken" href="http://lombardia.indymedia.org/">lombardia</a></div>
<div><a class="left broken" href="http://madrid.indymedia.org/">madrid</a></div>
<div><a class="left broken" href="http://malta.indymedia.org">malta</a></div>
<div><a class="left broken" href="http://marseille.indymedia.org">marseille</a></div>
<div><a class="left active" title="active" href="http://nantes.indymedia.org">nantes</a></div>
<div><a class="left questionable" href="http://napoli.indymedia.org">napoli</a></div>
<div><a class="left active" title="active" href="http://indymedia.nl">netherlands</a></div>
<div><a class="left broken" href="http://nice.indymedia.org">nice</a></div>
<div><a class="left broken" href="http://www.indymedia.no/">norway</a></div>
<div><a class="left broken" href="http://ovl.indymedia.org">oost-vlaanderen</a></div>
<div><a class="left broken" href="http://paris.indymedia.org/">paris/île-de-france</a></div>
<div><a class="left broken" href="http://patras.indymedia.org/">patras</a></div>
<div><a class="left broken" href="http://piemonte.indymedia.org">piemonte</a></div>
<div><a class="left broken" href="http://poland.indymedia.org">poland</a></div>
<div><a class="left active" title="active" href="http://pt.indymedia.org/">portugal</a></div>
<div><a class="left questionable" href="http://roma.indymedia.org">roma</a></div>
<div><a class="left broken" href="http://romania.indymedia.org">romania</a></div>
<div><a class="left broken" href="http://russia.indymedia.org/">russia</a></div>
<div><a class="left broken" href="http://piter.indymedia.ru">saint-petersburg</a></div>
<div><a class="left archive" title="archive" href="http://www.indymediascotland.org/">scotland</a></div>
<div><a class="left broken" href="http://sweden.indymedia.org/">sverige</a></div>
<div><a class="left archive" title="archive" href="http://switzerland.indymedia.org/">switzerland</a></div>
<div><a class="left broken" href="http://thessaloniki.indymedia.org/">thessaloniki</a></div>
<div><a class="left broken" href="http://torun.indymedia.org/">torun</a></div>
<div><a class="left broken" href="http://toscana.indymedia.org/">toscana</a></div>
<div><a class="left broken" href="http://193.189.147.16/cmitlse/">toulouse</a></div>
<div><a class="left archive" title="archive" href="http://ukraine.indymedia.org/">ukraine</a></div>
<div><a class="left broken" href="http://www.indymedia.org.uk/">united kingdom</a></div>
<div><a class="left broken" href="http://valencia.indymedia.org">valencia</a></div>

<div><span><br/><strong>latin america</strong><span></div>
<div><a class="left active" title="active" href="http://argentina.indymedia.org/">argentina</a></div>
<div><a class="left broken" href="http://bolivia.indymedia.org/">bolivia</a></div>
<div><a class="left broken" href="http://chiapas.indymedia.org/">chiapas</a></div>
<div><a class="left broken" href="http://chile.indymedia.org">chile</a></div>
<div><a class="left broken" href="http://chilesur.indymedia.org">chile sur</a></div>
<div><a class="left broken" href="http://www.midiaindependente.org">cmi brasil</a></div>
<div><a class="left broken" href="http://colombia.indymedia.org/">colombia</a></div>
<div><a class="left broken" href="http://ecuador.indymedia.org/">ecuador</a></div>
<div><a class="left active" title="active" href="http://mexico.indymedia.org/">mexico</a></div>
<div><a class="left broken" href="http://peru.indymedia.org">peru</a></div>
<div><a class="left active" title="active" href="http://indymediapr.org">puerto rico</a></div>
<div><a class="left broken" href="http://qollasuyu.indymedia.org/">qollasuyu</a></div>
<div><a class="left active" title="active" href="http://rosario.indymedia.org/">rosario</a></div>
<div><a class="left broken" href="http://santiago.indymedia.org">santiago</a></div>
<div><a class="left broken" href="http://tijuana.indymedia.org/">tijuana</a></div>
<div><a class="left broken" href="http://uruguay.indymedia.org/">uruguay</a></div>
<div><a class="left archive" title="archive" href="http://valparaiso.indymedia.org">valparaiso</a></div>
<div><a class="left archive" title="archive" href="http://venezuela.indymedia.org/">venezuela</a></div>

<div><span><br/><strong>oceania</strong></span></div>
<div><a class="left broken" href="http://adelaide.indymedia.org/">adelaide</a></div>
<div><a class="left active" title="active" href="http://www.indymedia.org.nz/">aotearoa</a></div>
<div><a class="left broken" href="http://brisbane.indymedia.org/">brisbane</a></div>
<div><a class="left broken" href="http://burma.indymedia.org/">burma</a></div>
<div><a class="left broken" href="http://darwin.indymedia.org">darwin</a></div>
<div><a class="left broken" href="http://jakarta.indymedia.org/">jakarta</a></div>
<div><a class="left broken" href="http://manila.indymedia.org/">manila</a></div>
<div><a class="left broken" href="http://melbourne.indymedia.org/">melbourne</a></div>
<div><a class="left broken" href="http://perth.indymedia.org/">perth</a></div>
<div><a class="left broken" href="http://qc.indymedia.org">qc</a></div>
<div><a class="left broken" href="http://sydney.indymedia.org/">sydney</a></div>

<div><span><br/><strong>south asia</strong></span></div>
<div><a class="left archive" title="archive" href="http://india.indymedia.org/">india</a></div>
<div><a class="left broken" href="http://mumbai.indymedia.org/">mumbai</a></div>

<div><span><br/><strong>united states</strong></span></div>
<div><a class="left broken" href="http://arizona.indymedia.org/">arizona</a></div>
<div><a class="left broken" href="http://arkansas.indymedia.org/">arkansas</a></div>
<div><a class="left broken" href="http://asheville.indymedia.org/">asheville</a></div>
<div><a class="left broken" href="http://atlanta.indymedia.org/">atlanta</a></div>
<div><a class="left active" title="active" href="http://austin.indymedia.org/">austin</a></div>
<div><a class="left broken" href="http://baltimore.indymedia.org/">baltimore</a></div>
<div><a class="left archive" title="archive" href="http://bigmuddyimc.org/">big muddy</a></div>
<div><a class="left active" title="active" href="http://binghamton.indymedia.org">binghamton</a></div>
<div><a class="left active" title="active" href="http://boston.indymedia.org/">boston</a></div>
<div><a class="left broken" href="http://buffalo.indymedia.org/">buffalo</a></div>
<div><a class="left broken" href="http://cvilleindymedia.org/">charlottesville</a></div>
<div><a class="left active" title="active" href="http://chicago.indymedia.org/">chicago</a></div>
<div><a class="left questionable" href="http://cleveland.indymedia.org/">cleveland</a></div>
<div><a class="left archive" title="archive" href="http://colorado.indymedia.org/">colorado</a></div>
<div><a class="left broken" href="http://columbus.indymedia.org">columbus</a></div>
<div><a class="left broken" href="http://dc.indymedia.org/">dc</a></div>
<div><a class="left broken" href="http://hawaii.indymedia.org/">hawaii</a></div>
<div><a class="left broken" href="http://houston.indymedia.org">houston</a></div>
<div><a class="left broken" href="http://www.hm.indymedia.org/">hudson mohawk</a></div>
<div><a class="left broken" href="http://kcindymedia.org/">kansas city</a></div>
<div><a class="left active" title="active" href="http://la.indymedia.org/">la</a></div>
<div><a class="left broken" href="http://madison.indymedia.org/">madison</a></div>
<div><a class="left broken" href="http://maine.indymedia.org/">maine</a></div>
<div><a class="left broken" href="http://miami.indymedia.org/">miami</a></div>
<div><a class="left archive" title="archive" href="http://www.michiganimc.org/">michigan</a></div>
<div><a class="left broken" href="http://milwaukee.indymedia.org/">milwaukee</a></div>
<div><a class="left broken" href="http://twincities.indymedia.org/">minneapolis/st. paul</a></div>
<div><a class="left broken" href="http://nh.indymedia.org">new hampshire</a></div>
<div><a class="left broken" href="http://newjersey.indymedia.org/">new jersey</a></div>
<div><a class="left archive" title="archive" href="http://newmexico.indymedia.org/">new mexico</a></div>
<div><a class="left archive" title="archive" href="http://neworleans.indymedia.org/">new orleans</a></div>
<div><a class="left broken" href="http://chapelhill.indymedia.org/">north carolina</a></div>
<div><a class="left broken" href="http://www.ntimc.org/">north texas</a></div>
<div><a class="left archive" title="archive" href="http://nyc.indymedia.org/">nyc</a></div>
<div><a class="left broken" href="http://www.okimc.org">oklahoma</a></div>
<div><a class="left broken" href="http://www.phillyimc.org/">philadelphia</a></div>
<div><a class="left archive" title="archive" href="http://pittsburgh.indymedia.org">pittsburgh</a></div>
<div><a class="left active" title="active" href="http://portland.indymedia.org/">portland</a></div>
<div><a class="left archive" title="archive" href="http://richmond.indymedia.org/">richmond</a></div>
<div><a class="left active" title="active" href="http://rochester.indymedia.org/">rochester</a></div>
<div><a class="left questionable" href="http://rogueimc.org/">rogue valley</a></div>
<div><a class="left broken" href="http://reboot.stlimc.org">saint louis</a></div>
<div><a class="left active" title="active" href="http://sandiego.indymedia.org/">san diego</a></div>
<div><a class="left active" title="active" href="http://www.indybay.org/">san francisco bay area</a></div>
<div><a class="left broken" href="http://sbindymedia.org/">santa barbara</a></div>
<div><a class="left active" title="active" href="http://www.indybay.org/santacruz/">santa cruz, ca</a></div>
<div><a class="left broken" href="http://sarasota.indymedia.org">sarasota</a></div>
<div><a class="left broken" href="http://seattle.indymedia.org/">seattle</a></div>
<div><a class="left broken" href="http://tampabay.indymedia.org">tampa bay</a></div>
<div><a class="left broken" href="http://tnimc.org/">tennessee</a></div>
<div><a class="left active" title="active" href="http://www.ucimc.org/">urbana-champaign</a></div>
<div><a class="left broken" href="http://vermont.indymedia.org/">vermont</a></div>
<div><a class="left broken" href="http://wmass.indymedia.org/">western mass</a></div>
<div><a class="left broken" href="http://worcester.indymedia.org">worcester</a></div>

<div><span><br/><strong>west asia</strong></span></div>
<div><a class="left broken" href="http://armenia.indymedia.org/">armenia</a></div>
<div><a class="left archive" title="archive" href="http://beirut.indymedia.org">beirut</a></div>
<div><a class="left archive" title="archive" href="http://israel.indymedia.org/">israel</a></div>
<div><a class="left archive" title="archive" href="http://www.indymedia.org/en/static/palestine">palestine</a></div>

<div><span><br/><strong>process</strong></span></div>
<div><a class="left archive" title="archive" href="http://www.indymedia.org/fbi/">fbi/legal updates</a></div>
<div><a class="left broken" href="http://lists.indymedia.org/">mailing lists</a></div>
<div><a class="left broken" href="http://docs.indymedia.org/">process &amp; imc docs</a></div>
<div><a class="left broken" href="http://tech.indymedia.org/">tech</a></div>
<div><a class="left broken" href="http://volunteer.indymedia.org/">volunteer</a></div>

<div><span><br/><strong>projects</strong></span></div>
<div><a class="left broken" href="http://print.indymedia.org/">print</a></div>
<div><a class="left broken" href="http://radio.indymedia.org/">radio</a></div>
<div><a class="left archive" title="archive" href="http://satellite.indymedia.org/">satellite tv</a></div>
<div><a class="left broken" href="http://video.indymedia.org">video</a></div>

<div><span class="broken"><br/><strong>regions</strong></span></div>
<div><a class="left broken" href="http://oceania.indymedia.org">oceania</a></div>
<div><a class="left broken" href="http://indymedia.us/">united states</a></div>

<div><span class="broken"><br/><strong>topics</strong></span></div>
<div><a class="left broken" href="http://biotech.indymedia.org/">biotech</a></div>

</div>
