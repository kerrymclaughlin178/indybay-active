<?php

/**
 * @file
 * Password reset utility script.
 *
 * Usage: `php scripts/reset-password.php {username} {password}`
 */

use Indybay\DB\UserDB;

require_once __DIR__ . '/../classes/config/indybay.cfg';

if (empty($argv[1])) {
  throw new InvalidArgumentException('Missing username.');
}

$user = new UserDB();

$result = $user->singleColumnQuery("SELECT user_id FROM user WHERE username = '" . $user->prepareString($argv[1]) . "'");

if (empty($result[0])) {
  throw new InvalidArgumentException('Could not find user.');
}

if (empty($argv[2])) {
  throw new InvalidArgumentException('Missing password.');
}

$user->changeUserPassword($result[0], $argv[2]);

echo 'Password reset successfully :)', PHP_EOL;
