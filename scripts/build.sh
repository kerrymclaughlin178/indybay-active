#!/bin/sh
set -e
cp -a node_modules/typeface-noticia-text website/themes/fonts
cp -a node_modules/jquery/dist/jquery.min.js website/js
cp -a node_modules/jquery-validation/dist/jquery.validate.min.js website/js
mkdir -p website/js/jquery.ui
cp -a node_modules/jquery-ui-dist/jquery-ui.min.* node_modules/jquery-ui-dist/images website/js/jquery.ui
