<?php

/**
 * @file
 * Database migration script for `composer migrate`.
 */

require_once __DIR__ . '/../classes/config/indybay.cfg';

use Indybay\DB\DB;

$db = new DB();

echo 'Running database migrations, please wait... 👷', PHP_EOL;

$result = $db->executeStatement('ALTER TABLE media_attachment ADD COLUMN browser_compat tinyint(3) DEFAULT NULL');
if ($result < 0) {
  error_log("Attempted to re-add browser_compat column: $result");
}

exit('Done :) 💃' . PHP_EOL);
