<?php

/**
 * @file
 * Renders the admin page header.
 */

use Indybay\Date;
use Indybay\Translate;

$sftr = new Translate();

$date = new Date();
$date->setTimeZone();
$fdate = $date->getFormattedDate();
$ftime = $date->getFormattedTime();
$username = $_SESSION['session_username'];

if (array_key_exists('time_diff', $GLOBALS) && $GLOBALS['time_diff'] > 0) {
  $tz = '+' . $GLOBALS['time_diff'];
}
elseif (array_key_exists('time_diff', $GLOBALS)) {
  $tz = $GLOBALS['time_diff'];
}
else {
  $tz = 0;
}

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8" />
    <meta name="MobileOptimized" content="width" />
    <meta name="HandheldFriendly" content="true" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="robots" content="noindex, nofollow" />
    <title>Indybay Admin</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<?php include INCLUDE_PATH . '/common/javascript.inc'; ?>

<script src="/js/admin.js"></script>

<link type="text/css" rel="stylesheet" href="/themes/styles.css<?php echo CSS_JS_QUERY_STRING; ?>" media="all" />
<link type="text/css" rel="stylesheet" href="/notice/notice.css<?php echo CSS_JS_QUERY_STRING; ?>" media="all" />
<link type="text/css" rel="stylesheet" href="/themes/fonts/gf-halda-smashed-indybay/index.css<?php echo CSS_JS_QUERY_STRING; ?>" media="all" />
<link type="text/css" rel="stylesheet" href="/themes/fonts/typeface-noticia-text/index.css<?php echo CSS_JS_QUERY_STRING; ?>" media="all" />

</head>
<body class="admin">

<div id="sitewrapper"><div id="shadow-maincontent" class="closenav"></div>

  <div id="siteinner">

    <div class="mast">

      <div class="mastimage">
        <img src="/im/banner-home.png" alt="" />
      </div>

      <div class="mast-inner">

        <!-- <div id="shadow-mast" class="closenav"></div> -->

        <div class="mastleft">
          <div class="masttitle">Indybay</div>
          <div class="masttitleshadow">Indybay</div>
          <div class="mastcheer">
            <a href="/" title="Indybay home">
              <img src="/im/banner_cheer.svg" alt="protest cheer" />
            </a>
          </div>
        </div><!-- END .mastleft -->

        <div class="mastright">

          <div class="headerlogo">
            <a href="/" title="home">
              <img src="/im/banner_logo.svg" alt="Indybay" />
            </a>
          </div>

          <div class="headerbuttons">

            <div id="headerbutton-admin" class="headerbutton" title="Admin">
              <img src="/im/menu_gear-red.svg" alt="admin menu" />
            </div>

          </div><!-- END #headerbuttons -->

        </div><!-- END .mastright -->

      </div><!-- END .mast-inner -->

      <div class="headermenus">

        <?php
        include INCLUDE_PATH . '/admin/admin-nav.inc';
        ?>

      </div><!-- END .headermenus -->

    </div><!-- END .mast -->

    <!-- main table for page -->

    <div class="adminbreadcrumbnav">
      <span>
        <strong>
          <?php
          echo $sftr->trans('adminsite');
          ?>
           : <a class="bar" href="/admin/logout.php"><?php echo $sftr->trans('adminlogout'); ?></a>
           : <a class="bar" target="_blank" href="/"><?php echo $sftr->trans('adminviewsite'); ?></a>
        </strong>
      </span>
    </div>

    <div class="admincontent">
