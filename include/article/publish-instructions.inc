<!-- publishing guidelines -->

<div class="inner-wrap">

  <div><h1 class="headline-text">Publish to the Newswire</h1></div>

  <p>Use the <a
  href="/calendar/event_add.php?page_id=TPL_LOCAL_PAGE_ID">Add an Event</a> form to publish announcements to the <a
  href="/calendar/?page_id=TPL_LOCAL_PAGE_ID">calendar</a>. This is for newswire posts.</p>

  <div class="clicktip">

    <p class="trigger" title="click here to learn more">Never published to the newswire before? Read the publishing guidelines!</p>

    <div class="target">

      <p>Want to show off your photos of an event? Got audio or video that people just
      have to see? This is the place to put it. We want to hear your story. Use any format you want, from journalistic, to academic, to
      personal account.</p>

      <p><strong>WHAT TO PUBLISH:</strong> Please use this form to contribute new stories and ideas. We think comments
      belong with the story being discussed. So to have your say in response to a story on the site, please use the &quot;add your
      comments&quot; link at the bottom of each story. Instead of reposting corporate media articles to the site, try posting your own
      critique or summary, short quotes, and links.</p>

      <p><strong>MULTIMEDIA:</strong>
      If your image exceeds 800 pixels in width or 500 pixels in height, it
      will be resized to these maximums.  We suggest that you crop and resize your images yourself to obtain the
      results you're looking for; a variety of free (libre) open-source software is available for this purpose.
      If you need help converting your sounds or video to a compressed digital file, consult our <a
      href="/news/2003/12/1665907.php">tutorial</a> page. For playability of media files on browsers and mobile devices, upload videos as MP4s with H.264 encoding. For audio, upload MP3s.</p>

      <p><strong>EDITORIAL POLICY:</strong>
      After stories have been published, they may be edited or hidden by the
      collective running this site. We generally only fix obvious mistakes, such
      as typos, or try to improve the formatting. Please read our full
      <strong><a href="/news/2002/08/139500.php">editorial
      policy</a></strong>.</p>

      <p><strong>LEGALESE:</strong>
      Unless otherwise stated, all content contributed to this site is free for
      non-commercial reuse, reprint or rebroadcast. If you want to specify different conditions, please do so in the summary, including any
      &copy; copyright (or copyleft) statement. Please read our <a href="/news/2003/12/1665906.php">privacy</a>, <a href="/newsitems/2014/10/05/18762449.php">copyright policy</a>, and <a
      href="/news/2003/12/1665905.php">disclaimer</a> statements before continuing.</p>

    </div><!-- END .target -->

  </div><!-- END .clicktip -->

</div><!-- END .inner-wrap -->

<!-- <br clear="all" /> -->
<!-- /publishing guidelines -->
