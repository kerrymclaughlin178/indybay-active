<?php

/**
 * @file
 * Renders the Indybay header links.
 */
?>
<div id="headernav">
  <div class="navbar navbarleft">
    <a class="navbarlink" href="/" title="Indybay Home">Home</a>
  </div>
  <div class="navbar">
    <a class="navbarlink" href="/news/2003/12/1664397.php" title="About the IMC">About</a>
  </div>
  <div class="navbar">
    <a class="navbarlink" href="/news/2003/12/1665899.php" title="Talk to Us">Contact</a>
  </div>
  <div class="navbar">
    <a class="navbarlink" href="/subscribe/" title="News Services">Subscribe</a>
  </div>
  <div class="navbar">
    <a class="navbarlink" href="/calendar/?page_id=<?php
    $current_url = $_SERVER['SCRIPT_NAME'];
    $page_id = '';
    if (strpos($current_url, 'news') < 1 && count($GLOBALS['page_ids']) == 1) {
      $page_id = $GLOBALS['page_ids'][0];
      echo $page_id;
    } ?>" title="Event Announcements">Calendar</a>
  </div>
  <div class="navbar">
    <a class="navbarlink" href="/publish.php<?php
    if (isset($GLOBALS['page_ids'][0]) && count($GLOBALS['page_ids']) == 1) {
      echo '?page_id=' . $GLOBALS['page_ids'][0];
    } ?>" title="Publish Your News">Publish</a>
  </div>
  <div class="navbar lastnavbar">
    <a class="navbarlink" href="/donate" title="Support Indpendent Media">Donate</a>
  </div>
</div><!-- END #headernav -->
