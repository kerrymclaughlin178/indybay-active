<?php

/**
 * @file
 * Renders feature pages.
 */

$GLOBALS['body_class'] = 'page-feature';

include INCLUDE_PATH . '/common/content-header.inc';
include INCLUDE_PATH . '/common/index_center.inc';
include INCLUDE_PATH . '/calendar/featurepage_event_list.inc';
include INCLUDE_PATH . '/common/index_bottom.inc';
include INCLUDE_PATH . '/common/center_right.inc';
include INCLUDE_PATH . '/common/footer.inc';
