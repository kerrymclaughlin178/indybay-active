<?php

/**
 * @file
 * Renders end of center column and start of right column.
 */

use Indybay\Translate;

$sftr = new Translate();

$page_id = $GLOBALS['page_ids'][0] ?? $GLOBALS['page_id'] ?? FRONT_PAGE_CATEGORY_ID;

?>
<div class="rightcol tabmenu">
  <div class="newswire tabmenu">

    <div class="newswirewrap newswirewrap-header">
        <!-- <div align="center" class="publink">
          <a class="publishlink"
          href="/publish.php<php
          if ($page_id != FRONT_PAGE_CATEGORY_ID) {
            echo '?page_id=', $page_id;
          }
          ?>"><php echo $sftr->trans('publish_your_news'); ?></a>
        </div> -->
        <!-- END .publink -->
        <!-- <div align="center" class="eventlink"><a
          class="publishlink" href="/calendar/event_add.php<php
          if ($page_id != FRONT_PAGE_CATEGORY_ID) {
            echo '?page_id=', $page_id;
          }
          ?>"><php echo $sftr->trans('add_an_event'); ?></a>
        </div> -->
        <!-- END .eventlink -->

        <div class="newswirewrap-header-title">Newswire media types:</div>

        <?php include INCLUDE_PATH . '/common/media_key.inc'; ?>

    </div><!-- END .newswirewrap-header -->

        <?php
        include CACHE_PATH . '/newswires/newswire_page' . $page_id;
        ?>

        <div class="rss-link" style="">
          <a href="/syn/generate_rss.php?news_item_status_restriction=1155&amp;page_id=<?php
          if ($page_id != FRONT_PAGE_CATEGORY_ID) {
            echo $page_id;
          }
          ?>"><img src="/images/feed-icon-16x16.png"
          width="16" height="16" border="0" alt="feed" title="Newswire RSS feed" /></a>
        </div><!-- END .rss-link -->

  </div><!-- END .newswire -->
</div><!-- END .rightcol -->
