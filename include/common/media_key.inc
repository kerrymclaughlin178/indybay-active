<?php

/**
 * @file
 * Renders media icons.
 */
?>
<table class="nooz-types">
<tr><td class="">
  <a href="/search/?page_id=<?php print $GLOBALS['page_ids'][0]; ?>&amp;include_posts=1&amp;include_attachments=0&amp;media_type_grouping_id=1"><img
  style="" src="/im/imc_article.svg"
  border="0" alt="browse articles" /></a>
  <a class="newswire"
  href="/search/?page_id=<?php print $GLOBALS['page_ids'][0];
  ?>&amp;include_posts=1&amp;include_attachments=0&amp;media_type_grouping_id=1">article</a>
</td><td class="">
  <a href="/gallery/?page_id=<?php print $GLOBALS['page_ids'][0]; ?>&amp;include_posts=1&amp;include_attachments=1&amp;media_type_grouping_id=2"><img
  style="" src="/im/imc_photo.svg" border="0"
  alt="browse photos" /></a>
  <a class="newswire"
  href="/gallery/?page_id=<?php print $GLOBALS['page_ids'][0];
  ?>&amp;include_posts=1&amp;include_attachments=1&amp;media_type_grouping_id=2&amp;news_item_status_restriction=690690">photo</a>
</td></tr><tr><td class="">
  <a href="/search/?page_id=<?php print $GLOBALS['page_ids'][0]; ?>&amp;include_posts=1&amp;include_attachments=1&amp;media_type_grouping_id=4"><img
  style="" src="/im/imc_video.svg" border="0"
  alt="browse videos" /></a>
  <a class="newswire"
  href="/search/?page_id=<?php print $GLOBALS['page_ids'][0]; ?>&amp;include_posts=1&amp;include_attachments=1&amp;media_type_grouping_id=4">video</a>
</td><td class="">
  <a href="/search/?page_id=<?php print $GLOBALS['page_ids'][0]; ?>&amp;include_posts=1&amp;include_attachments=1&amp;media_type_grouping_id=3"><img
  style="" src="/im/imc_audio.svg" border="0"
  alt="browse audio" /></a>
  <a class="newswire"
  href="/search/?page_id=<?php print $GLOBALS['page_ids'][0]; ?>&amp;include_posts=1&amp;include_attachments=1&amp;media_type_grouping_id=3">audio</a>
</td></tr>
<!--
<tr><td class="" colspan="4">
  <a href="/torrents/"><img style=""
  border="0" alt="bittorrent"
  src="/im/bittorrent.png" title="BitTorrent" /></a> <a title="torrents"
  href="/torrents/" rel="tag" class="newswire">bittorrent downloads</a>
</td></tr>
-->
</table>
