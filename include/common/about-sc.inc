<?php

/**
 * @file
 * Renders the SC IMC header links.
 */
?>
<div id="headernav">
  <div class="navbar navbarleft">
    <a class="navbarlink" href="/" title="Indybay Home">Home</a>
  </div>
  <div class="navbar">
    <a class="navbarlink" href="/newsitems/2003/12/08/16643971.php" title="About the IMC">About</a>
  </div>
  <div class="navbar">
    <a class="navbarlink" href="/newsitems/2003/12/15/16658991.php" title="Contact Us">Contact</a>
  </div>
  <div class="navbar">
    <a class="navbarlink" href="https://lists.riseup.net/www/info/scimc-news" title="SC-IMC Newsletter">Newsletter</a>
  </div>
  <div class="navbar">
    <a class="navbarlink" href="/calendar/?page_id=60" title="SC-IMC Calendar">Calendar</a>
  </div>
  <div class="navbar">
    <a class="navbarlink" href="/publish.php?page_id=60" title="Publish Your News">Publish</a>
  </div>
  <div class="navbar lastnavbar">
    <a class="navbarlink" href="/donate" title="Support Indpendent Media">Donate</a>
  </div>
</div><!-- END #headernav -->
