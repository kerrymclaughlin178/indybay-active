<?php

/**
 * @file
 * Renders page footer.
 */
?>

    </div><!-- END .pagecontent -->
  </div><!-- END .page -->

</div><!-- END #maincontent -->

<div class="footer">

  <div class="disclaimer">

    <?php include INCLUDE_PATH . '/common/disclaimer.inc'; ?>

  </div><!-- end .disclaimer -->

</div><!-- end .footer -->


<script><!--//--><![CDATA[//><!--
  $(function() {
    //$('#indy-link').click(function(){ $('.cities-list').load('/cities.html'); return false; });
    var indybayDonation = '<?php !is_readable(CACHE_PATH . '/paypal201005.txt') ?: readfile(CACHE_PATH . '/paypal201005.txt'); ?>';
    $('.indybay-donation').html(indybayDonation);
<?php if (0 && !isset($_COOKIE['censored'])) : ?>
    <?php $GLOBALS['ui']['widget'] = TRUE; ?>
    <?php $GLOBALS['ui']['button'] = TRUE; ?>
    <?php $GLOBALS['ui']['draggable'] = TRUE; ?>
    <?php $GLOBALS['ui']['mouse'] = TRUE; ?>
    <?php $GLOBALS['ui']['position'] = TRUE; ?>
    <?php $GLOBALS['ui']['resizable'] = TRUE; ?>
    <?php $GLOBALS['ui']['dialog'] = TRUE; ?>
    <?php setcookie('censored', '1', time() + 10 * 60, '/'); ?>
    $( "#stop-sopa" ).dialog({
      modal: true,
      width: 550,
      resizable: false,
      buttons: {
        Continue: function() {
          $( this ).dialog( "close" );
        }
      }
    });
<?php endif; ?>
  });
//--><!]]></script>


</div><!-- END #siteinner -->
</div><!-- END #sitewrapper -->

<div id="notices"></div>

</body>
</html>

<?php
if (array_key_exists('log_id', $GLOBALS) && $GLOBALS['log_id'] + 0 != 0) {
  $log_class = new LogDB();
  $log_class->updateLogIdEndTime();
}
