truncate legacy_category_mapping;
truncate category;
truncate page;
truncate page_category;
insert into  legacy_category_mapping (legacy_system_id, legacy_category_id, category_id, page_id) select  1, category_id,category_id,category_id from legacy_sfactive_category;
insert into  category (category_id, parent_category_id, category_type_id, name,description) select  category_id, parentid,1, name,description from legacy_sfactive_category;
insert into  page (page_id, short_display_name, long_display_name,center_column_type_id,newswire_type_id,items_per_newswire_section  )
 select  category_id, shortname,name,1,1,summarylength  from legacy_sfactive_category;
insert into  page_category select category_id,category_id from legacy_sfactive_category;
update page set newswire_type_id=2 where page_id in (select category_id from legacy_sfactive_category where newswire='f');
update category set parent_category_id=null where parent_category_id=1;
update category set category_type_id=2 where category_id in (select category_id from legacy_sfactive_category where catclass='t');
update category set category_type_id=2 where category_id in (select category_id from legacy_sfactive_category where catclass='m');
update page set relative_path=short_display_name where page_id in (select category_id from category where parent_category_id is null);
update page set relative_path=CONCAT('international/',short_display_name) where page_id in (select category_id from category where parent_category_id is not null);
update page set relative_path='', include_in_readmore_links=0, short_display_name='frontpage' where short_display_name='production'


