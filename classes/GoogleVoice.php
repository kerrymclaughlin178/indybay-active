<?php

/**
 * Provides Google Voice services.
 */
class GoogleVoice {

  /**
   * Our credentials.
   *
   * @var string
   */
  private $login;

  /**
   * Our credentials.
   *
   * @var string
   */
  private $pass;

  /**
   * Some crazy google thing that we need later.
   *
   * @var string
   */
  private $rnrSe;

  /**
   * Our private curl handle.
   *
   * @var resource
   */
  private $ch;

  /**
   * The location of our cookies.
   *
   * @var string
   */
  private $cookieFile;

  /**
   * Are we logged in already?
   *
   * @var bool
   */
  private $loggedIn = FALSE;

  /**
   * Class constructor.
   */
  public function __construct($login, $pass) {
    $this->login = $login;
    $this->pass = $pass;

    $this->cookieFile = '/tmp/gvCookies.txt';

    $this->ch = curl_init();
    curl_setopt($this->ch, CURLOPT_COOKIEJAR, $this->cookieFile);
    curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, TRUE);
    curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($this->ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0)');
  }

  /**
   * Login.
   */
  private function logIn() {
    if ($this->loggedIn) {
      return TRUE;
    }

    // Fetch the login page.
    curl_setopt($this->ch, CURLOPT_URL, 'https://accounts.google.com/ServiceLogin?passive=true&service=grandcentral');
    $html = curl_exec($this->ch);

    if (preg_match('/name="GALX"\s*value="([^"]+)"/', $html, $match)) {
      $galx = $match[1];
    }
    else {
      throw new Exception('Could not parse for GALX token');
    }

    curl_setopt($this->ch, CURLOPT_URL, 'https://accounts.google.com/ServiceLoginAuth?service=grandcentral');
    curl_setopt($this->ch, CURLOPT_POST, TRUE);
    curl_setopt($this->ch, CURLOPT_POSTFIELDS, [
      'Email' => $this->login,
      'Passwd' => $this->pass,
      'continue' => 'https://www.google.com/voice/account/signin',
      'service' => 'grandcentral',
      'GALX' => $galx,
    ]);

    $html = curl_exec($this->ch);
    if (preg_match('/name="_rnr_se".*?value="(.*?)"/', $html, $match)) {
      $this->rnrSe = $match[1];
    }
    else {
      throw new Exception('Could not log in to Google Voice with username: ' . $this->login);
    }
  }

  /**
   * Send SMS.
   */
  public function sendSms($number, $message) {
    $this->logIn();

    curl_setopt($this->ch, CURLOPT_URL, 'https://www.google.com/voice/sms/send/');
    curl_setopt($this->ch, CURLOPT_POST, TRUE);
    curl_setopt($this->ch, CURLOPT_POSTFIELDS, [
      '_rnr_se' => $this->rnrSe,
      'phoneNumber' => $number,
      'text' => $message,
    ]);
    curl_exec($this->ch);
  }

}
