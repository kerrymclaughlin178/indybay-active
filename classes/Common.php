<?php

namespace Indybay;

/**
 * Written January 2005.
 *
 * Modification Log:
 * 12/2005 - 1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development
 * --------------------------------------------
 * this class is responsible for common functions used by any class
 * in the system.
 */

/**
 * Indybay base class.
 */
class Common {

  /**
   * Renders a template.
   */
  public function render($template, $variables) {
    static $twig;
    if (!isset($twig)) {
      $loader = new \Twig_Loader_Filesystem(TEMPLATE_PATH);
      $twig = new \Twig_Environment($loader, [
        'cache' => CACHE_PATH . '/templates',
      ]);
    }
    return $twig->render($template, $variables);
  }

}
