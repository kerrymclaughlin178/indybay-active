<?php

namespace Indybay\Syndication;

use Indybay\MediaAndFileUtil\FileUtil;
use Indybay\Pages\Rss\RssGenerator;

/**
 * Legacy RSS generator.
 *
 * Looks for archived files and if they dont exist or are old
 * maps old rss names to new searchs, runs them, generates the files
 * and then returns the text of thefile.
 */
class LegacyRssGenerator {

  /**
   * Generates RSS.
   */
  public function generateRss($file_name) {

    // Look for existing file and get date.
    $full_path = WEB_PATH . '/syn/cache/' . $file_name;
    $file_util = new FileUtil();

    $recentFileExists = TRUE;

    if (file_exists($full_path)) {
      $age_in_secs = $file_util->getFileAgeInSeconds($full_path);
      if ($age_in_secs > 600) {
        $recentFileExists = FALSE;
      }
      else {
        $recentFileExists = TRUE;
      }
    }
    else {
      $recentFileExists = FALSE;
    }

    if ($recentFileExists) {
      // Return string from file.
      $str = $file_util->loadFileAsString($full_path);
    }
    else {

    }

    $search_fields = [];
    $search_fields['rss_version'] = 2;
    if (strrpos($file_name, '.rdf') > 0) {
      $search_fields['rss_version'] = 1;
    }
    // Set search params based off old file name (or new name)
    $search_fields['include_comments'] = 0;
    $search_fields['include_posts'] = 0;
    $search_fields['include_blurbs'] = 0;
    $search_fields['include_events'] = 0;
    $search_fields['include_full_text'] = 0;
    $search_fields['news_item_status_restriction'] = NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED * NEWS_ITEM_STATUS_ID_NONLOCAL_HIGHLIGHTED;
    switch ($file_name) {
      case 'production_features_long.rdf':
        $search_fields['include_blurbs'] = 1;
        $search_fields['news_item_status_restriction'] = NEWS_ITEM_STATUS_ALL_NONHIDDEN;
        $search_fields['include_full_text'] = 1;
        break;

      case 'features.rdf':
        $search_fields['include_blurbs'] = 1;
        $search_fields['news_item_status_restriction'] = NEWS_ITEM_STATUS_ALL_NONHIDDEN;
        break;

      case 'newswire.rss':
        $search_fields['include_posts'] = 1;
        $search_fields['include_full_text'] = 0;
        break;

      case 'newswire.2.rss':
        $search_fields['include_posts'] = 1;
        $search_fields['include_full_text'] = 0;
        break;
    }
    $rss_generator = new RssGenerator();
    // Runs search and creates rss or rdf.
    $str = $rss_generator->generate($search_fields);

    // Saves rss or rdf to file.
    $file_util->saveStringAsFile($full_path, $str);

    // Return string.
    return $str;
  }

}
