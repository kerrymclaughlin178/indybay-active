<?php

namespace Indybay\Syndication;

/**
 * RSS aggregator.
 */
class RSSAggregator {

  /**
   * Parse with SimplePie.
   */
  public function simplePie($feed_url) {
    $feed = new \SimplePie();
    $feed->set_cache_location(CACHE_PATH . '/SimplePie');
    $feed->set_feed_url($feed_url);
    $feed->init();
    return $feed;
  }

  /**
   * Renders feed for javascript.
   */
  public function renderForJavascript() {
    $rss_file_features = 'generate_rss.php?include_posts=0&include_attachments=0&include_events=0&include_blurbs=1';
    $feed_features = $this->simplePie(FULL_ROOT_URL . 'syn/' . $rss_file_features);
    $rss_file_newswire = 'generate_rss.php?include_posts=1&include_attachments=0&include_events=1&news_item_status_restriction=15&include_blurbs=0';
    $feed_newswire = $this->simplePie(FULL_ROOT_URL . 'syn/' . $rss_file_newswire);
    if ($feed_features && $feed_newswire) {
      foreach ($feed_features->get_items() as $item) {
        $allstories[] = [
          'title' => $item->get_title(),
          'link' => $item->get_permalink(),
          'date' => $item->get_date('c'),
        ];
      }
      foreach ($feed_newswire->get_items() as $item) {
        $allstories[] = [
          'title' => $item->get_title(),
          'link' => $item->get_permalink(),
          'date' => $item->get_date('c'),
        ];
      }
      $sortedstories = $this->sortArray($allstories, 'date', 'DESC');
      $sortedstories = array_slice($sortedstories, 0, 10);
    }
    return $sortedstories;
  }

  /**
   * Sorts array.
   */
  public function sortArray() {
    $arguments = func_get_args();
    $array = $arguments[0];
    usort($array, function ($a, $b) use ($arguments) {
      for ($c = 1; $c < count($arguments); $c += 2) {
        if (in_array($arguments[$c + 1], ['ASC', 'DESC'])) {
          if ($a[$arguments[$c]] != $b[$arguments[$c]]) {
            if ($arguments[$c + 1] == 'ASC') {
              return ($a[$arguments[$c]] < $b[$arguments[$c]] ? -1 : 1);
            }
            else {
              return ($a[$arguments[$c]] < $b[$arguments[$c]] ? 1 : -1);
            }
          }
        }
      }
      return 0;
    });
    return $array;
  }

}
