<?php

namespace Indybay\Cache;

/**
 * Written January 2006.
 *
 * Modification Log:
 * 1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */

use Indybay\Date;

/**
 * Generates cache files updated when a news_item is changed or added.
 */
class DependentListCache extends Cache {

  /**
   * Updates all lists on the site when an article (event or post) is added.
   *
   * For posts this means newswires and for events it could mean eventlinks and
   * calendar week caches.
   */
  public function updateAllDependenciesOnAdd($article_info) {
    $newswire_cache_class = new NewswireCache();
    $newswire_cache_class->regenerateNewswiresForNewNewsitem($article_info['news_item_id']);
    if ($article_info['news_item_type_id'] == NEWS_ITEM_TYPE_ID_EVENT) {
      $event_cache_class = new EventCache();
      $year = $article_info['displayed_date_year'];
      $month = $article_info['displayed_date_month'];
      $day = $article_info['displayed_date_day'];
      $date = new Date();
      $date->setTime(0, 0, $day, $month, $year);
      $event_cache_class->recacheWeekGivenDate($date);
    }
  }

  /**
   * Update all lists on the site when an article (event or post) is updated.
   *
   * For posts this means newswires and for events it could mean eventlinks and
   * calendar week caches.
   */
  public function updateAllDependenciesOnUpdate($old_article_info, $new_article_info, $old_cat_list) {

    $old_news_item_type_id = $old_article_info['news_item_type_id'];
    $new_news_item_type_id = $new_article_info['news_item_type_id'];

    // Update newswires if needed.
    if ($new_news_item_type_id == NEWS_ITEM_TYPE_ID_EVENT ||$old_news_item_type_id == NEWS_ITEM_TYPE_ID_EVENT
    || $new_news_item_type_id == NEWS_ITEM_TYPE_ID_POST || $old_news_item_type_id == NEWS_ITEM_TYPE_ID_POST) {
      $newswire_cache_class = new NewswireCache();
      $newswire_cache_class->regenerateNewswiresForNewsitem($new_article_info['news_item_id'], $old_cat_list,
       $old_article_info['news_item_status_id'], $new_article_info['news_item_status_id']);
    }

    // Update calendar weeks, highlighted lists and upcomming events.
    if ($new_news_item_type_id == NEWS_ITEM_TYPE_ID_EVENT ||
    $old_news_item_type_id == NEWS_ITEM_TYPE_ID_EVENT) {

      $event_list_cache_class = new EventListCache();
      if ($old_news_item_type_id != NEWS_ITEM_TYPE_ID_EVENT) {
        // @fixme: Commenting out because this method does not exist!
        // $event_list_cache_class->update_caches_on_add($new_article_info);
      }
      else {
        $event_list_cache_class->updateCachesOnChange($old_article_info, $new_article_info);
      }

    }

  }

}
