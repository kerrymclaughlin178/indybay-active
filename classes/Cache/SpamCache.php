<?php

namespace Indybay\Cache;

use Indybay\MediaAndFileUtil\FileUtil;

/**
 * New Blocking System (as opposed to legacy one from a year earlier).
 *
 * Consists of lists of ips that will get redirected to a spam page for all but
 * the front page, lists of keywords, title words and ips that will result in
 * posting as hidden and finally a list of keywords that will cause validation
 * failure on post with messages telling those posting not to use the keywords
 * (examples being things like "[url=" that seem to only be in spam as well as
 * phrases like "download free porn" etc...)
 *
 * Written June 2007.
 * Modification Log:
 * 5-6/2007  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class SpamCache extends Cache {

  /**
   * Looks for specific redirection urls based off HTTP params.
   *
   * The last part of this method is the main hook-in to the
   * continuing functionality from the legacy spam system.
   */
  public function getAlternateSpamBlockPage() {

    // Set local vars from http headers.
    $current_url = $_SERVER['SCRIPT_NAME'];
    if ($current_url == '/index.php' || strpos($current_url, 'admin') == 1) {
      return '';
    }

    if (getenv('REMOTE_ADDR')) {
      $current_ip = getenv('REMOTE_ADDR');
    }
    elseif (getenv('HTTP_CLIENT_IP')) {
      $current_ip = getenv('HTTP_CLIENT_IP');
    }
    elseif (getenv('HTTP_X_FORWARDED_FOR')) {
      $current_ip = getenv('HTTP_X_FORWARDED_FOR');
    }
    else {
      $current_ip = 'UNKNOWN';
    }

    $ip_pieces = explode('.', $current_ip);
    $current_ip1 = trim($ip_pieces[0]);
    $current_ip2 = isset($ip_pieces[1]) ? trim($ip_pieces[1]) : '';
    $threepartip = $current_ip1 . '.' . $current_ip2 . '.' . $current_ip1 . '.*';
    $twopartip = $current_ip1 . '.' . $current_ip2 . '.*';
    $onepartip = $current_ip1 . '.*';

    $iplist = $this->loadIpBlockFile();

    $test = getenv('HTTP_CLIENT_IP');
    if (strlen($test) > 5 && strpos(' ' . $iplist, $test) > 0) {
      return FULL_ROOT_URL . 'spam2.php';
    }
    $test = getenv('HTTP_X_FORWARDED_FOR');
    if (strlen($test) > 5 && strpos(' ' . $iplist, $test) > 0) {
      return FULL_ROOT_URL . 'spam2.php';
    }
    $test = getenv('REMOTE_ADDR');
    if (strlen($test) > 5 && strpos(' ' . $iplist, $test) > 0) {
      return FULL_ROOT_URL . 'spam2.php';
    }

    if (strpos(' ' . $iplist, $threepartip) > 0) {
      return FULL_ROOT_URL . 'spam2.php';
    }
    if (strpos(' ' . $iplist, $twopartip) > 0) {
      return FULL_ROOT_URL . 'spam2.php';
    }
    if (strpos(' ' . $iplist, $onepartip) > 0) {
      return FULL_ROOT_URL . 'spam2.php';
    }

    // Now check in older system.
    $legacy_spam_class = new LegacySpamCache();
    return $legacy_spam_class->getAlternateSpamBlockPage();
  }

  /**
   * This code is run after a post is added to the DB.
   *
   * This is part of the newer spam blocking system that consists of lists of
   * ips, keywords etc.. that should result in items getting posted as hidden.
   */
  public function shouldBlockDbAdd() {

    if (getenv('REMOTE_ADDR')) {
      $current_ip = getenv('REMOTE_ADDR');
    }
    elseif (getenv('HTTP_CLIENT_IP')) {
      $current_ip = getenv('HTTP_CLIENT_IP');
    }
    elseif (getenv('HTTP_X_FORWARDED_FOR')) {
      $current_ip = getenv('HTTP_X_FORWARDED_FOR');
    }
    else {
      $current_ip = 'UNKNOWN';
    }

    $ip_pieces = explode('.', $current_ip);
    $current_ip1 = trim($ip_pieces[0]);
    $current_ip2 = trim($ip_pieces[1]);
    $threepartip = $current_ip1 . '.' . $current_ip2 . '.' . $current_ip1 . '.*';
    $twopartip = $current_ip1 . '.' . $current_ip2 . '.*';
    $onepartip = $current_ip1 . '.*';

    $iplist = $this->loadIpSpamFile();
    if (strpos(' ' . $iplist, $current_ip) > 0) {
      return TRUE;
    }
    if (strpos(' ' . $iplist, $threepartip) > 0) {
      return TRUE;
    }
    if (strpos(' ' . $iplist, $twopartip) > 0) {
      return TRUE;
    }
    if (strpos(' ' . $iplist, $onepartip) > 0) {
      return TRUE;
    }

    $titlelist = $this->loadTitleSpamFile();
    $titlelist = str_replace("\r", '', $titlelist);
    if (strpos(" \n" . $titlelist . "\n", "\n" . trim($_POST['title1']) . "\n") > 0) {
      return TRUE;
    }

    $textlist = $this->loadTextSpamFile();
    $textlist = str_replace("\r", '', $textlist);
    $text_array = explode("\n", $textlist);
    foreach ($text_array as $spamstr) {
      $spamstr = trim($spamstr);
      if ($spamstr == '') {
        continue;
      }
      if (isset($_POST['summary']) && strpos(' ' . $_POST['summary'], $spamstr) > 0) {
        return TRUE;
      }
      if (isset($_POST['text']) && strpos(' ' . $_POST['text'], $spamstr) > 0) {
        return TRUE;
      }
    }

  }

  /**
   * Loads IP block file.
   */
  public function loadIpBlockFile() {
    $file_util = new FileUtil();
    return $file_util->loadFileAsString(CACHE_PATH . '/spam/ip_block_list.txt');
  }

  /**
   * Saves IP block file.
   */
  public function saveIpBlockFile($str) {
    $file_util = new FileUtil();
    $file_util->saveStringAsFile(CACHE_PATH . '/spam/ip_block_list.txt', $str);
  }

  /**
   * Loads IP spam file.
   */
  public function loadIpSpamFile() {
    $file_util = new FileUtil();
    return $file_util->loadFileAsString(CACHE_PATH . '/spam/ip_spam_list.txt');
  }

  /**
   * Saves IP spam file.
   */
  public function saveIpSpamFile($str) {
    $file_util = new FileUtil();
    $file_util->saveStringAsFile(CACHE_PATH . '/spam/ip_spam_list.txt', $str);
  }

  /**
   * Loads title spam file.
   */
  public function loadTitleSpamFile() {
    $file_util = new FileUtil();
    return $file_util->loadFileAsString(CACHE_PATH . '/spam/title_spam_list.txt');
  }

  /**
   * Saves title spam file.
   */
  public function saveTitleSpamFile($str) {
    $file_util = new FileUtil();
    $file_util->saveStringAsFile(CACHE_PATH . '/spam/title_spam_list.txt', $str);
  }

  /**
   * Loads text spam file.
   */
  public function loadTextSpamFile() {
    $file_util = new FileUtil();
    return $file_util->loadFileAsString(CACHE_PATH . '/spam/text_spam_list.txt');
  }

  /**
   * Saves text spam file.
   */
  public function saveTextSpamFile($str) {
    $file_util = new FileUtil();
    $file_util->saveStringAsFile(CACHE_PATH . '/spam/text_spam_list.txt', $str);
  }

  /**
   * Loads validation string flie.
   */
  public function loadValidationStringFile() {
    $file_util = new FileUtil();
    return $file_util->loadFileAsString(CACHE_PATH . '/spam/validation_string_list.txt');
  }

  /**
   * Saves validation string file.
   */
  public function saveValidationStringFile($str) {
    $file_util = new FileUtil();
    $file_util->saveStringAsFile(CACHE_PATH . '/spam/validation_string_list.txt', $str);
  }

}
