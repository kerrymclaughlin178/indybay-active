<?php

namespace Indybay;

/**
 * This class should contain all methods for manipulating and rendering times.
 */
class Time {

  /**
   * Time.
   *
   * @var int
   */
  public $time = 0;

  /**
   * Sets a time as a number between 0 and 24.
   */
  public function setTime($newtime) {
    $this->time = $newtime;
  }

  /**
   * Retuns the time as a number between 0 and 24.
   */
  public function getTime() {
    return $this->time;
  }

  /**
   * Returns the hour portion of the time object in 24 hour format.
   */
  public function getHour() {
    return floor($this->time);
  }

  /**
   * Returms the minute portion of the time object.
   */
  public function getMinutes() {
    return round(60 * ($this->time - $this->getHour()));
  }

  /**
   * Returns true if the time is in the AM.
   */
  public function getIsAm() {
    $hour = $this->getHour();
    $test = floor($hour / 12);
    $test = $test % 2;
    if ($test == 0) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Returns "AM" or "PM" depending on if the time is in the AM or PM.
   */
  public function getAm() {
    if ($this->getIsAm()) {
      return 'AM';
    }
    else {
      return 'PM';
    }
  }

  /**
   * Returns the hour hour portion of the time object in 12 hour format.
   */
  public function get12Hour() {
    $hour = $this->getHour();
    if ($hour < 0) {
      return 0;
    }
    if (!(($hour - 1000) < 0)) {
      return 0;
    }
    if ($hour == 0) {
      return 12;
    }
    while ($hour > 12) {
      $hour = $hour - 12;
    }
    return $hour;
  }

  /**
   * Returns a formatted time in 12 hour format.
   */
  public function getDisplayTime() {
    $time = $this->get12Hour();
    $time = $time . ':';
    $minute = $this->getMinutes();
    if ($minute < 10) {
      $minute = '0' . $minute;
    }
    $time = $time . $minute . ' ';
    $time = $time . $this->getAm();
    return $time;
  }

}
