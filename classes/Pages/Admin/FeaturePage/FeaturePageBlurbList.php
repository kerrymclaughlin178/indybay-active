<?php

namespace Indybay\Pages\Admin\FeaturePage;

use Indybay\Page;
use Indybay\Translate;
use Indybay\DB\BlurbDB;
use Indybay\Date;
use Indybay\DB\FeaturePageDB;
use Indybay\Renderer\FeaturePageRenderer;

/**
 * Feature page blurb list.
 */
class FeaturePageBlurbList extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $feature_page_renderer_class = new FeaturePageRenderer();
    $blurb_db_class = new BlurbDB();
    if (isset($_GET['page_id'])) {
      $page_id = $_GET['page_id'];
    }
    else {
      $page_id = 0;
    }
    if ($page_id == 0 && isset($_POST['page_id'])) {
      $page_id = $_POST['page_id'];
    }
    $tblhtml = '';
    $tr = new Translate();

    if (isset($_GET['make_blurb_unhidden'])) {
      $news_item_id = $_GET['make_blurb_unhidden'] + 0;
      if ($news_item_id > 0) {
        $news_item_db_class = new NewsItemDB();
        $news_item_db_class->updateNewsItemStatusId($news_item_id, NEWS_ITEM_STATUS_ID_LOCAL_HIGHLIGHTED);
      }
    }

    if (isset($_GET['make_blurb_current'])) {
      $news_item_id = $_GET['make_blurb_current'];
      if ($news_item_id > 0) {
        $blurb_db_class->addBlurbToPage($news_item_id, $page_id);

      }
    }

    if (isset($_POST['reorder']) > 0) {
      $i = 1;
      while (isset($_POST['news_item_id' . $i])) {
        $next_news_item_id = $_POST['news_item_id' . $i] + 0;
        $next_order_num = $_POST['blurb_order' . $next_news_item_id];
        $next_display_option_id = $_POST['next_display_option_id' . $next_news_item_id];
        $blurb_db_class->updateOrderEntry($page_id, $next_news_item_id, $next_order_num, $next_display_option_id);
        if ($_POST['display_' . $next_news_item_id] != 'display') {
        }{
        if ($_POST['display_' . $next_news_item_id] == 'archive') {
          $blurb_db_class->removeBlurbFromPage($next_news_item_id, $page_id);
        }
        elseif ($_POST['display_' . $next_news_item_id] == 'hide') {
          $blurb_db_class->removeBlurbFromPage($next_news_item_id, $page_id);
          $blurb_db_class->updateNewsItemStatusId($next_news_item_id, NEWS_ITEM_STATUS_ID_HIDDEN);
        }
        }
        $i = $i + 1;
      }
    }

    $this->tkeys['local_subtitle'] = $tr->trans('features_edit');

    $feature_page_db_class = new FeaturePageDB();
    $feature_page_info = $feature_page_db_class->getFeaturePageInfo($page_id);
    $feature_page_long_name = $feature_page_info['long_display_name'];
    $this->tkeys['local_feature_page_name'] = $feature_page_long_name;
    $last_update_date_obj = new Date();
    $last_update_date_obj->setTimeFromSqlTime($feature_page_info['last_pushed_date']);
    $formatted_date = $last_update_date_obj->getFormattedDate() . ' ' . $last_update_date_obj->getFormattedTime();
    $this->tkeys['local_last_pushed_date'] = $formatted_date;

    $blurb_list = $feature_page_db_class->getCurrentBlurbListLimitedInfo($page_id);

    $this->tkeys['local_page_id'] = $page_id;
    $this->tkeys['local_current_link'] = $tr->trans('current');
    $this->tkeys['local_archived_link'] = "<a href=\"feature_page_archived_blurb_list.php?page_id=$page_id\">";
    $this->tkeys['local_archived_link'] .= $tr->trans('archived');
    $this->tkeys['local_archived_link'] .= '</a></td>';
    $this->tkeys['local_hidden_link'] = "<a href=\"feature_page_hidden_blurb_list.php?page_id=$page_id\">";
    $this->tkeys['local_hidden_link'] .= $tr->trans('hidden');
    $this->tkeys['local_hidden_link'] .= '</a></td>';

    $display_options = [
      BLURB_DISPLAY_OPTION_AUTO => 'auto',
      BLURB_DISPLAY_OPTION_AUTO_LONG => 'long version (auto template)',
      BLURB_DISPLAY_OPTION_AUTO_SHORT => 'short version (auto template)',
      BLURB_DISPLAY_OPTION_NOIMAGE_LONG => 'long version (no image)',
      BLURB_DISPLAY_OPTION_LEFT_LONG => 'long version (image on left)',
      BLURB_DISPLAY_OPTION_RIGHT_LONG => 'long version (image on right)',
      BLURB_DISPLAY_OPTION_NOIMAGE_SHORT => 'short version (no image)',
      BLURB_DISPLAY_OPTION_LEFT_SHORT => 'short version (image on left)',
      BLURB_DISPLAY_OPTION_RIGHT_SHORT => 'short version (image on right)',
      BLURB_DISPLAY_OPTION_HTML_LONG => 'long version (html only)',
      BLURB_DISPLAY_OPTION_HTML_SHORT => 'short version (html only)',
    ];

    if (!is_array($blurb_list) || count($blurb_list) < 1) {
      // Colspan to 9 (i added the test column :)
      $tblhtml = '<tr><td colspan="8">';
      $tblhtml .= $tr->trans('no_features_to_edit');
      $tblhtml .= '</td></tr>';

    }
    else {

      $i = 0;
      foreach ($blurb_list as $next_blurb) {
        $display_type =
                     $feature_page_renderer_class->makeSelectForm('next_display_option_id' . $next_blurb['news_item_id'],
        $display_options, $next_blurb['display_option_id']);
        ;

        $tblhtml .= '<tr ';
        $i = $i + 1;
        if (!is_int($i / 2)) {
          $tblhtml .= 'class="bgsearchgrey"';
        }

        $tblhtml .= ' ><td><input type="hidden" name="news_item_id';
        $tblhtml .= $i;
        $tblhtml .= '" value="';
        $tblhtml .= $next_blurb['news_item_id'];
        $tblhtml .= '" />';

        $tblhtml .= '<a href="blurb_edit.php?id=';
        $tblhtml .= $next_blurb['news_item_id'];
        $tblhtml .= '">';
        $tblhtml .= $next_blurb['news_item_id'] . '</td><td>';
        $tblhtml .= '<a href="blurb_edit.php?id=';
        $tblhtml .= $next_blurb['news_item_id'];
        $tblhtml .= '">';
        $tblhtml .= $next_blurb['title2'];
        $tblhtml .= '</a>';
        // Wanting to add live blurb link here
        // $tblhtml .= '<br/><span class="small">(';
        // $tblhtml .= '<a href="';
        // $tblhtml .= '/newsitems/' . $next_blurb['news_item_id'] . '.php';
        // $tblhtml .= {{ TPL_LOCAL_SINGLE_ITEM_VIEW_LINK|raw }};
        // $tblhtml .= '" target="_blank" title="view live blurb">';
        // $tblhtml .= 'view live blurb</a>';
        // $tblhtml .= ')</span>';
        // end live blurb link.
        $tblhtml .= '</td><td>';
        $tblhtml .= $next_blurb['created'];
        $tblhtml .= '</td><td';

        $next_blurb_date_obj = new Date();
        $next_blurb_date_obj->setTimeFromSqlTime($next_blurb['version_creation_date']);
        if ($next_blurb_date_obj->getUnixTime() > $last_update_date_obj->getUnixTime()) {
          $tblhtml .= ' style="color: #cc0000; font-weight: bold"';
        }

        $tblhtml .= '>';
        $tblhtml .= $next_blurb['modified'];
        $tblhtml .= '</td><td><INPUT size=4 name="blurb_order';
        $tblhtml .= $next_blurb['news_item_id'];
        $tblhtml .= '" value="';
        $tblhtml .= $next_blurb['order_num'];
        $tblhtml .= '" /></td>';

        $tblhtml .= '<td>' . $display_type . '</td>';
        $tblhtml .= '<td colspan="2"><select id="display_' . $next_blurb['news_item_id'] . '" name="display_' .
          $next_blurb['news_item_id'] . '">';
        $tblhtml .= '<option value="display">Display</option>';
        $tblhtml .= '<option value="archive">Archive</option>';
        $tblhtml .= '<option value="hide">Hide</option>';
        $tblhtml .= '</select><span class="linklike admin-select-control" title="archive this blurb" data-select-id="display_' .
          $next_blurb['news_item_id'] . '" data-select-set="archive">Archive</span></td>';
        $tblhtml .= '';
        // $tblhtml .= "<td>&#160;<a href=\"feature_page_archived_blurb_list.php?page_id=$page_id&remove_blurb_ordering=";
        // $tblhtml .= $next_blurb['news_item_id'];
        // $tblhtml .= "\">" . $tr->trans('action_archive') . "</a></td>";
        // $tblhtml .= "<td>&#160;<a href=\"feature_page_hidden_blurb_list.php?page_id=$page_id&make_blurb_hidden=";
        // $tblhtml .= $next_blurb['news_item_id']."\"";
        // $tblhtml .= ">" . $tr->trans('action_hide') . "</a></td>";.
      }
    }

    $this->tkeys['local_table_rows'] = $tblhtml;

    return 1;

  }

}
