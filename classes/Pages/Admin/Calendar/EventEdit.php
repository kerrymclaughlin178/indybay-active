<?php

namespace Indybay\Pages\Admin\Calendar;

use Indybay\Pages\Admin\Article\ArticleEdit;

/**
 * For now just inherits but also needs to deal with display date issues.
 */
class EventEdit extends ArticleEdit {

  /**
   * {@inheritdoc}
   */
  public function processAdditionalDbDependencies($old_article_info, $new_article_info) {

  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $ret = 1;
    if (trim($_POST['title1']) == '') {
      $this->addValidationMessage('Title Is Required.');
      $ret = 0;
    }
    return $ret;
  }

}
