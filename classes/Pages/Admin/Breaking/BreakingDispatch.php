<?php

namespace Indybay\Pages\Admin\Breaking;

use Indybay\Page;

use Twilio\Twiml;

/**
 * Dispatches update to reporters.
 */
class BreakingDispatch extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {
    if (!empty($_POST['AccountSid']) && !empty($_POST['CallSid']) && $_POST['AccountSid'] == TWILIO_SID) {
      $db_obj = new DB();
      $response = new Twiml();
      $titles = $db_obj->singleColumnQuery("SELECT title1 FROM news_item_version INNER JOIN news_item ON current_version_id = news_item_version_id INNER JOIN dispatch ON news_item.news_item_id = dispatch.news_item_id WHERE dispatch.sid = '" . $db_obj->prepareString($_POST['CallSid']) . "'");
      foreach ($titles as $title) {
        $response->say($title);
      }
      $response->hangup();
      print $response;
    }
  }

}
