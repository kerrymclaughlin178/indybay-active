<?php

namespace Indybay\Pages\Admin\Media;

use Indybay\Page;
use Indybay\DB\MediaAttachmentDB;
use Indybay\Renderer\MediaAttachmentRenderer;

/**
 * Class for upload_display_add page.
 */
class UploadList extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $media_attachment_db_class = new MediaAttachmentDB();
    $media_attachment_renderer_class = new MediaAttachmentRenderer();

    $image_list = '<br />';

    $page_size = 15;
    if (isset($_GET['page_number'])) {
      $page_number = $_GET['page_number'] + 0;
    }
    else {
      $page_number = 0;
    }
    $upload_type_id = 0;
    if (!isset($_GET['media_type_grouping_id'])) {
      // Sets default selected option for Uploaded Media page Media Type.
      $media_type_grouping_id = MEDIA_TYPE_GROUPING_IMAGE;
    }
    else {
      $media_type_grouping_id = $_GET['media_type_grouping_id'];
    }
    if (!isset($_GET['upload_type_id'])) {
      // Sets default selected option for Uploaded Media page Upload Type.
      $upload_type_id = UPLOAD_TYPE_POST;
    }
    else {
      $upload_type_id = $_GET['upload_type_id'];
    }

    if (isset($_GET['keyword'])) {
      $keyword = $_GET['keyword'];
    }
    else {
      $keyword = '';
    }

    $creator_id = 0;

    $start_limit = ($page_number) * $page_size;

    $image_list_info = $media_attachment_db_class
      ->getMediaAttachmentList($upload_type_id, $media_type_grouping_id,
                $creator_id, $page_size + 1, $start_limit, $keyword);

    $additional_links = '&keyword=' . urlencode($keyword) . "&media_type_grouping_id=$media_type_grouping_id&upload_type_id=$upload_type_id";
    $this->tkeys['nav'] = '';
    if ($page_number > 0) {
      $this->tkeys['nav'] .= '<a href="/admin/media/upload_list.php?page_number=' . ($page_number - 1) . $additional_links .
                '"><img src="/im/prev_arrow.gif" border=0/></a>&#160;&#160;';
    }
    $this->tkeys['nav'] .= $page_number + 1;
    if (count($image_list_info) > $page_size) {
      $this->tkeys['nav'] .= '&#160;&#160;
	        	<a href="/admin/media/upload_list.php?page_number=' . ($page_number + 1) . $additional_links .
                    '"><img src="/im/next_arrow.gif" border=0/></a>';
    }
    $this->tkeys['nav'] .= '';

    if (is_array($image_list_info)) {
      $i = 0;
      foreach ($image_list_info as $next_image_info) {
        $image_list .= $this->renderMediaAttachmentEditRow($next_image_info);
        $i = $i + 1;
        if ($i == $page_size) {
          break;
        }
      }
    }
    $this->tkeys['upload_rows'] = $image_list;
    $medium_options = $media_attachment_db_class->getMediumOptions();
    $medium_options[0] = 'All Media';
    $this->tkeys['LOCAL_SELECT_GROUPING_IDS'] = $media_attachment_renderer_class->makeSelectForm('media_type_grouping_id', $medium_options, $media_type_grouping_id);
    $upload_types = $media_attachment_db_class->getUploadTypes();
    $upload_types[0] = 'All Types';
    $this->tkeys['LOCAL_SELECT_UPLOAD_TYPES'] = $media_attachment_renderer_class->makeSelectForm('upload_type_id', $upload_types, $upload_type_id);
    $this->tkeys['LOCAL_KEYWORD'] = $keyword;

  }

  /**
   * Renders media attachment edit row.
   */
  public function renderMediaAttachmentEditRow($row) {

    $media_attachment_renderer = new MediaAttachmentRenderer();

    $local_upload_size = '';

    if ($row['file_size'] + 0 != 0) {
      $local_upload_size = round($row['file_size'] / 1000) . 'kb';
      if ($row['image_width'] + 0 != 0) {
        $local_upload_size .= '(' . $row['image_width'] . 'x' . $row['image_height'] . ')';
      }
    }
    $ret = '<tr class="bgsearchgrey" >';
    $ret .= '<td><a href="upload_edit.php?media_attachment_id=' . $row['media_attachment_id'] . '">';
    $ret .= $row['media_attachment_id'] . '</a></td>';
    $ret .= '<td>' . $row['file_name'] . '<br />' . $local_upload_size . '</td>';
    $ret .= '<td>';
    // Both 225 were 200.
    $ret .= $media_attachment_renderer->renderAttachment($row, 1, 225, 225) . '</td>';
    $ret .= '</tr>';

    return $ret;
  }

}
