<?php

namespace Indybay\Pages\Admin\User;

use Indybay\Page;
use Indybay\DB\UserDB;

/**
 * Class for user_display_list page.
 */
class UserList extends Page {

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $user_db_obj = new UserDB();
    $user_list = $user_db_obj->getRecentList(15);

    $tblhtml = '';
    foreach ($user_list as $nextuser) {
      $tblhtml .= '<tr style="background: #ffffff"><td>';
      $tblhtml .= $nextuser['user_id'] . '</td>';
      if ($nextuser['user_id'] == $_SESSION['session_user_id']) {
        $tblhtml .= '<td><a href="user_edit.php?user_id=';
      }
      else {
        $tblhtml .= '<td><a href="user_detail.php?user_id=';
      }
      $tblhtml .= $nextuser['user_id'] . '">';
      $tblhtml .= $nextuser['username'] . '</a></td>';
      $tblhtml .= '<td>' . $nextuser['first_name'] . '</td>';
      $tblhtml .= '<td>' . $nextuser['last_name'] . '</td>';
      $tblhtml .= '<td>' . $nextuser['email'] . '</td>';
      $tblhtml .= '<td>' . $nextuser['last_login'] . '</td>';
      $tblhtml .= '<td>' . $nextuser['last_activity'] . '</td>';
      $tblhtml .= "</tr>\n";
    }
    $this->tkeys['enabled_users_recent'] = $tblhtml;

    $user_list = $user_db_obj->getNonrecentList(15);
    $tblhtml = '';
    foreach ($user_list as $nextuser) {
      $tblhtml .= '<tr style="background: #ffffff"><td>';
      $tblhtml .= $nextuser['user_id'] . '</td>';
      if ($nextuser['user_id'] == $_SESSION['session_user_id']) {
        $tblhtml .= '<td><a href="user_edit.php?user_id=';
      }
      else {
        $tblhtml .= '<td><a href="user_detail.php?user_id=';
      }
      $tblhtml .= $nextuser['user_id'] . '">';
      $tblhtml .= $nextuser['username'] . '</a></td>';
      $tblhtml .= '<td>' . $nextuser['first_name'] . '</td>';
      $tblhtml .= '<td>' . $nextuser['last_name'] . '</td>';
      $tblhtml .= '<td>' . $nextuser['email'] . '</td>';
      $tblhtml .= '<td>' . $nextuser['last_login'] . '</td>';
      $tblhtml .= '<td>' . $nextuser['last_activity'] . '</td>';
      $tblhtml .= "</tr>\n";
    }

    $this->tkeys['enabled_users_nonrecent'] = $tblhtml;

    $user_list = $user_db_obj->getList(0);

    $tblhtml = '';
    foreach ($user_list as $nextuser) {
      $tblhtml .= '<tr style="background: #ffffff"><td>';
      $tblhtml .= $nextuser['user_id'] . '</td>';
      $tblhtml .= '<td><a href="user_edit.php?user_id=';
      $tblhtml .= $nextuser['user_id'] . '">';
      $tblhtml .= $nextuser['username'] . '</a></td>';
      $tblhtml .= '<td>' . $nextuser['first_name'] . '</td>';
      $tblhtml .= '<td>' . $nextuser['last_name'] . '</td>';
      $tblhtml .= '<td>' . $nextuser['email'] . '</td>';
      $tblhtml .= '<td>' . $nextuser['last_login'] . '</td>';
      $tblhtml .= "</tr>\n";
    }

    $this->tkeys['disabled_users'] = $tblhtml;
    $this->tkeys['local_session_user_id'] = $_SESSION['session_user_id'];
    return 1;
  }

}
