<?php

namespace Indybay\Pages\Admin\Misc;

use Indybay\Page;

/**
 * Class for admin_index page.
 */
class Status extends Page {

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    // Class constructor, does nothing.
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    // Execution method, does nothing.
    return 1;
  }

}
