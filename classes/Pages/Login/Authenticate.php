<?php

namespace Indybay\Pages\Login;

use Indybay\DB\UserDB;
use Indybay\Page;

/**
 * Class for authenticate page.
 */
class Authenticate extends Page {

  /**
   * We can do everything in the constructor to avoid the whole mess.
   */
  public function __construct() {
    if (empty($_POST['username1']) || empty($_POST['password']) || !isset($_POST['goto'])) {
      header("{$_SERVER['SERVER_PROTOCOL']} 403 Forbidden");
      exit;
    }
    $user_obj = new UserDB();
    $user_id = $user_obj->authenticate($_POST['username1'], $_POST['password']);
    if ($user_id + 0 > 0) {
      if (!isset($_SESSION)) {
        ini_set('session.save_path', SESSIONS_PATH);
        session_start();
      }
      // Regenerate session ID to avoid session fixation.
      session_regenerate_id(TRUE);
      $_SESSION['session_username']           = $_POST['username1'];
      $_SESSION['session_user_id']            = $user_id;
      $_SESSION['session_is_editor']          = TRUE;
      $_SESSION['session_last_activity_time'] = time();

      session_write_close();

      if ($_POST['goto'] != ''
      && strpos($_POST['goto'], 'admin') + 0 > 0
      && ($_POST['goto'] != '/admin/')
      && ($_POST['goto'] != '//admin/')
      && $_POST['goto'] && strpos($_POST['goto'], 'authentication') + 0 == 0
      ) {
        $goto = $_POST['goto'];
        if (strpos(' ' . $goto, '/') == 1) {
          $goto = substr($goto, 1);
        }
      }
      else {
        $goto = 'admin/article/article_list.php?include_posts=1&include_events=1&include_comments=1&news_item_status_restriction=874';
      }
      header('Location: ' . ADMIN_ROOT_URL . $goto);
      exit;
    }
    else {
      $goto = $_POST['goto'];
      $goto = urlencode($goto);
      header('Location: ' . ADMIN_ROOT_URL . "admin/authentication/authenticate_display_logon.php?logon_failed=true&goto=$goto");
      exit;
    }
  }

  /**
   * Execution method, does nothing.
   */
  public function execute() {
    return 1;
  }

}
