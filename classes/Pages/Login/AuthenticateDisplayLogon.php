<?php

namespace Indybay\Pages\Login;

use Indybay\DB\DB;
use Indybay\Page;
use Indybay\Translate;
use Indybay\Renderer\Renderer;

/**
 * Class for authenticate_display_logon page.
 */
class AuthenticateDisplayLogon extends Page {

  /**
   * Class constructor, does nothing.
   */
  public function __construct() {
    return 1;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {

    $tr = new Translate();

    // Test connection.
    $db_class = new DB();
    $db_class->getConnection();

    if (array_key_exists('db_down', $GLOBALS) && $GLOBALS['db_down'] == '1') {
      $this->tkeys['local_error'] = '<p class="error"><strong>The DB Seems to be overloaded (possibly due to a spam attack or other such problem), please try to connect again in a few minutes</strong></p>';
    }
    elseif (array_key_exists('logon_failed', $_GET)) {
      $this->tkeys['local_error'] = '<p class="error"><strong>' . $tr->trans('logon_failed') . '</strong></p>';
    }
    else {
      $this->tkeys['local_error'] = '';
    }
    if (array_key_exists('username1', $_POST)) {
      $username = $_POST['username1'];
    }
    elseif (array_key_exists('session_username', $_SESSION)) {
      $username = $_SESSION['session_username'];
    }
    else {
      $username = '';
    }
    $this->tkeys['sitenick'] = $GLOBALS['site_nick'];
    $renderer_class = new Renderer();
    $this->tkeys['local_username'] = $renderer_class->checkPlain($username);
    $this->tkeys['goto'] = $renderer_class->checkPlain($_GET['goto']);

    return 1;
  }

}
