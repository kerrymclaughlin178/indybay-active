<?php

namespace Indybay;

/**
 * The Page class is used to compile and build pages.
 *
 * Written December 2005 - January 2006
 * modified from an sf-active class
 *  * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */
class Page extends Common {

  /**
   * Page ID.
   *
   * @var string
   */
  public $pageid;

  /**
   * Page.
   *
   * @var object
   */
  public $page;

  /**
   * Script file.
   *
   * @var string
   */
  public $scriptFile;

  /**
   * Forced template file.
   *
   * @var string
   */
  public $forcedTtemplateFile;

  /**
   * Template path.
   *
   * @var string
   */
  public $templatePath;

  /**
   * Class constructor.
   */
  public function __construct($pageid = '', $relative_dir = '') {
    if ($pageid == '') {
      return 1;
    }

    if ($relative_dir != '') {
      $relative_dir = '/' . $relative_dir;
    }

    $this->error = '';
    $tr = new Translate();

    $tr->createTranslateTable('page');

    // Constructor which begins setting page properties.
    if (!is_string($pageid)) {
      $this->error = $tr->trans('string_error');
    }
    else {
      $this->pageid = $pageid;

      // First, check for a template file.
      $this->template_file = $pageid . '.tpl';
      if (!file_exists(TEMPLATE_PATH . '/pages' . $relative_dir . '/' . $this->template_file)) {
        echo 'UNABLE TO LOAD TEMPLATE: ' . TEMPLATE_PATH . '/pages' . $relative_dir . '/' . $this->template_file . '<br />';
        $this->error = $tr->trans('template_error');
      }
      else {

        $this->templatePath = 'pages' . $relative_dir;
        // Next, check for a script file.
        $class = '\Indybay\Pages\\';
        foreach (explode('/', trim($relative_dir, '/')) as $component) {
          $class .= str_replace('_', '', ucwords($component, '_')) . '\\';
        }
        $class .= str_replace('_', '', ucwords($pageid, '_'));
        // Next, try to instantiate the script class.
        if (!class_exists($class)) {
          $this->error = $tr->trans('class_error');
          echo "UNABLE TO LOAD PAGEID $pageid <br />";
        }
        else {
          $this->page = new $class();
        }
      }
    }

    // Last, check for a dictionary file.
    $this->translate = new Translate();

    $dict_file = str_replace('/', '_', $relative_dir);
    if (strlen($dict_file) > 0) {
      $dict_file = substr($dict_file, 1);
    }
    $this->translate->createTranslateTable($dict_file);

    return 1;
  }

  /**
   * Gets error.
   */
  public function getError() {
    if (strlen($this->error) > 0) {
      return $this->error;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Builds page.
   */
  public function buildPage($content_page = '') {
    unset($this->page->forcedTemplateFile);

    if (isset($this->page)) {
      $this->page->execute();
    }

    if (isset($this->page->forcedTemplateFile)) {
      $this->forceNewTemplate($this->page->forcedTemplateFile);
    }

    $defaults = [];

    foreach ($GLOBALS['dict'] as $key => $value) {
      $keyid = 'TPL_' . strtoupper($key);
      $defaults[$keyid] = $value;
    }

    if (isset($this->page->tkeys) && is_array($this->page->tkeys)) {
      foreach ($this->page->tkeys as $key => $value) {
        $keyid = 'TPL_' . strtoupper($key);
        $pagevars["$keyid"] = $value;
      }
      $defaults = array_merge($defaults, $pagevars);
    }
    $defaults['TPL_VALIDATION_MESSAGES'] = $this->getFormattedValidationMessages();
    $defaults['TPL_STATUS_MESSAGES'] = $this->getFormattedStatusMessages();

    krsort($defaults);

    if (isset($this->templatePath)) {
      $this->html = $this->render("{$this->templatePath}/{$this->template_file}", $defaults);
    }

    return 1;
  }

  /**
   * Gets HTML.
   */
  public function getHtml() {
    if (isset($this->html)) {
      return $this->html;
    }
  }

  /**
   * Forces new template.
   */
  public function forceNewTemplate($template_name) {
    // Forces a new template if you need to do it in mid-stream.
    if (!file_exists(TEMPLATE_PATH . '/pages/' . $template_name)) {
      $tr = new Translate();
      $tr->createTranslateTable('page');
      $this->error = $tr->trans('template_error');
    }
    else {
      $this->template_file = $template_name;
      $this->templatePath = 'pages';
    }
  }

  /**
   * Clears validation messages.
   */
  public function clearValidationMessages() {
    $GLOBALS['validation_messages'] = [];
  }

  /**
   * Adds validation messages.
   */
  public function addValidationMessage($message) {
    if (!isset($GLOBALS['validation_messages'])) {
      $GLOBALS['validation_messages'] = [];
    }
    array_push($GLOBALS['validation_messages'], $message);
  }

  /**
   * Adds status messages.
   */
  public function addStatusMessage($message) {
    if (!isset($GLOBALS['status_messages'])) {
      $GLOBALS['status_messages'] = [];
    }
    array_push($GLOBALS['status_messages'], $message);
  }

  /**
   * Gets formatted validation messages.
   */
  public function getFormattedValidationMessages() {
    $tr = new Translate();
    if (!isset($GLOBALS['validation_messages'])) {
      return '';
    }
    $formatted_messages = '';

    foreach ($GLOBALS['validation_messages'] as $next_message) {
      $formatted_messages .= '<li class="error">' . $next_message . '</li>';
    }

    if (strlen($formatted_messages) > 0) {
      $html = '<p class="error"><strong>';
      $html .= $tr->trans('error_detected') . '</strong><br />';
      $html .= $tr->trans('fill_out_form_right') . ':<br />';
      $formatted_messages = $html . '<ul class="error">' . $formatted_messages . '</ul>';
    }
    return $formatted_messages;
  }

  /**
   * Gets formatted status messages.
   */
  public function getFormattedStatusMessages() {
    if (!isset($GLOBALS['status_messages'])) {
      return '';
    }
    $formatted_messages = '';

    foreach ($GLOBALS['status_messages'] as $next_message) {
      $formatted_messages .= '<br /><strong>' . $next_message . '</strong>';
    }
    return $formatted_messages . '<br /><br />';
  }

  /**
   * Merges arrays without renumbering keys.
   */
  public function arrayMergeWithoutRenumbering($array1, $array2) {
    $temp_array = $array1;
    foreach ($temp_array as $key => $value) {
      $array2[$key] = $value;
    }
    return $array2;

  }

  /**
   * Redirects page.
   */
  public function redirect($url, $time_delay = 0) {
    $this->tkeys['redirect_url'] = $url;
    $this->tkeys['delay_time'] = $time_delay;
    $this->forcedTemplateFile = 'misc/redirect.tpl';

  }

}
