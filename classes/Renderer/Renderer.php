<?php

namespace Indybay\Renderer;

/**
 * Written December 2005.
 *
 * Modification Log:
 * 12/2005-1/2005  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */

use Indybay\Common;

/**
 * Common methods used in rendering dropdown lists, safe text from users, etc.
 */
class Renderer extends Common {

  /**
   * Encode special characters in a plain-text string for display as HTML.
   */
  public static function checkPlain($text) {
    return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
  }

  /**
   * Functional wrapper for the HTML Purifier library.
   *
   * @param string $html
   *   The markup to purify.
   * @param string $allowed
   *   Optional comma-delimited list of allowed tags.
   * @param bool $trusted
   *   Whether or not content was created by an admin user.
   *
   * @return string
   *   Purified HTML.
   */
  public static function htmlPurify($html, $allowed = NULL, $trusted = FALSE) {
    static $purifier = FALSE;
    $config = \HTMLPurifier_Config::createDefault();
    $config->set('Cache.SerializerPath', CACHE_PATH . '/HTMLPurifier');
    if (!$purifier) {
      $purifier = new \HTMLPurifier($config);
    }
    $config->set('HTML.Allowed', $allowed);
    if ($trusted) {
      $config->set('HTML.DefinitionID', 'trusted');
      $config->set('HTML.DefinitionRev', 1);
      $config->set('Attr.EnableID', TRUE);
      if ($def = $config->maybeGetRawHTMLDefinition()) {
        $def->addAttribute('img', 'usemap', 'URI');
        $def->addElement('map', 'Block', 'Flow', 'Common', [
          'name' => 'CDATA',
        ]);
        $def->addElement('area', 'Inline', 'Empty', 'Common', [
          'shape' => 'Enum#rect,circle,poly,default',
          'coords' => 'CDATA',
          'href' => 'URI',
          'nohref' => 'Bool',
          'alt' => 'CDATA',
        ]);
      }
    }
    return $purifier->purify($html, $config);
  }

  /**
   * Return an HTML-escaped page title.
   */
  public static function pageTitle() {
    $title = isset($GLOBALS['page_title']) ? $GLOBALS['page_title'] . ' : ' . $GLOBALS['site_nick'] : $GLOBALS['site_name'];
    return self::checkPlain($title);
  }

  /**
   * Returns an HTML-escaped page URL.
   */
  public static function htmlPageUrl() {
    return SERVER_URL . self::checkPlain(str_replace('/index.php', '/', $_SERVER['REQUEST_URI']));
  }

  /**
   * Converts URLs to links and HTML-encodes plain text.
   */
  public function cleanupText($tmpvar, $trusted = FALSE) {

    // Encode.
    $tmpvar = str_replace(['<', '>', '"'], ['&lt;', '&gt;', '&quot;'], $tmpvar);

    // re-encode to avoid confusing the link generator.
    $from = ['&quot;', '&lt;', '&gt;'];
    $to = [' &quot; ', ' &lt; ', ' &gt; '];
    $tmpvar = str_replace($from, $to, $tmpvar);

    // Replace newlines with html.
    $tmpvar = preg_replace('/(\n\n|\r\r|\r\n\r\n|\r\n\t|\n\t|\r\t)/', ' <br /><br /> ', $tmpvar);
    $tmpvar = preg_replace('/(\r\n|\n|\r)/', ' <br /> ', $tmpvar);

    // Turn e-mail addresses into mailto links.
    $tmpvar = preg_replace('/([A-Za-z0-9_]([-._]?[A-Za-z0-9])*){1}@([A-Za-z0-9]([-.]?[A-Za-z0-9])*\.[A-Za-z]+)/', '<a href="mailto:\\0">\\1 [at] \\3</a>', $tmpvar);

    $rel = $trusted ? '' : 'rel="nofollow" ';

    // Basic regex for URLs (after the scheme).
    $url = '[!#%&+./0-9:;=?A-Z_a-z~@-]+';
    // A subset of characters are permitted at the end of URLs.
    $end = '[#%&/0-9:?A-Z_a-z~@]';
    // Match URLs that contain a set of parentheses.
    $url = "($url\($url\)$url$end|$url\($url\)|$url$end)";

    // Turn URLs into links.
    $tmpvar = preg_replace("^www\.$url^i", "http://\\0", $tmpvar);
    $tmpvar = preg_replace("^(https?://)http://(www\.$url)^i", "\\1\\2", $tmpvar);
    $tmpvar = preg_replace("^https?://$url^i", "<a {$rel}href=\"\\0\">\\0</a>", $tmpvar);

    // Repair <a href="<a href=""></a>"> and <a href=<a href=""></a>>.
    $tmpvar = preg_replace("^ &lt; a href= &quot; <a {$rel}href=\"https?://$url\">(https?://$url)</a> &quot;  &gt; ^i", "<a {$rel}href=\"\\2\">", $tmpvar);
    $tmpvar = preg_replace("^ &lt; a href=<a {$rel}href=\"https?://$url\">(https?://$url)</a> &gt; ^i", "<a {$rel}href=\"\\2\">", $tmpvar);
    $tmpvar = preg_replace('^ &lt; /a &gt; ^i', '</a>', $tmpvar);

    // de-reencode.
    $from = [' &lt; ', ' &gt; ', ' &quot; '];
    $to = ['&lt;', '&gt;', '&quot;'];
    $tmpvar = str_replace($from, $to, $tmpvar);

    return $tmpvar;
  }

  /**
   * Shortens links for display.
   */
  public function shortenLinkForDisplay($url) {

    if (strlen($url) > 45) {
      $url = substr($url, 0, 42) . '...';
    }

    return $url;
  }

  /**
   * Formats links.
   */
  public function formatLink($urllink) {

    if (trim($urllink) == '') {
      return trim($urllink);
    }
    $urllink = trim($urllink);
    if (!(preg_match('#^http://|^ftp://|^https://#i', $urllink, $reg))) {
      $urllink = 'http://' . $urllink;
    }

    return $this->checkPlain($urllink);

  }

  /**
   * Makes a 24-hour date/time select form.
   */
  public function makeDatetimeSelectForm24Hour($base_name, $num_years_in_past, $num_years_in_future, $selected_day, $selected_month, $selected_year, $selected_hour, $selected_minute) {

    $return_html = '';
    $return_html .= $this->makeSelectForm($base_name . '_month', $this->customRange(1, 12), $selected_month);
    $return_html .= '<span class="timeseparator">/</span>';
    $return_html .= $this->makeSelectForm($base_name . '_day', $this->customRange(1, 31), $selected_day);
    $return_html .= '<span class="timeseparator">/</span>';
    $year_start = strftime('%Y') - $num_years_in_past;
    $year_end = strftime('%Y') + $num_years_in_future;
    $return_html .= $this->makeSelectForm($base_name . '_year', $this->customRange($year_start, $year_end), $selected_year);
    $return_html .= '<input type="hidden" size="10" id="linkedDates" disabled="disabled" /><br />';
    $hours = [
      0 => 'Midnight',
      1 => '1 AM',
      2 => '2 AM',
      3 => '3 AM',
      4 => '4 AM',
      5 => '5 AM',
      6 => '6 AM',
      7 => '7 AM',
      8 => '8 AM',
      9 => '9 AM',
      10 => '10 AM',
      11 => '11 AM',
      12 => 'Noon',
      13 => '1 PM',
      14 => '2 PM',
      15 => '3 PM',
      16 => '4 PM',
      17 => '5 PM',
      18 => '6 PM',
      19 => '7 PM',
      20 => '8 PM',
      21 => '9 PM',
      22 => '10 PM',
      23 => '11 PM',
    ];
    $return_html .= $this->makeSelectForm($base_name . '_hour', $hours, $selected_hour);
    $return_html .= '<span class="timeseparator">:</span>';
    $minute_options = [];
    $minute_options[0] = 'on the hour';
    $minute_options[15] = '15 min after';
    $minute_options[30] = '30 min after';
    $minute_options[45] = '45 min after';
    $return_html .= $this->makeSelectForm($base_name . '_minute', $minute_options, $selected_minute);

    return $return_html;
  }

  /**
   * Makes a date/time select form.
   */
  public function makeDatetimeSelectForm($base_name, $num_years_in_past, $num_years_in_future, $selected_day, $selected_month, $selected_year, $selected_hour, $selected_minute, $ampm) {

    $return_html = '';
    $return_html .= $this->makeSelectForm($base_name . '_month', $this->customRange(1, 12), $selected_month);
    $return_html .= '/';
    $return_html .= $this->makeSelectForm($base_name . '_day', $this->customRange(1, 31), $selected_day);
    $return_html .= '/';
    $year_start = strftime('%Y') - $num_years_in_past;
    $year_end = strftime('%Y') + $num_years_in_future;
    $return_html .= $this->makeSelectForm($base_name . '_year', $this->customRange($year_start, $year_end), $selected_year);
    $return_html .= ' ';
    $return_html .= $this->makeSelectForm($base_name . '_hour', $this->customRange(1, 12), $selected_hour);
    $return_html .= ':';
    $minute_options = [];
    $minute_options[0] = '00';
    $minute_options[15] = '15';
    $minute_options[30] = '30';
    $minute_options[45] = '45';
    $return_html .= $this->makeSelectForm($base_name . '_minute', $minute_options, $selected_minute);
    $return_html .= '<select name="' . $base_name . '_ampm">';
    $return_html .= '<option value="AM">AM</option>';
    $return_html .= '<option value="PM" ';
    if ($ampm == 'PM') {
      $return_html .= ' selected ';
    }
    $return_html .= '>PM</option>';
    $return_html .= '</select>';

    return $return_html;
  }

  /**
   * Makes a date select form.
   */
  public function makeDateSelectForm($base_name, $num_years_in_past, $num_years_in_future, $selected_day, $selected_month, $selected_year) {

    $return_html = '';
    $return_html .= $this->makeSelectForm($base_name . '_month', $this->customRange(1, 12), $selected_month);
    $return_html .= '<span class="datetext"> / </span>';
    $return_html .= $this->makeSelectForm($base_name . '_day', $this->customRange(1, 31), $selected_day);
    $return_html .= '<span class="datetext"> / </span>';
    $year_start = strftime('%Y') - $num_years_in_past;
    $year_end = strftime('%Y') + $num_years_in_future;
    $return_html .= $this->makeSelectForm($base_name . '_year', $this->customRange($year_start, $year_end), $selected_year);

    return $return_html;
  }

  /**
   * Makes a boolean checkbox form.
   */
  public function makeBooleanCheckboxForm($checkbox_name, $checked) {
    $return_html = '';
    $return_html .= '<input type="checkbox" name="' . $checkbox_name . '" id="' . $checkbox_name . '"';
    $return_html .= " value='1' ";
    if ($checked == 1) {
      $return_html .= ' checked="checked"';
    }
    $return_html .= ' />';

    return $return_html;

  }

  /**
   * Detects if a string is UTF-8.
   */
  public function isUtf8($str) {
    // Values of -1 represent disalloweded values for the first bytes in
    // current UTF-8.
    static $trailing_bytes = [
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
      -1, -1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
      2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,
    ];

    $ups = unpack('C*', $str);
    if (!($aCnt = count($ups))) {
      // Empty string *is* valid UTF-8.
      return TRUE;
    }
    for ($i = 1; $i <= $aCnt;) {
      if (!($tbytes = $trailing_bytes[($b1 = $ups[$i++])])) {
        continue;
      }
      if ($tbytes == -1) {
        return FALSE;
      }

      $first = TRUE;
      while ($tbytes > 0 && $i <= $aCnt) {
        $cbyte = $ups[$i++];
        if (($cbyte & 0xC0) != 0x80) {
          return FALSE;
        }

        if ($first) {
          switch ($b1) {
            case 0xE0:
              if ($cbyte < 0xA0) {
                return FALSE;
              }
              break;

            case 0xED:
              if ($cbyte > 0x9F) {
                return FALSE;
              }
              break;

            case 0xF0:
              if ($cbyte < 0x90) {
                return FALSE;
              }
              break;

            case 0xF4:
              if ($cbyte > 0x8F) {
                return FALSE;
              }
              break;

            default:
              break;
          }
          $first = FALSE;
        }
        $tbytes--;
      }
      if ($tbytes) {
        // Incomplete sequence at EOS.
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * Makes a checkbox form.
   */
  public function makeCheckboxForm($checkbox_name, $options, $checked, $numberline) {

    $i = 1;
    $return_html = '<table><tr>';
    foreach ($options as $key => $value) {
      $return_html .= '<td><input type="checkbox" name="' . $checkbox_name . '"';
      $return_html .= ' value="' . $key . '"';
      if (is_array($checked)) {
        if (in_array($key, $checked)) {
          $return_html .= ' checked="checked"';
        }
      }
      $return_html .= ' />' . $value . "</td>\n";
      if ($i == $numberline) {
        $return_html .= '</tr><tr>';
        $i = 1;
      }
      else {
        $i = $i + 1;
      }
    }
    $return_html .= '</tr></table>';

    return $return_html;

  }

  /**
   * Makes a select form.
   */
  public function makeSelectForm($select_name, $options, $match, $default = '') {

    $return_html = '<select name="' . $select_name . '" id="' . $select_name . "\">\n";
    if ($default != '') {
      $return_html .= '<option value="0">' . $default . '</option>';
    }
    foreach ($options as $key => $value) {
      $return_html .= "<option value=\"$key\"";
      if ($match == $key) {
        $return_html .= ' selected="selected"';
      }
      $return_html .= '>' . $this->checkPlain($value) . "</option>\n";
    }
    $return_html .= "</select>\n";

    return $return_html;
  }

  /**
   * Creates a custom range.
   */
  public function customRange($val1, $val2) {

    $new_array = [];
    if ($val2 > $val1 && $val2 - 100 < $val1) {
      $i = $val1;
    }
    while ($i <= $val2) {
      $new_array[$i] = $i;
      $i = $i + 1;
    }

    return $new_array;
  }

  /**
   * Creates a custom range using $n as increment.
   */
  public function customRangeByN($val1, $val2, $n) {

    $new_array = [];
    if ($n > 0 && $val2 > $val1 && $val2 - 100 * $n < $val1) {
      $i = $val1;
      while ($i <= $val2) {
        $new_array[$i] = $i;
        $i = $i + $n;
      }
    }

    return $new_array;
  }

  /**
   * Creates a dropdown.
   */
  public function createDropdown($list_of_objs, $selected_item) {

    // Renders dropdown for event updates.
    $return_val = '';
    foreach ($list_of_objs as $key => $value) {
      $return_val .= '<option value="';
      $return_val .= $key;
      $return_val .= '"';
      if (strlen($selected_item) > 0) {
        if ($selected_item == $key) {
          $return_val .= ' selected="selected" ';
        }
      }
      $return_val .= '>' . $value;
      $return_val .= '</option>';
    }

    return $return_val;
  }

  /**
   * Gets days of month (unused?).
   */
  public function getDaysOfMonth() {
    // Returns an an array of obejcts with ids and names set to numbers from 1
    // to 31.
    $days_of_month = [];
    for ($i = 1; $i < 32; $i = $i + 1) {
      $days_of_month[$i] = $i;
    }
    return $days_of_month;
  }

  /**
   * Gets months (unused?).
   */
  public function getMonths() {
    // Returns an an array of obejcts with ids and names set to numbers from 1
    // to 12.
    $months = [];
    for ($i = 1; $i < 13; $i = $i + 1) {
      $months[$i] = $i;
    }
    return $months;
  }

  /**
   * Gets years (unused?).
   */
  public function getYears() {

    // Returns an an array of obejcts with ids and names set to numbers
    // between gloabl var $dropdown_min_year and $dropdown_max_year.
    $dropdown_max_year = $GLOBALS['dropdown_max_year'];
    $dropdown_min_year = $GLOBALS['dropdown_min_year'];
    $now_array = getdate(time());
    if (strlen($dropdown_min_year) < 1) {
      $dropdown_min_year = 2000;
    }
    if (strlen($dropdown_max_year) < 1) {
      $dropdown_max_year = $now_array['year'] + 2;
    }
    $years = [];
    for ($i = $dropdown_min_year; $i < $dropdown_max_year; $i = $i + 1) {
      $years[$i] = $i;
    }

    return $years;
  }

  /**
   * Gets hours (unused?).
   */
  public function getHours() {

    $hours = [];
    for ($i = 1; $i < 13; $i = $i + 1) {
      $hours[$i] = $i;
    }

    return $hours;
  }

  /**
   * Gets minutes (unused?).
   */
  public function getMinutes() {

    // Returns an an array of objects with ids 0 and 30 and names "00" and "30".
    $minutes = [];
    $minutes[0] = '00';
    $minutes[30] = '30';

    return $minutes;
  }

  /**
   * Gets AM/PM (unused?).
   */
  public function getAmpm() {

    // Returns an an array of objects with ids 0 and 12 and names "AM" and "PM".
    $ampm = [];
    $new_object = new DefaultObject();
    $new_object->set_id(0);
    $new_object->set_name('AM');
    array_unshift($ampm, $new_object);
    $new_object = new DefaultObject();
    $new_object->set_id(12);
    $new_object->set_name('PM');
    array_unshift($ampm, $new_object);

    return $ampm;
  }

}
