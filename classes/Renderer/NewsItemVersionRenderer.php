<?php

namespace Indybay\Renderer;

/**
 * Written December 2005 - January 2006.
 *
 * Modification Log:
 * 12/2005-1/2006  zogren/Zachary Ogren/zogren@yahoo.com
 * initial development.
 */

/**
 * Parent class for article and blurb renderers.
 */
class NewsItemVersionRenderer extends NewsItemRenderer {

  /**
   * Renders a version list for the article edit page.
   */
  public function renderVersionList($article_list_info, $url) {

    $return = '<table><tr><td><strong>VersionID</strong></td><td><strong>Version Date</strong></td>';
    $return .= '<td>User ID</td><td>Username</td><title>title1</title></tr>';
    foreach ($article_list_info as $version_info) {
      $return .= '<tr>';
      $return .= '<td><a href="' . $url . '?version_id=' . $version_info['news_item_version_id'] . '">';
      $return .= $version_info['news_item_version_id'] . '</a></td>';
      $return .= '<td>' . $version_info['version_creation_date'] . '</td>';
      $return .= '<td>' . $version_info['user_id'] . '</td>';
      $return .= '<td>' . $version_info['username'] . '</td>';
      $return .= '<td>' . $version_info['title1'] . '</td>';
      $return .= '</tr>';
    }
    $return .= '</table>';

    return $return;
  }

}
